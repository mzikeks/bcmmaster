﻿
namespace BCMmaster.Sync
{
    partial class ChooseECUNBC250Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NBC250CL_rb = new System.Windows.Forms.RadioButton();
            this.NBC250A_rb = new System.Windows.Forms.RadioButton();
            this.buttonOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // NBC250CL_rb
            // 
            this.NBC250CL_rb.AutoSize = true;
            this.NBC250CL_rb.Location = new System.Drawing.Point(13, 27);
            this.NBC250CL_rb.Name = "NBC250CL_rb";
            this.NBC250CL_rb.Size = new System.Drawing.Size(170, 21);
            this.NBC250CL_rb.TabIndex = 0;
            this.NBC250CL_rb.TabStop = true;
            this.NBC250CL_rb.Text = "NBC 250L / NBC 250C";
            this.NBC250CL_rb.UseVisualStyleBackColor = true;
            // 
            // NBC250A_rb
            // 
            this.NBC250A_rb.AutoSize = true;
            this.NBC250A_rb.Location = new System.Drawing.Point(13, 55);
            this.NBC250A_rb.Name = "NBC250A_rb";
            this.NBC250A_rb.Size = new System.Drawing.Size(94, 21);
            this.NBC250A_rb.TabIndex = 1;
            this.NBC250A_rb.TabStop = true;
            this.NBC250A_rb.Text = "NBC 250A";
            this.NBC250A_rb.UseVisualStyleBackColor = true;
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(135, 158);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // ChooseECUNBC250Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(232, 203);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.NBC250A_rb);
            this.Controls.Add(this.NBC250CL_rb);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ChooseECUNBC250Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Выберите тип прошивки";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton NBC250CL_rb;
        private System.Windows.Forms.RadioButton NBC250A_rb;
        private System.Windows.Forms.Button buttonOk;
    }
}