﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BCMmaster
{
    public partial class ChooseECUUC : UserControl
    {
        public enum ECUType
        {
            None = 0,
            Continental,     // Continental SID208
            Bosch,            // Bosch EDC17CP52
            Delphi1,          // Delphi DCM6.2
            Delphi2,          // Delphi DCM7.1
            Marelli,          // Marelli MJD8F3
            NBC250CL,
            NBC250A
        }

        public ECUType selectedECU
        {
            get
            {
                if (Continental_radioButton.Checked)
                    return ECUType.Continental;
                if (Bosch_radioButton.Checked)
                    return ECUType.Bosch;
                if (Delphi1_radioButton.Checked)
                    return ECUType.Delphi1;
                if (Delphi2_radioButton.Checked)
                    return ECUType.Delphi2;
                if (Marelli_radioButton.Checked)
                    return ECUType.Marelli;

                return ECUType.None;
            }
        }

        public ChooseECUUC()
        {
            InitializeComponent();
        }
    }
}
