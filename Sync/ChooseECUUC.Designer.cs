﻿
namespace BCMmaster
{
    partial class ChooseECUUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Continental_radioButton = new System.Windows.Forms.RadioButton();
            this.Bosch_radioButton = new System.Windows.Forms.RadioButton();
            this.Delphi1_radioButton = new System.Windows.Forms.RadioButton();
            this.Delphi2_radioButton = new System.Windows.Forms.RadioButton();
            this.Marelli_radioButton = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // Continental_radioButton
            // 
            this.Continental_radioButton.AutoSize = true;
            this.Continental_radioButton.Location = new System.Drawing.Point(14, 14);
            this.Continental_radioButton.Name = "Continental_radioButton";
            this.Continental_radioButton.Size = new System.Drawing.Size(150, 21);
            this.Continental_radioButton.TabIndex = 0;
            this.Continental_radioButton.TabStop = true;
            this.Continental_radioButton.Text = "Continental SID208";
            this.Continental_radioButton.UseVisualStyleBackColor = true;
            // 
            // Bosch_radioButton
            // 
            this.Bosch_radioButton.AutoSize = true;
            this.Bosch_radioButton.Location = new System.Drawing.Point(14, 41);
            this.Bosch_radioButton.Name = "Bosch_radioButton";
            this.Bosch_radioButton.Size = new System.Drawing.Size(150, 21);
            this.Bosch_radioButton.TabIndex = 0;
            this.Bosch_radioButton.TabStop = true;
            this.Bosch_radioButton.Text = "Bosch EDC17CP52";
            this.Bosch_radioButton.UseVisualStyleBackColor = true;
            // 
            // Delphi1_radioButton
            // 
            this.Delphi1_radioButton.AutoSize = true;
            this.Delphi1_radioButton.Location = new System.Drawing.Point(14, 68);
            this.Delphi1_radioButton.Name = "Delphi1_radioButton";
            this.Delphi1_radioButton.Size = new System.Drawing.Size(123, 21);
            this.Delphi1_radioButton.TabIndex = 0;
            this.Delphi1_radioButton.TabStop = true;
            this.Delphi1_radioButton.Text = "Delphi DCM6.2";
            this.Delphi1_radioButton.UseVisualStyleBackColor = true;
            // 
            // Delphi2_radioButton
            // 
            this.Delphi2_radioButton.AutoSize = true;
            this.Delphi2_radioButton.Location = new System.Drawing.Point(14, 95);
            this.Delphi2_radioButton.Name = "Delphi2_radioButton";
            this.Delphi2_radioButton.Size = new System.Drawing.Size(123, 21);
            this.Delphi2_radioButton.TabIndex = 0;
            this.Delphi2_radioButton.TabStop = true;
            this.Delphi2_radioButton.Text = "Delphi DCM7.1";
            this.Delphi2_radioButton.UseVisualStyleBackColor = true;
            // 
            // Marelli_radioButton
            // 
            this.Marelli_radioButton.AutoSize = true;
            this.Marelli_radioButton.Location = new System.Drawing.Point(14, 122);
            this.Marelli_radioButton.Name = "Marelli_radioButton";
            this.Marelli_radioButton.Size = new System.Drawing.Size(126, 21);
            this.Marelli_radioButton.TabIndex = 0;
            this.Marelli_radioButton.TabStop = true;
            this.Marelli_radioButton.Text = "Marelli MJD8F3";
            this.Marelli_radioButton.UseVisualStyleBackColor = true;
            // 
            // ChooseECUUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Marelli_radioButton);
            this.Controls.Add(this.Delphi2_radioButton);
            this.Controls.Add(this.Delphi1_radioButton);
            this.Controls.Add(this.Bosch_radioButton);
            this.Controls.Add(this.Continental_radioButton);
            this.Name = "ChooseECUUC";
            this.Size = new System.Drawing.Size(172, 161);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton Continental_radioButton;
        private System.Windows.Forms.RadioButton Bosch_radioButton;
        private System.Windows.Forms.RadioButton Delphi1_radioButton;
        private System.Windows.Forms.RadioButton Delphi2_radioButton;
        private System.Windows.Forms.RadioButton Marelli_radioButton;
    }
}
