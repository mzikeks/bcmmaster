﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static BCMmaster.ChooseECUUC;

namespace BCMmaster.Sync
{
    public partial class ChooseECUNBC250Form : Form
    {
        public ChooseECUNBC250Form(bool isEnglishInterface)
        {
            InitializeComponent();

            buttonOk.DialogResult = DialogResult.OK;
            AcceptButton = buttonOk;
            if (isEnglishInterface)
                Text = "Choose eprom type";
        }

        public ECUType selectedECU  
        {
            get
            {
                if (NBC250CL_rb.Checked)
                    return ECUType.NBC250CL;
                return ECUType.NBC250A;
            }
            
        }

        public void buttonOk_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
