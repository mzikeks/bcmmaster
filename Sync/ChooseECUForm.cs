﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static BCMmaster.ChooseECUUC;

namespace BCMmaster
{
    public partial class ChooseECUForm : Form
    {
        public ChooseECUForm(bool isEnglishInterface)
        {
            InitializeComponent();
            buttonOk.DialogResult = DialogResult.OK;
            AcceptButton = buttonOk;
            if (isEnglishInterface)
                Text = "Choose eprom type";
        }

        public ECUType selectedECU => chooseECUUC1.selectedECU;

        public void buttonOk_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
