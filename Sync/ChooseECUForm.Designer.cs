﻿
namespace BCMmaster
{
    partial class ChooseECUForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChooseECUForm));
            this.buttonOk = new System.Windows.Forms.Button();
            this.chooseECUUC1 = new BCMmaster.ChooseECUUC();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(131, 179);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // chooseECUUC1
            // 
            this.chooseECUUC1.Location = new System.Drawing.Point(12, 12);
            this.chooseECUUC1.Name = "chooseECUUC1";
            this.chooseECUUC1.Size = new System.Drawing.Size(172, 161);
            this.chooseECUUC1.TabIndex = 0;
            // 
            // ChooseECUForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(236, 220);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.chooseECUUC1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChooseECUForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Выберите тип прошивки";
            this.ResumeLayout(false);

        }

        #endregion

        private ChooseECUUC chooseECUUC1;
        private System.Windows.Forms.Button buttonOk;
    }
}