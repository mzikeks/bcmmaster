﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;
using BCMmaster.MarkupParsing;

namespace BCMmaster
{
    public partial class MainForm : Form
    {
        public bool bEpromLoaded = false;
        public EpromMarkup currentMarkup = EpromMarkup.GetMarkup("BCS20I");

        public const string DEFAULT_INI_PATH = "initOptions.xml";
        public const string DEFAULT_PARAMS_PATH = "paramsList.xml";
        public const string DEFAULT_EPROM_PATH = "eprom.bin";
        public const string DEFAULT_ECU_PATH = "ecu.bin";
        public const string DEFAULT_BCM_SRC_PATH = "srcbcm.bin";
        public const string DEFAULT_BCM_DST_PATH = "dstbcm.bin";
        public const string DEFAULT_BCM_RES_PATH = "resbcm.bin";

        //
        // опции запуска программы
        //
        public class ProgramOptions
        {
            public bool bEnglishInterface { get; set; }

            // считывать последний епром при запуске
            //
            public bool bLoadLastEpromOnStart { get; set; }

            // путь к last used епром
            //
            public string sEpromFilePath { get; set; }
 
            // путь к епром (отображаемому в списках)
            //
            public string sEpromDirPath { get; set; }

            // путь к исходным bcm
            //
            public string sBCMsrcDirPath { get; set; }

            // путь к результирующим bcm
            //
            public string sBCMresDirPath { get; set; }
            
            // путь к исходным ecu
            //
            public string sECUsrcDirPath { get; set; }


            public ProgramOptions()
            {

            }

            public ProgramOptions(
                bool englishInterface,
                bool loadLastEpromOnStart,
                string epromFilePath, 
                string epromDirPath,
                string BCMsrcDirPath,
                string BCMresDirPath,
                string ECUsrcDirPath )
            {
                bEnglishInterface = englishInterface;
                sEpromFilePath = epromFilePath;
                bLoadLastEpromOnStart = loadLastEpromOnStart;
                sEpromDirPath = epromDirPath;
                sBCMsrcDirPath = BCMsrcDirPath;
                sBCMresDirPath = BCMresDirPath;
                sECUsrcDirPath = ECUsrcDirPath; 
            }
        };

        // опции запуска
        //
        public static ProgramOptions gProgOptions = new ProgramOptions();

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        public MainForm()
        {
            InitializeComponent();

            DisableControlsWithoutEpromLoading();

            if (!ReadProgramOptions() || !ReadParamsList())
            {
                FirstStart();
            }
            else
            {
                if (gProgOptions.bLoadLastEpromOnStart)
                {
                    if (null != gProgOptions.sEpromFilePath)
                    {
                        readAndParceEpromFile(false);
                    }
                }
            }

            SetInterface(gProgOptions.bEnglishInterface);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        public string SetStringLanguageAccording(
            string ruString, 
            string engString)
        {
            if (gProgOptions.bEnglishInterface)
                return engString;
            else
                return ruString;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void SetInterface(bool isEnglishInterface)
        {
            if (gProgOptions.bLoadLastEpromOnStart)
                loadLastEpromOnStartToolStripMenuItem.Checked = true;
            else
                loadLastEpromOnStartToolStripMenuItem.Checked = false;

            if (isEnglishInterface)
            {
                SyncControlSum_checkBox.Text = "Control sum";
                toolStripButtonOpen.Text = "Open";
                toolStripButtonSave.Text = "Save";

                buttonVinChange.Text = "Change";
                buttonECUBCM.Text = "Start";
                buttonBCMClone.Text = "Start";
                buttonCheckSum.Text = "Recalculate";

                groupBoxCheckSum.Text = "Check sum";
                groupBoxECUBCM.Text = "ECU-BCM synchronization";
                groupBoxBCMClone.Text = "BCM clone";

                checkBoxBlock1.Text = "Block 1";
                checkBoxBlock2.Text = "Block 2";
                checkBoxBlock3.Text = "Block 3";
                checkBoxFile.Text = "All blocks";

                column1.Text = "Parameter";
                column2.Text = "Value";

                columnConfig1.Text = "Parameter";
                columnConfig2.Text = "Value";
                columnConfig3.Text = "Help";

                columnEquipment1.Text = "Parameter";
                columnEquipment2.Text = "Value";
                //columnEquipment4.Text = "Measure";
                columnEquipment3.Text = "Help";

                columnSafety1.Text = "Parameter";
                columnSafety2.Text = "Value";
                columnSafety3.Text = "Help";

                fileToolStripMenuItem.Text = "&File";
                optionsToolStripMenuItem.Text = "&Options";
                helpToolStripMenuItem.Text = "&Help";

                readEpromToolStripMenuItem.Text = "Open eprom";
                savepromToolStripMenuItem.Text = "Save eprom";
                savepromAsToolStripMenuItem.Text = "Save eprom as..";

                exitToolStripMenuItem.Text = "Exit";

                languageToolStripMenuItem.Text = "Language EN";
                loadLastEpromOnStartToolStripMenuItem.Text = "Load last eprom on start";
                setEpromDirtoolStrip.Text = "Set eprom dir";
                saveOptionsNowToolStripMenuItem.Text = "Save Options now";

                aboutToolStripMenuItem.Text = "About";

                tabControlMain.TabPages[0].Text = "Identification";
                tabControlMain.TabPages[1].Text = "Configuration";
                tabControlMain.TabPages[2].Text = "Equipment";
                tabControlMain.TabPages[3].Text = "Passive Safety";
                tabControlMain.TabPages[4].Text = "Instruments";
            }
            else
            {
                SyncControlSum_checkBox.Text = "Контрольная сумма";
                toolStripButtonOpen.Text = "Открыть";
                toolStripButtonSave.Text = "Сохранить";

                buttonVinChange.Text = "Изменить";
                buttonECUBCM.Text = "Начать";
                buttonBCMClone.Text = "Начать";
                buttonCheckSum.Text = "Пересчитать";

                groupBoxCheckSum.Text = "Контрольная сумма";
                groupBoxECUBCM.Text = "Синхронизация ECU-BCM";
                groupBoxBCMClone.Text = "Клонирование BCM";

                checkBoxBlock1.Text = "Блок 1";
                checkBoxBlock2.Text = "Блок 2";
                checkBoxBlock3.Text = "Блок 3";
                checkBoxFile.Text = "Все";

                column1.Text = "Наименование";
                column2.Text = "Значение";

                columnConfig1.Text = "Наименование";
                columnConfig2.Text = "Значение";
                columnConfig3.Text = "Помощь";

                columnEquipment1.Text = "Параметр";
                columnEquipment2.Text = "Значение";
                //columnEquipment4.Text = "Единицы";
                columnEquipment3.Text = "Помощь";

                columnSafety1.Text = "Наименование";
                columnSafety2.Text = "Значение";
                columnSafety3.Text = "Помощь";

                fileToolStripMenuItem.Text = "&Файл";
                optionsToolStripMenuItem.Text = "&Опции";
                helpToolStripMenuItem.Text = "&Помощь";

                readEpromToolStripMenuItem.Text = "Открыть eprom";
                savepromToolStripMenuItem.Text = "Сохранить eprom";
                savepromAsToolStripMenuItem.Text = "Сохранить eprom как..";

                exitToolStripMenuItem.Text = "Выход";

                languageToolStripMenuItem.Text = "Язык RU";
                loadLastEpromOnStartToolStripMenuItem.Text = "Загружать последний eprom при запуске";
                setEpromDirtoolStrip.Text = "Установить рабочий каталог с eprom";
                saveOptionsNowToolStripMenuItem.Text = "Сохранить опции";
                aboutToolStripMenuItem.Text = "О программе";

                tabControlMain.TabPages[0].Text = "Идентификация";
                tabControlMain.TabPages[1].Text = "Конфигурация";
                tabControlMain.TabPages[2].Text = "Оборудование";
                tabControlMain.TabPages[3].Text = "Пассивная безопасность";
                tabControlMain.TabPages[4].Text = "Инструменты";
            }

            setEpromDirtoolStrip.ToolTipText = gProgOptions.sEpromDirPath;

            return;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void FirstStart()
        {
            string s = Directory.GetCurrentDirectory();

            /*public ProgramOptions(
                bool englishInterface,
                bool loadLastEpromOnStart,
                string epromFilePath,
                string epromDirPath,
                string BCMsrcDirPath,
                string BCMresDirPath,
                string ECUsrcDirPath)*/

            gProgOptions = new ProgramOptions(
                true, 
                false, 
                DEFAULT_EPROM_PATH,
                s,
                s,
                s,
                s);

            WriteProgramOptions();

            return;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        bool ReadParamsList()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(DataParamDescriptor[]));
            
            bool allOK = true;

            try
            {
                using (FileStream fs = new FileStream(DEFAULT_PARAMS_PATH , FileMode.Open))
                {
                    currentMarkup.propertiesMarkup.DataBlockParamsList = (DataParamDescriptor[])formatter.Deserialize(fs);
                }
            }
            catch
            {
                allOK = false;
            }

            return allOK;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // считать опции запуска программы из файла - язык, последний открытый eprom итд
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        bool ReadProgramOptions()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(ProgramOptions));
            
            bool allOK = true;

            try
            {
                using (FileStream fs = new FileStream(DEFAULT_INI_PATH, FileMode.Open))
                {
                    gProgOptions = (ProgramOptions)formatter.Deserialize(fs);
                }
            }
            catch
            {
                allOK = false;
            }

            return allOK;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // сохранить опции запуска программы в файл - язык, последний открытый eprom итд
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        bool WriteProgramOptions()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(ProgramOptions));

            bool allOK = true;

            try
            {
                using (FileStream fs = new FileStream(DEFAULT_INI_PATH, FileMode.Create))
                {
                    formatter.Serialize(fs, gProgOptions);
                }
            }
            catch
            {
                allOK = false;
            }

            formatter = new XmlSerializer(typeof(DataParamDescriptor[]));

            try
            {
                using (FileStream fs = new FileStream(DEFAULT_PARAMS_PATH, FileMode.Create))
                {
                    formatter.Serialize(fs, currentMarkup.propertiesMarkup.DataBlockParamsList);
                }
            }
            catch
            {
                allOK = false;
            }

            if (! allOK)
            {
                string msg;
                string hdr;

                msg = SetStringLanguageAccording("Ошибка сохранения опций. Освободите файл initOptions.xml",
                        "Options file saving error. Release access for initOptions.xml");
                hdr = SetStringLanguageAccording("Ошибка", "Error");

                MessageBox.Show(
                    msg,
                    hdr,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            return allOK;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        //
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        //
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void languageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gProgOptions.bEnglishInterface = !gProgOptions.bEnglishInterface;

            SetInterface(gProgOptions.bEnglishInterface);

            if (bEpromLoaded)
                FillAllTabs();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        //
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void saveOptionsNowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WriteProgramOptions();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        //
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void loadLastEpromOnStartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loadLastEpromOnStartToolStripMenuItem.Checked)
                gProgOptions.bLoadLastEpromOnStart = true;
            else
                gProgOptions.bLoadLastEpromOnStart = false;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // выбор пункта меню "открыть епром"
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void readEpromToolStripMenuItem_Click(object sender, EventArgs e)
        {
            readAndParceEpromFile(true);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // нажатие кнопки меню "открыть епром"
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void toolStripButtonOpen_Click(object sender, EventArgs e)
        {
            readAndParceEpromFile(true);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // считать файл eprom, проверить целостность, отобразить информацию в табе "Информация"
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void readAndParceEpromFile(bool bNewFileOpening)
        {
            string msg;
            string hdr;

            //
            // считать файл епром в память
            //
            bool bReadOK = PrepareAndTryToReadEpromFile(bNewFileOpening);

            if (!bNewFileOpening && !bReadOK)
            {
                msg = SetStringLanguageAccording("Не удалось прочитать файл. Выберите другой",
                    "Can't read file. Choose another");
                hdr = SetStringLanguageAccording("Ошибка", "Error");

                if (DialogResult.No == MessageBox.Show(
                    msg,
                    hdr,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error))
                    return;
            }

            if (bNewFileOpening)
            {
                //
                // если открывали новый файл (руками), то в файле опций обновить путь к нему
                //
                WriteProgramOptions();
            }

            //
            // проверить епром и идентифицировать производителя
            //

            if (!CheckEpromIntegrity())
            {
                msg = SetStringLanguageAccording("Целостность файла нарушена, продолжить работу с этим файлом?",
                    "File corrupted. Continue to work with it?");
                hdr = SetStringLanguageAccording("Ошибка", "Error");

                if (DialogResult.No == MessageBox.Show(msg, hdr, MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    return;
            }

            bEpromLoaded = true;

            EnableControlsAfterEpromLoading();

            //
            // отобразить данные
            //
            FillAllTabs();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void EnableControlsAfterEpromLoading()
        {
            tabControlMain.Enabled = true;
            tabControlMain.Visible = true;

            toolStripButtonSave.Enabled = true;
            toolStripTextBoxSearch.Enabled = true;

            pictureBoxImg.Visible = true;
            pictureBoxLogo.Visible = true;

            listViewID.Visible = true;
            listViewID.Enabled = true;

            listViewConfig.Enabled = true;
            listViewConfig.Visible = true;

            listViewEquipment.Enabled = true;
            listViewEquipment.Visible = true;

            listViewSafety.Enabled = true;
            listViewSafety.Visible = true;

            listViewIDMainInfo.Visible = true;

            savepromToolStripMenuItem.Enabled = true;
            savepromAsToolStripMenuItem.Enabled = true;

            toolStripButtonFind.Enabled = true;

        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void DisableControlsWithoutEpromLoading()
        {
            tabControlMain.Enabled = false;
            tabControlMain.Visible = false;

            toolStripButtonSave.Enabled = false;
            toolStripTextBoxSearch.Enabled = false;

            pictureBoxImg.Visible = false;
            pictureBoxLogo.Visible = false;

            listViewID.Visible = false;
            listViewID.Enabled = false;

            listViewConfig.Enabled = false;
            listViewConfig.Visible = false;

            listViewEquipment.Enabled = false;
            listViewEquipment.Visible = false;

            listViewSafety.Enabled = false;
            listViewSafety.Visible = false;

            listViewIDMainInfo.Visible = false;

            savepromToolStripMenuItem.Enabled = false;
            savepromAsToolStripMenuItem.Enabled = false;

            toolStripButtonFind.Enabled = false;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------

        void SetCurrentCarModel()
        {
            gVINStr = Encoding.UTF8.GetString(epromFile, EpromMarkup.OFFSET_VIN_STRING, EpromMarkup.SIZE_VIN_STRING);
            IdentifyCarModel(gVINStr, out currentMarkup);
            FillAllTabs();

        }

        void FillAllTabs()
        {
            gVINStr = Encoding.UTF8.GetString(epromFile, EpromMarkup.OFFSET_VIN_STRING, EpromMarkup.SIZE_VIN_STRING);

            this.Text = "BCMTools" + " --- " + gModelStr + " --- " + currFileName;

            InsertLogoInPictureBox();

            InsertImgInPictureBox();

            FillIdentificationMainListView();

            FillIdentificationListViewFromParsedList();

            FillConfigurationListViewFromParsedList();

            FillEquipmentListViewFromParsedList();

            FillSafetyListViewFromParsedList();

            textBoxVin.Text = gVINStr;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // установить каталог рабочих прошивок eprom
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void toolStripSetEpromDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbDlg = new FolderBrowserDialog();

            if (fbDlg.ShowDialog() == DialogResult.OK)
            {
                gProgOptions.sEpromDirPath = fbDlg.SelectedPath;
                SetInterface(gProgOptions.bEnglishInterface);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // ownerdraw отрисовка списка на вкладке идентификации. для изменения цветов
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void listViewID_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            listViewID.SuspendLayout();

            if (0 != (e.State & ListViewItemStates.Selected)) // выделенный элемент
            {
                e.Graphics.FillRectangle(Brushes.Sienna, e.Bounds);
                e.Graphics.DrawString(e.Item.Text, listViewID.Font, Brushes.Wheat, e.Bounds);
                e.Graphics.DrawString(e.Item.SubItems[1].Text, listViewID.Font, Brushes.Wheat, new Point (e.Bounds.X + 400, e.Bounds.Y));
            }
            else
            {
                e.Graphics.FillRectangle(Brushes.Wheat, e.Bounds);
                e.Graphics.DrawString(e.Item.Text, listViewID.Font, Brushes.Black, e.Bounds);
                e.Graphics.DrawString(e.Item.SubItems[1].Text, listViewID.Font, Brushes.Black, new Point(e.Bounds.X + 400, e.Bounds.Y));
            }

            listViewID.ResumeLayout();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // ownerdraw отрисовка заголовка списка на вкладке идентификации. для изменения цветов
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void listViewID_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;

            e.Graphics.FillRectangle(Brushes.DarkKhaki, e.Bounds);
            e.Graphics.DrawString(e.Header.Text, new Font("Helvetica", 14, FontStyle.Regular), Brushes.White, e.Bounds, sf);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // выбор пункта меню "сохранить"
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void savepromToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveEprom(currFileName);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // нажатие кнопки меню "сохранить"
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void toolStripButtonSave_Click(object sender, EventArgs e)
        {
            saveEprom(currFileName);
        }
            
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // сохранение файла eprom
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void saveEprom(string epromFileName)
        {
            string msg;
            string hdr;

            /*
            MessageBox.Show(
                "Available in full version only",
                "Please upgrade",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);

            return;
            */

            if (null != fsResultBCM) fsResultBCM.Close();

            try
            {
                fsResultBCM = new FileStream(epromFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                bwResultBCM = new BinaryWriter(fsResultBCM);
                bwResultBCM.Write(epromFile);
                bwResultBCM.Flush();
            }
            catch
            {
                msg = SetStringLanguageAccording("Проблема с доступом к файлу." +
                        "\rЗакройте другие программы, работающие с этим файлом," +
                        "\rили выберите другие исходные файлы.",
                        "There is a problem with file access." +
                        "\rClose other programs that works with that file," +
                        "\rOr choose another file.");

                hdr = SetStringLanguageAccording("Ошибка",
                    "Error");

                MessageBox.Show(
                        msg,
                        hdr,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
            }
            
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void savepromAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string msg;
            string hdr;

            /*
            MessageBox.Show(
                "Available in full version only",
                "Please upgrade",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);

            return;
            */

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = System.Environment.CurrentDirectory;

            saveFileDialog.Filter = "Бинарные файлы (*.bin)|*.bin|Все файлы (*.*)|*.*";
            saveFileDialog.DefaultExt = "bin";
            saveFileDialog.FileName = "eeprom.bin";
            saveFileDialog.Title = "Выберите результирующий BCM файл";

            if (DialogResult.OK == saveFileDialog.ShowDialog() &&
                saveFileDialog.FileName.Length > 0)
            {
                try
                {
                    fsResultBCM = new FileStream(saveFileDialog.FileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                    bwResultBCM = new BinaryWriter(fsResultBCM);
                    bwResultBCM.Write(epromFile);
                    bwResultBCM.Flush();
                }
                catch
                {
                    msg = SetStringLanguageAccording("Проблема с доступом к файлу." +
                            "\rЗакройте другие программы, работающие с этим файлом," +
                            "\rили выберите другие исходные файлы.",
                            "There is a problem with file access." +
                            "\rClose other programs that works with that file," +
                            "\rOr choose another file.");

                    hdr = SetStringLanguageAccording("Ошибка",
                        "Error");

                    MessageBox.Show(
                            msg,
                            hdr,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                }
            }
            else
            {
                return;
            }


        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAbout aboutForm = new FormAbout();
            aboutForm.ShowDialog();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void checkBoxFile_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxFile.Checked)
            {
                checkBoxVIN.Checked = checkBoxBlock1.Checked = checkBoxBlock2.Checked = checkBoxBlock3.Checked = true;
                checkBoxVIN.Enabled = checkBoxBlock1.Enabled = checkBoxBlock2.Enabled = checkBoxBlock3.Enabled = false;
            }
            else
            {
                checkBoxVIN.Enabled = checkBoxBlock1.Enabled = checkBoxBlock2.Enabled = checkBoxBlock3.Enabled = true;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        public void EditSelectedParameter()
        {
            ListView currentList = new ListView();

            GetCurrentList(ref currentList);

            int selectedListIndex = currentList.SelectedIndices[0];

            EditParameters EditParametersForm = new EditParameters(this);

            if (DialogResult.OK == EditParametersForm.ShowDialog(this))
            {
                CopyModifiedBlockAndUpdateCheckSum();
                //saveEprom(currFileName);
                FillAllTabs();
                GetCurrentList(ref currentList);
                currentList.EnsureVisible(selectedListIndex);
            }

        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // нажатие CTRL+F - активизация поиска
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void tabControlMain_KeyDown(object sender, KeyEventArgs e)
        {
            if ((tabControlMain.SelectedTab == tabPage2) ||
                (tabControlMain.SelectedTab == tabPage3) ||
                (tabControlMain.SelectedTab == tabPage4))
            {
                if (e.Control && e.KeyCode == Keys.F)
                {
                    toolStripTextBoxSearch.Focus();
                    toolStripTextBoxSearch.SelectAll();
                    e.SuppressKeyPress = true;
                }

                if (e.KeyCode == Keys.F3) //((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.F3))
                {
                    FindNext();
                    e.SuppressKeyPress = true;
                }

                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Insert) || (e.KeyCode == Keys.Add))
                {
                    EditSelectedParameter();
                    e.SuppressKeyPress = true;
                }

            }
        }

        public static listType gListType;

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void tabControlMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            lock (oLockIndex)
            {
                gStartIndex = 0;
            }

            if (tabPage1 == tabControlMain.SelectedTab)
            {
                gListType = listType.IDList;
            }
            else if (tabPage2 == tabControlMain.SelectedTab)
            {
                gListType = listType.ConfigList;
            }
            else if (tabPage3 == tabControlMain.SelectedTab)
            {
                gListType = listType.EquipmentList;
            }
            else if (tabPage4 == tabControlMain.SelectedTab)
            {
                gListType = listType.PassiveSafetyList;
            }
            else gListType = listType.Other;
        }

        public stringsMultyColumnsData[] gCurrentMCData = new stringsMultyColumnsData[MAX_ROWS_AT_EQUIPMENT_TAB];

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        public ListView GetCurrentList(ref ListView currentList)
        {
            switch (gListType)
            {
                case listType.ConfigList:
                    currentList = listViewConfig;
                    gCurrentMCData = strListConfig;
                    break;

                case listType.EquipmentList:
                    currentList = listViewEquipment;
                    gCurrentMCData = strListEquipmet;
                    break;

                case listType.PassiveSafetyList:
                    currentList = listViewSafety;
                    gCurrentMCData = strListSafety;
                    break;

                default:
                    currentList = listViewConfig;
                    gCurrentMCData = strListConfig;
                    break;
            }

            return currentList;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private bool itemMatches(ListViewItem item, string searchStr)
        {
            if (item.Text.ToLower().Contains(searchStr.ToLower()))
                return true;

            foreach (ListViewItem.ListViewSubItem subItem in item.SubItems)
            {
                if (subItem.Text.ToLower().Contains(searchStr.ToLower()))
                    return true;
            }

            return false;
        }

        

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void FindString()
        {
            ListView currentList = new ListView();

            GetCurrentList(ref currentList);

            // определить в котором списке искать
            //
            int i = 0;
            int currPos = 0;
            bool found = false;

            //
            // найти последний выделенный элемент списка
            //
            foreach (ListViewItem item in currentList.Items)
            {
                if (item.Selected)
                    currPos = item.Index;
            }

            //
            // искать начнем от последнего выделенного или от gStartIndex, смотря что ниже в списке
            //
            currPos = Math.Max(currPos, gStartIndex);

            for (i = currPos; i < currentList.Items.Count; i++)
            {
                if (itemMatches(currentList.Items[i], gStrToFind))
                {
                    //
                    //
                    //
                    gStartIndex = i+1;

                    currentList.SelectedItems.Clear();
                    currentList.EnsureVisible(i);

                    currentList.Select();
                    currentList.Items[i].Selected = true;

                    found = true;

                    break;
                }
            }

            if (!found && (0 != gStartIndex))
            //
            // искали не с начала и не нашли. поищем от начала 
            //
            {
                for (i = 0; i < currPos; i++)
                {
                    if (itemMatches(currentList.Items[i], gStrToFind))
                    {
                        //
                        //
                        //
                        gStartIndex = i + 1;

                        currentList.SelectedItems.Clear();
                        currentList.EnsureVisible(i);

                        currentList.Select();
                        currentList.Items[i].Selected = true;

                        found = true;

                        break;
                    }
                }

            }

            lock (oLockIndex)
            {
                if (i == currentList.Items.Count)
                    gStartIndex = 0;
            }
        }

        object oLockIndex = new object();
        string gStrToFind;
        int gStartIndex = 0;

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void FindNext()
        {
            // поиск только на вкладках 2, 3, 4. на 1 и 5 не искать
            //
            if ((tabPage1 == tabControlMain.SelectedTab) ||
                (tabPage5 == tabControlMain.SelectedTab))
                return;

            if ((null != gStrToFind) && (string.Empty != gStrToFind))
                FindString();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void toolStripTextBoxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.F3))
            {
                FindNext();
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void toolStripButtonFind_Click(object sender, EventArgs e)
        {
            FindNext();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void toolStripTextBoxSearch_TextChanged(object sender, EventArgs e)
        {
            gStrToFind = toolStripTextBoxSearch.Text;
        }

    }
}


