﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BCMmaster.MarkupParsing;

namespace BCMmaster
{
    public partial class MainForm : Form
    {

        /*
        Наименование параметра                      Значение            Адрес, смещение                     Маска               Размер, байт            Проверено

        Код конфигурации	                        08250260106	            0x204		                    -                       11                      +
        Код прибора для телекодирования             OUTPUTDJIT	            0x20F		                    -                       10                      +
        Дата выпуска	                            08/05/2014	            0x24F		                    -                       4                       ???
        Интеллектуальный коммутационный блок        Имеется	                0x220	                        0x01	                1                       +
        Интеллектуальный коммутационный блок 
        телекодирован на заводе                     Да	                    0x228	                        0x01	                1                       +
        Компьютер двигателя                         Имеется	                0x220	                        0x02	                1                       +
        Панель приборов                             Имеется	                0x220	                        0x08	                1                       +
        Панель приборов
        телекодирована на заводе                    Да	                    0x228	                        0x08	                1
        Радиоприемник                               Отсутствует	            0x220	                        0x20	                1
        Автомагнитола телекодирована на заводе      Нет	                    0x228	                        0x20	                1
        Антиблокировка колес(ABS) / 
        Система динамической стабилизации(ESP)      Имеется	                0x220	                        0x40	                1
        Кондиционер                                 Отсутствует	            0x221	                        0x04	                1
        Кондиционер телекодирован на заводе         Нет	                    0x229	                        0x04	                1

        Автоматизированная механическая 
        коробка передач                             Отсутствует			    ------------------------------------

        Датчик угла поворота рулевого колеса        Имеется	                0x221	                        0x10	                1
        Система помощи при парковке                 Имеется	                0x223	                        0x01	                1
        Подушка безопасности                        Имеется	                0x223	                        0x04	                1
        Подушки безопасности 
        телекодированы на заводе                    Да	                    0x22B	                        0x04	                1

        Пневматическая подвеска Отсутствует			1                       ------------------------------------

        Предупреждение непреднамеренном 
        пересечении линии дорожной разметки         Отсутствует	            0x223	                        0x40	                1
        Обнаружение падения давления воздуха 
        в шинах                                     Отсутствует	            0x224	                        0x01	                1
        Обнаружение пониженного давления 
        в шинах телекодировано на заводе            Нет	                    0x22C	                        0x01	                1
        Навигационная система                       Имеется	                0x224	                        0x02	                1
        Радионавигация телекодирована на заводе     Да	                    0x22C	                        0x02	                1
        
        Цифровой хронотахограф(спидометр, тахометр) Отсутствует			    ------------------------------------
        Коммутационный блок прицепа                 Отсутствует			    ------------------------------------
        Общее число технических обслуживаний        5			            ------------------------------------
        */

        public const int MAX_ROWS_AT_CONFIG_TAB = 35;

        stringsMultyColumnsData[] strListConfig = new stringsMultyColumnsData[MAX_ROWS_AT_CONFIG_TAB];

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void FillConfigurationListViewFromParsedList()
        {
            ListViewItem lvi;
            listViewConfig.Items.Clear();
            strListConfig = new stringsMultyColumnsData[MAX_ROWS_AT_CONFIG_TAB];

            int i = 0, j = 0, index = 0;
            for (i = 0; i < PropertiesMarkup.MAX_PARAMS_IN_DATA_BLOCK; i++)
            // в цикле по всем параметрам
            {
                if (null == currentMarkup.propertiesMarkup.DataBlockParamsList[i].additionalInfo)
                    // пустой элемент - конец массива параметров, выходим 
                    break;

                if (currentMarkup.propertiesMarkup.DataBlockParamsList[i].listTab == listType.ConfigList)
                // если параметр для этого (ConfigList) списка
                {
                    index = currentMarkup.propertiesMarkup.DataBlockParamsList[i].positionInList;
                    if (index >= MAX_ROWS_AT_CONFIG_TAB)
                        continue;

                    strListConfig[index].FirstCol = SetStringLanguageAccording(currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramDescriptionRu,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramDescriptionEn);

                    strListConfig[index].SecondCol = SetStringEncodingTypeAccording(
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].encodingType,
                        epromFile,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramOffset,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramLength,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].bitMask,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].currDataBytes,
                        ref currentMarkup.propertiesMarkup.DataBlockParamsList[i].currCaseSelector);

                    strListConfig[index].ThirdCol = currentMarkup.propertiesMarkup.DataBlockParamsList[i].additionalInfo;

                    strListConfig[index].FourthCol = SetValueTypeAccording(
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramOffset,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramLength,
                        epromFile,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].currDataBytes);

                    strListConfig[index].index = i;

                    j++;
                }
            }

            for (i = 0; i < j; i++)
            {
                lvi = new ListViewItem(strListConfig[i].FirstCol);
                lvi.SubItems.Add(strListConfig[i].SecondCol);
                lvi.SubItems.Add(strListConfig[i].ThirdCol);

                listViewConfig.Items.Add(lvi);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void listViewConfig_KeyDown(object sender, KeyEventArgs e)
        {
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void listViewConfig_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            EditSelectedParameter();
        }

    }
}