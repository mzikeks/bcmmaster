﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using static BCMmaster.ChooseECUUC;
using BCMmaster.Sync;

namespace BCMmaster
{
    public partial class MainForm : Form
    {
        
        public static string originalBcmFilePath = "eeprom_ori_bcm.bin";
        public static string originalEcuFilePath = "eeprom_new_ecu.bin";
        public static string resultBcmFilePath = "eeprom_bcm_synch_ecu.bin";

        public byte[] SIGNATURE;

        FileStream fsOrigBCM;
        FileStream fsDestBCM;
        FileStream fsOrigECU;
        FileStream fsResultBCM;

        //FileStream fsBinFile;

        //BinaryReader brBinFile;
        //BinaryWriter bwBinFile;

        BinaryReader brOrigBCM;
        BinaryReader brOrigECU;
        BinaryReader brResultBCM;
        BinaryReader brDestBCM;
        BinaryWriter bwResultBCM;
        
        public const int BCM_BUFF_SIZE = 8192;

        public bool bEpromChanged = false;

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // нажатие кнопки "изменить VIN" - ввод нового с контролем правильности, запись нового в буфер epromFile[]
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void buttonVinChange_Click(object sender, EventArgs e)
        {
            DialogResult result;
            int currLength = textBoxVin.Text.Length;

            string msg = "";
            string hdr = "";

            string oldVIN = gVINStr;

            if (currLength != EpromMarkup.SIZE_VIN_STRING)
            {
                msg = SetStringLanguageAccording("VIN имеет неверной размер. Продолжить?",
                        "VIN size wrong. Continue?");
                hdr = SetStringLanguageAccording("Внимание", "Warning");

                result = MessageBox.Show(
                    msg,
                    hdr,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning);

                if (DialogResult.No == result)
                    return;
            }

            int i;

            for (i = 0; i < currLength; i++)
            {
                epromFile[EpromMarkup.OFFSET_VIN_STRING + i] = (byte)textBoxVin.Text[i];
            }

            for (i = currLength; i < EpromMarkup.SIZE_VIN_STRING; i++)
            {
                epromFile[EpromMarkup.OFFSET_VIN_STRING + i] = (byte)' ';
            }

            bEpromChanged = true;

            FillAllTabs();

            msg = SetStringLanguageAccording(
                "Завершено." +
                "\rСтарый VIN: " + oldVIN +
                "\rНовый  VIN: " + textBoxVin.Text +
                "\rНе забудьте выполнить пересчет контрольной суммы.",
                "Finished." +
                "\rOld VIN: " + oldVIN +
                "\rNew VIN: " + textBoxVin.Text +
                "\rDo not forget to recalculate checksum.");

            hdr = SetStringLanguageAccording("Продолжить", "Continue");

            MessageBox.Show(
                    msg,
                    hdr,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // для контроля текущей длины строки VIN  
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void textBoxVin_KeyUp(object sender, KeyEventArgs e)
        {
            if (textBoxVin.TextLength != EpromMarkup.SIZE_VIN_STRING)
                buttonVinChange.Enabled = false;
            else
                buttonVinChange.Enabled = true;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // разрешить ввод заглавных латинских, цифр и нажатие клавиш удаления символа
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void textBoxVin_KeyPress(object sender, KeyPressEventArgs e)
        {
            char symbol = e.KeyChar;

            if (!Char.IsDigit(symbol) && (symbol < 65 || symbol > 90) && (symbol != 8)) //backspace, letters, digits
            {
                e.Handled = true;
            }

        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        public bool SearchSignature(ref long AddrFound)
        {
            fsOrigECU.Seek(0, SeekOrigin.Begin);

            var signSize = SIGNATURE.Length;
            var currWord = new byte[signSize];

            for (int i = 0; i < signSize; i++)
                currWord[i] = brOrigECU.ReadByte();

            while (fsOrigECU.Position != fsOrigECU.Length)
            {
                for (int i = 0; i < signSize - 1; i++)
                    currWord[i] = currWord[i + 1];
                currWord[signSize - 1] = brOrigECU.ReadByte();

                if (Enumerable.SequenceEqual(SIGNATURE, currWord))
                {
                    AddrFound = (fsOrigECU.Position - signSize);
                    return true;
                }
            }

            return false;
        }
        
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        public void DoCloneBlocks()
        {
            byte[] srcBCMBuff = new byte[BCM_BUFF_SIZE];
            byte[] dstBCMBuff = new byte[BCM_BUFF_SIZE];

            string msg = "";
            string hdr = "";

            try
            {
                //
                // считать bcm файлы
                //
                srcBCMBuff = brOrigBCM.ReadBytes(BCM_BUFF_SIZE);
                dstBCMBuff = brDestBCM.ReadBytes(BCM_BUFF_SIZE);

                //
                // скопировать 0x80 – 0x60F из srcBCMBuff в dstBCMBuff
                //
                Array.Copy(srcBCMBuff, 0x80, dstBCMBuff, 0x80, (0x60F- 0x80 + 1));

                //
                // скопировать 0x1E60 – 0x1E79 из srcBCMBuff в dstBCMBuff
                //
                Array.Copy(srcBCMBuff, 0x1E60, dstBCMBuff, 0x1E60, (0x1E79 - 0x1E60 + 1));

                //
                // записать в result содержимое dst
                //
                bwResultBCM.Write(dstBCMBuff);

                bwResultBCM.Flush();
                bwResultBCM.Close();

                msg = SetStringLanguageAccording("Преобразование успешно завершено.",
                    "Modification successfully finished.");

                hdr = SetStringLanguageAccording("Продолжить",
                    "Continue");

                MessageBox.Show(
                        msg,
                        hdr,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
            }
            catch
            {
                msg = SetStringLanguageAccording("Проблема с доступом к файлам, или неверный формат файлов." +
                        "\rЗакройте другие программы, работающие с этими файлами," +
                        "\rили выберите другие исходные файлы.",
                        "There is a problem with files access, or wrong files format." +
                        "\rClose other programs that works with such files," +
                        "\rOr choose another files.");

                hdr = SetStringLanguageAccording("Ошибка",
                    "Error");

                MessageBox.Show(
                        msg,
                        hdr,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
            }

        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void buttonBCMClone_Click(object sender, EventArgs e)
        {
            if (null != fsOrigBCM) fsOrigBCM.Close();
            if (null != fsDestBCM) fsDestBCM.Close();
            if (null != fsResultBCM) fsResultBCM.Close();

            OpenFileDialog openFileDialog = new OpenFileDialog();
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            string msg = "";
            string hdr = "";

            try
            {
                openFileDialog.Filter = SetStringLanguageAccording("Бинарные файлы (*.bin)|*.bin|Все файлы (*.*)|*.*", "Binary files (*.bin)|*.bin|All files (*.*)|*.*");
                openFileDialog.DefaultExt = "bin";
                openFileDialog.FileName = "eeprom_src_bcm.bin";
                openFileDialog.Title = SetStringLanguageAccording("Выберите исходный BCM файл (из которого брать блок)", "Choose source BCM file (to get block from)");

                if (DEFAULT_BCM_SRC_PATH == gProgOptions.sBCMsrcDirPath)
                    openFileDialog.InitialDirectory = System.Environment.CurrentDirectory;
                else
                    openFileDialog.InitialDirectory = gProgOptions.sBCMsrcDirPath;

                if (DialogResult.OK == openFileDialog.ShowDialog() &&
                    openFileDialog.FileName.Length > 0)
                {
                    fsOrigBCM = new FileStream(openFileDialog.FileName, FileMode.Open);
                }
                else
                {
                    return;
                }

                gProgOptions.sBCMsrcDirPath = Path.GetDirectoryName(openFileDialog.FileName);
                WriteProgramOptions();

                openFileDialog.Filter = SetStringLanguageAccording("Бинарные файлы (*.bin)|*.bin|Все файлы (*.*)|*.*", "Binary files (*.bin)|*.bin|All files (*.*)|*.*");
                openFileDialog.DefaultExt = "bin";
                openFileDialog.FileName = "eeprom_dst_bcm.bin";
                openFileDialog.Title = SetStringLanguageAccording("Выберите BCM файл-приемник (в котором менять блок)", "Choose BCM file to modify (to put block to)");

                if (DEFAULT_BCM_SRC_PATH == gProgOptions.sBCMsrcDirPath)
                    openFileDialog.InitialDirectory = System.Environment.CurrentDirectory;
                else
                    openFileDialog.InitialDirectory = gProgOptions.sBCMsrcDirPath;

                if (DialogResult.OK == openFileDialog.ShowDialog() &&
                    openFileDialog.FileName.Length > 0)
                {
                    fsDestBCM = new FileStream(openFileDialog.FileName, FileMode.Open);
                }
                else
                {
                    return;
                }

                gProgOptions.sBCMsrcDirPath = Path.GetDirectoryName(openFileDialog.FileName);
                WriteProgramOptions();

                saveFileDialog.Filter = SetStringLanguageAccording("Бинарные файлы (*.bin)|*.bin|Все файлы (*.*)|*.*", "Binary files (*.bin)|*.bin|All files (*.*)|*.*");
                saveFileDialog.DefaultExt = "bin";
                saveFileDialog.FileName = "eeprom_result_clone.bin";
                saveFileDialog.Title = SetStringLanguageAccording("Выберите результирующий BCM файл (в который писать)", "Choose result BCM file (to save into)");

                if (DEFAULT_BCM_DST_PATH == gProgOptions.sBCMresDirPath)
                    saveFileDialog.InitialDirectory = gProgOptions.sBCMsrcDirPath;
                else
                    saveFileDialog.InitialDirectory = gProgOptions.sBCMresDirPath;

                if (DialogResult.OK == saveFileDialog.ShowDialog() &&
                    saveFileDialog.FileName.Length > 0)
                {
                    fsResultBCM = new FileStream(saveFileDialog.FileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                }
                else
                    return;

                gProgOptions.sBCMresDirPath = Path.GetDirectoryName(saveFileDialog.FileName);
                WriteProgramOptions();

                if (null != brOrigBCM) brOrigBCM.Close();
                if (null != brDestBCM) brDestBCM.Close();
                if (null != brResultBCM) brResultBCM.Close();
                if (null != bwResultBCM) bwResultBCM.Close();

                brOrigBCM = new BinaryReader(fsOrigBCM);
                brDestBCM = new BinaryReader(fsDestBCM);
                brResultBCM = new BinaryReader(fsResultBCM);
                bwResultBCM = new BinaryWriter(fsResultBCM);

                DoCloneBlocks();

                bwResultBCM.Close();
                fsResultBCM.Close();



            }
            catch
            {
                msg = SetStringLanguageAccording("Проблема с доступом к файлам, или неверный формат файлов." +
                        "\rЗакройте другие программы, работающие с этими файлами," +
                        "\rили выберите другие исходные файлы.",
                        "There is a problem with files access, or wrong files format." +
                        "\rClose other programs that works with such files," +
                        "\rOr choose another files.");

                hdr = SetStringLanguageAccording("Ошибка",
                    "Error");

                MessageBox.Show(
                        msg,
                        hdr,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
            }
          }


        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        public bool DoReplaceBytes(ECUType selectedECUType)
        {
            byte[] bufferBCM = new byte[BCM_BUFF_SIZE];

            long signature_offset = 0;

            string msg = "";
            string hdr = "";

            try
            {
                //
                // скопировать bcm файл
                //
                bufferBCM = brOrigBCM.ReadBytes(BCM_BUFF_SIZE);
                bwResultBCM.Write(bufferBCM);

                //
                // найти сигнатуру в файле eeprom_new_ecu.bin
                //
                if (!SearchSignature(ref signature_offset))
                {
                    msg = SetStringLanguageAccording($"Сигнатура {string.Join(" ", SIGNATURE)} не найдена в ECU файле",
                        $"Signature {string.Join(" ", SIGNATURE)} not found at ECU File");

                    hdr = SetStringLanguageAccording("Ошибка",
                        "Error");

                    MessageBox.Show(
                            msg,
                            hdr,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    return false;
                }
                //signature_offset += SIGNATURE.Length;

                if (selectedECUType == ECUType.Continental || selectedECUType == ECUType.None)
                    UpdateContinental(signature_offset);
                if (selectedECUType == ECUType.Bosch)
                    UpdateBosch(signature_offset);
                if (selectedECUType == ECUType.Delphi1)
                    UpdateDelphi1(signature_offset);
                if (selectedECUType == ECUType.Delphi2)
                    UpdateDelphi2(signature_offset);
                if (selectedECUType == ECUType.Marelli)
                    UpdateMarelli(signature_offset);
                if (selectedECUType == ECUType.NBC250CL)
                    UpdateNBC250CL(signature_offset);
                if (selectedECUType == ECUType.NBC250A)
                    UpdateNBC250A(signature_offset);

                if (SyncControlSum_checkBox.Checked)
                {
                    if (selectedECUType == ECUType.NBC250CL)
                        UpdateControlSum3Blocks();
                    else
                        UpdateControlSum2Blocks();
                }   

                msg = SetStringLanguageAccording("Преобразование успешно завершено.",
                    "Modification successfully finished.");

                hdr = SetStringLanguageAccording("Продолжить",
                    "Continue");

                MessageBox.Show(
                        msg,
                        hdr,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                return true;
            }
            catch
            {
                msg = SetStringLanguageAccording("Проблема с доступом к файлам, или неверный формат файлов." +
                        "\rЗакройте другие программы, работающие с этими файлами," +
                        "\rили выберите другие исходные файлы.",
                        "There is a problem with files access, or wrong files format." +
                        "\rClose other programs that works with such files," +
                        "\rOr choose another files.");

                hdr = SetStringLanguageAccording("Ошибка",
                    "Error");

                MessageBox.Show(
                        msg,
                        hdr,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                return false;
            }

        }

        private void UpdateSignature(ECUType selectedECUType)
        {
            switch (selectedECUType)
            {
                case (ECUType.Continental):
                    SIGNATURE = new byte[] { 0x46, 0x0A };
                    break;
                case (ECUType.Bosch):
                    SIGNATURE = new byte[] { 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00 };
                    break;
                case (ECUType.Delphi1):
                    SIGNATURE = new byte[] { 0x05, 0x03, 0x00, 0x0a };
                    break;
                case (ECUType.Delphi2):
                    SIGNATURE = new byte[] { 0x55, 0xaa, 0x0a, 0x02, 0x0a, 0x00 };
                    break;
                case (ECUType.Marelli):
                    SIGNATURE = new byte[] { 0x39, 0x99 };
                    break;
                case (ECUType.NBC250A):
                    SIGNATURE = new byte[] { };
                    break;
                case (ECUType.NBC250CL):
                    SIGNATURE = new byte[] { 0xa5, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                             0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                             0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                             0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                             0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                             0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x4e, 0x4f };
                    break;
                default:
                    SIGNATURE = new byte[] { 0x46, 0x0A };
                    break;
            }
        }

        private void UpdateControlSum2Blocks()
        {
            uint CHECKSUMM_LENGTH = 0x5D;

            uint CHECKSUMM_ADDR1 = 0x01A2;
            uint CHECKSUMM_ADDR2 = 0x0202;

            Int16 checkSum = 0;
            byte hiByte = 0;
            byte loByte = 0;

            // пересчитать контрольную сумму 0x0144-0x01a1
            fsResultBCM.Seek(0x0144, SeekOrigin.Begin);

            for (int i = 0; i < CHECKSUMM_LENGTH; i++)
                checkSum += brResultBCM.ReadByte();

            // swap bytes
            hiByte = (byte)(checkSum >> 8);
            loByte = (byte)(checkSum & 0xFF);
            checkSum = (Int16)((loByte << 8) | hiByte);

            // записать контрольную сумму 
            fsResultBCM.Seek(CHECKSUMM_ADDR1, SeekOrigin.Begin);
            bwResultBCM.Write(checkSum);
            fsResultBCM.Seek(CHECKSUMM_ADDR2, SeekOrigin.Begin);
            bwResultBCM.Write(checkSum);
        }

        private void UpdateControlSum3Blocks()
        {
            uint CHECKSUMM_LENGTH = 0x176 - 0x11F + 1;

            uint CHECKSUMM_ADDR1 = 0x7C0;
            uint CHECKSUMM_ADDR2 = 0x7C2;

            Int16 checkSum = 0;
            byte hiByte;
            byte loByte;


            // пересчитать контрольную сумму 1: 0x227 - 0x269
            fsResultBCM.Seek(0x11F, SeekOrigin.Begin);
            for (int i = 0; i < CHECKSUMM_LENGTH; i++)
                checkSum += brResultBCM.ReadByte();

            // записать контрольную сумму 
            fsResultBCM.Seek(CHECKSUMM_ADDR1, SeekOrigin.Begin);
            bwResultBCM.Write(checkSum);
            fsResultBCM.Seek(CHECKSUMM_ADDR2, SeekOrigin.Begin);
            bwResultBCM.Write(checkSum);
        }

        private void UpdateContinental(long signature_offset)
        {
            uint PLUS14_OFFSET = 14;
            uint PLUS16_OFFSET = 16;

            uint ADDR_0x194 = 0x0194;
            uint ADDR_0x1F4 = 0x01F4;
            uint ADDR_0x144 = 0x0144;
            uint ADDR_0x1A4 = 0x01A4;

            byte[] wordToReadBytes = new byte[2];
            byte[] dwordToReadBytes = new byte[4];

            // скопировать из eprom_bcm_synch_ecu.bin по смещению 14 + sign_460A_offset
            // по адресу Addr 0x0194 два байта с инверсией порядка
            fsOrigECU.Seek(signature_offset + PLUS14_OFFSET, SeekOrigin.Begin);
            wordToReadBytes = brOrigECU.ReadBytes(wordToReadBytes.Length);

            fsResultBCM.Seek(ADDR_0x194, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes[1]);
            fsResultBCM.WriteByte(wordToReadBytes[0]);

            // скопировать из eprom_bcm_synch_ecu.bin по смещению 14 + sign_460A_offset
            // по адресу Addr 0x01F4 два байта с инверсией порядка
            fsResultBCM.Seek(ADDR_0x1F4, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes[1]);
            fsResultBCM.WriteByte(wordToReadBytes[0]);

            // скопировать из eprom_bcm_synch_ecu.bin по смещению 16 + sign_460A_offset
            // по адресу Addr 0x0144 4 байта с инверсией порядка
            fsOrigECU.Seek(signature_offset + PLUS16_OFFSET, SeekOrigin.Begin);
            dwordToReadBytes = brOrigECU.ReadBytes(dwordToReadBytes.Length);

            fsResultBCM.Seek(ADDR_0x144, SeekOrigin.Begin);
            fsResultBCM.WriteByte(dwordToReadBytes[3]);
            fsResultBCM.WriteByte(dwordToReadBytes[2]);
            fsResultBCM.WriteByte(dwordToReadBytes[1]);
            fsResultBCM.WriteByte(dwordToReadBytes[0]);

            // скопировать из eprom_bcm_synch_ecu.bin по смещению 16 + sign_460A_offset
            // по адресу Addr 0x01A4 4 байта с инверсией порядка
            fsResultBCM.Seek(ADDR_0x1A4, SeekOrigin.Begin);
            fsResultBCM.WriteByte(dwordToReadBytes[3]);
            fsResultBCM.WriteByte(dwordToReadBytes[2]);
            fsResultBCM.WriteByte(dwordToReadBytes[1]);
            fsResultBCM.WriteByte(dwordToReadBytes[0]);
        }

        private void UpdateBosch(long signature_offset)
        {
            uint PLUS_OFFSET = 28;

            uint ADDR1 = 0x0144;
            uint ADDR2 = 0x0194; 
            uint ADDR3 = 0x1A4;
            uint ADDR4 = 0x1F4;

            byte[] wordToReadBytes6 = new byte[6];

            fsOrigECU.Seek(signature_offset + PLUS_OFFSET, SeekOrigin.Begin);
            wordToReadBytes6 = brOrigECU.ReadBytes(wordToReadBytes6.Length);

            //Копируем 1
            fsResultBCM.Seek(ADDR1, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[0]);
            fsResultBCM.WriteByte(wordToReadBytes6[1]);
            fsResultBCM.WriteByte(wordToReadBytes6[2]);
            fsResultBCM.WriteByte(wordToReadBytes6[3]);

            // Копируем 2
            fsResultBCM.Seek(ADDR2, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[4]);
            fsResultBCM.WriteByte(wordToReadBytes6[5]);

            //Копируем 3
            fsResultBCM.Seek(ADDR3, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[0]);
            fsResultBCM.WriteByte(wordToReadBytes6[1]);
            fsResultBCM.WriteByte(wordToReadBytes6[2]);
            fsResultBCM.WriteByte(wordToReadBytes6[3]);

            // Копируем 4
            fsResultBCM.Seek(ADDR4, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[4]);
            fsResultBCM.WriteByte(wordToReadBytes6[5]);
        }

        private void UpdateDelphi1(long signature_offset)
        {
            uint PLUS_OFFSET = 7;

            uint ADDR1 = 0x0144;
            uint ADDR2 = 0x0194; 
            uint ADDR3 = 0x1A4;
            uint ADDR4 = 0x1F4;

            byte[] wordToReadBytes6 = new byte[6];

            fsOrigECU.Seek(signature_offset + PLUS_OFFSET, SeekOrigin.Begin);
            wordToReadBytes6 = brOrigECU.ReadBytes(wordToReadBytes6.Length);

            //Копируем 1
            fsResultBCM.Seek(ADDR1, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[5]);
            fsResultBCM.WriteByte(wordToReadBytes6[4]);
            fsResultBCM.WriteByte(wordToReadBytes6[3]);
            fsResultBCM.WriteByte(wordToReadBytes6[2]);

            // Копируем 2
            fsResultBCM.Seek(ADDR2, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[1]);
            fsResultBCM.WriteByte(wordToReadBytes6[0]);
            
            //Копируем 3
            fsResultBCM.Seek(ADDR3, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[5]);
            fsResultBCM.WriteByte(wordToReadBytes6[4]);
            fsResultBCM.WriteByte(wordToReadBytes6[3]);
            fsResultBCM.WriteByte(wordToReadBytes6[2]);

            // Копируем 4
            fsResultBCM.Seek(ADDR4, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[1]);
            fsResultBCM.WriteByte(wordToReadBytes6[0]);
        }

        private void UpdateDelphi2(long signature_offset)
        {
            uint PLUS_OFFSET = 17;

            uint ADDR1 = 0x0144;
            uint ADDR2 = 0x0194;
            uint ADDR3 = 0x1A4;
            uint ADDR4 = 0x1F4;

            byte[] wordToReadBytes6 = new byte[6];

            fsOrigECU.Seek(signature_offset + PLUS_OFFSET, SeekOrigin.Begin);
            wordToReadBytes6 = brOrigECU.ReadBytes(wordToReadBytes6.Length);

            //Копируем 1
            fsResultBCM.Seek(ADDR1, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[5]);
            fsResultBCM.WriteByte(wordToReadBytes6[4]);
            fsResultBCM.WriteByte(wordToReadBytes6[3]);
            fsResultBCM.WriteByte(wordToReadBytes6[2]);

            // Копируем 2
            fsResultBCM.Seek(ADDR2, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[1]);
            fsResultBCM.WriteByte(wordToReadBytes6[0]);

            //Копируем 3
            fsResultBCM.Seek(ADDR3, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[5]);
            fsResultBCM.WriteByte(wordToReadBytes6[4]);
            fsResultBCM.WriteByte(wordToReadBytes6[3]);
            fsResultBCM.WriteByte(wordToReadBytes6[2]);

            // Копируем 4
            fsResultBCM.Seek(ADDR4, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[1]);
            fsResultBCM.WriteByte(wordToReadBytes6[0]);
        }

        private void UpdateMarelli(long signature_offset)
        {
            uint PLUS_OFFSET = 2;

            uint ADDR1 = 0x0144;
            uint ADDR2 = 0x0194;
            uint ADDR3 = 0x1A4;
            uint ADDR4 = 0x1F4;

            byte[] wordToReadBytes6 = new byte[6];

            fsOrigECU.Seek(signature_offset + PLUS_OFFSET, SeekOrigin.Begin);
            wordToReadBytes6 = brOrigECU.ReadBytes(wordToReadBytes6.Length);

            //Копируем 1
            fsResultBCM.Seek(ADDR1, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[5]);
            fsResultBCM.WriteByte(wordToReadBytes6[4]);
            fsResultBCM.WriteByte(wordToReadBytes6[3]);
            fsResultBCM.WriteByte(wordToReadBytes6[2]);

            // Копируем 2
            fsResultBCM.Seek(ADDR2, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[1]);
            fsResultBCM.WriteByte(wordToReadBytes6[0]);

            //Копируем 3
            fsResultBCM.Seek(ADDR3, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[5]);
            fsResultBCM.WriteByte(wordToReadBytes6[4]);
            fsResultBCM.WriteByte(wordToReadBytes6[3]);
            fsResultBCM.WriteByte(wordToReadBytes6[2]);

            // Копируем 4
            fsResultBCM.Seek(ADDR4, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[1]);
            fsResultBCM.WriteByte(wordToReadBytes6[0]);
        }

        private void UpdateNBC250CL(long signature_offset)
        {
            int PLUS_OFFSET = -6;

            uint ADDR1 = 0x11F;
            uint ADDR2 = 0x16F;
            uint ADDR3 = 0x177;
            uint ADDR4 = 0x1C7;

            byte[] wordToReadBytes6 = new byte[6];

            fsOrigECU.Seek(signature_offset + PLUS_OFFSET, SeekOrigin.Begin);
            wordToReadBytes6 = brOrigECU.ReadBytes(wordToReadBytes6.Length);

            //Копируем 1
            fsResultBCM.Seek(ADDR1, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[0]);
            fsResultBCM.WriteByte(wordToReadBytes6[1]);
            fsResultBCM.WriteByte(wordToReadBytes6[2]);
            fsResultBCM.WriteByte(wordToReadBytes6[3]);

            // Копируем 2
            fsResultBCM.Seek(ADDR2, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[4]);
            fsResultBCM.WriteByte(wordToReadBytes6[5]);

            //Копируем 3
            fsResultBCM.Seek(ADDR3, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[0]);
            fsResultBCM.WriteByte(wordToReadBytes6[1]);
            fsResultBCM.WriteByte(wordToReadBytes6[2]);
            fsResultBCM.WriteByte(wordToReadBytes6[3]);

            // Копируем 4
            fsResultBCM.Seek(ADDR4, SeekOrigin.Begin);
            fsResultBCM.WriteByte(wordToReadBytes6[4]);
            fsResultBCM.WriteByte(wordToReadBytes6[5]);
        }

        private void UpdateNBC250A(long signature_offset)
        {
            
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void buttonECUBCM_Click(object sender, EventArgs e)
        {
            //
            // выбор файлов
            //

            if (null != fsOrigBCM) fsOrigBCM.Close();
            if (null != fsOrigECU) fsOrigECU.Close();
            if (null != fsResultBCM) fsResultBCM.Close();

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = System.Environment.CurrentDirectory;

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = System.Environment.CurrentDirectory;

            try
            {
                openFileDialog.Filter = "Бинарные файлы (*.bin)|*.bin|Все файлы (*.*)|*.*";
                openFileDialog.DefaultExt = "bin";
                openFileDialog.FileName = "eeprom_ori_bcm.bin";
                openFileDialog.Title = "Выберите исходный BCM файл";

                if (DEFAULT_BCM_SRC_PATH == gProgOptions.sBCMsrcDirPath)
                    openFileDialog.InitialDirectory = System.Environment.CurrentDirectory;
                else
                    openFileDialog.InitialDirectory = gProgOptions.sBCMsrcDirPath;

                if (DialogResult.OK == openFileDialog.ShowDialog() && openFileDialog.FileName.Length > 0)
                    fsOrigBCM = new FileStream(openFileDialog.FileName, FileMode.Open);
                else
                    return;

                long lengthBCM = new System.IO.FileInfo(openFileDialog.FileName).Length;

                gProgOptions.sBCMsrcDirPath = Path.GetDirectoryName(openFileDialog.FileName);
                WriteProgramOptions();

                openFileDialog.Filter = "Бинарные файлы (*.bin)|*.bin|Все файлы (*.*)|*.*";
                openFileDialog.DefaultExt = "bin";
                openFileDialog.FileName = "eeprom_new_ecu.bin";
                openFileDialog.Title = "Выберите исходный ECU файл";

                if (DEFAULT_ECU_PATH == gProgOptions.sECUsrcDirPath)
                    openFileDialog.InitialDirectory = gProgOptions.sBCMsrcDirPath;
                else
                    openFileDialog.InitialDirectory = gProgOptions.sECUsrcDirPath;

                if (DialogResult.OK == openFileDialog.ShowDialog() && openFileDialog.FileName.Length > 0)
                    fsOrigECU = new FileStream(openFileDialog.FileName, FileMode.Open);
                else
                    return;

                gProgOptions.sECUsrcDirPath = Path.GetDirectoryName(openFileDialog.FileName);
                WriteProgramOptions();
                
                ECUType selectedECUType;

                if (lengthBCM == 2048)
                {
                    var selectECUForm = new ChooseECUNBC250Form(gProgOptions.bEnglishInterface);
                    if (DialogResult.OK != selectECUForm.ShowDialog())
                        return;
                    selectedECUType = selectECUForm.selectedECU;
                }
                
                else 
                {
                    var selectECUForm = new ChooseECUForm(gProgOptions.bEnglishInterface);
                    if (DialogResult.OK != selectECUForm.ShowDialog())
                        return;
                    selectedECUType = selectECUForm.selectedECU;
                }

                UpdateSignature(selectedECUType);

                saveFileDialog.Filter = "Бинарные файлы (*.bin)|*.bin|Все файлы (*.*)|*.*";
                saveFileDialog.DefaultExt = "bin";
                saveFileDialog.FileName = "eeprom_result_ecubcm.bin";
                saveFileDialog.Title = "Выберите результирующий BCM файл";

                if (DEFAULT_BCM_DST_PATH == gProgOptions.sBCMresDirPath)
                    saveFileDialog.InitialDirectory = gProgOptions.sBCMsrcDirPath;
                else
                    saveFileDialog.InitialDirectory = gProgOptions.sBCMresDirPath;

                if (DialogResult.OK == saveFileDialog.ShowDialog() && saveFileDialog.FileName.Length > 0)
                {
                    File.WriteAllText(saveFileDialog.FileName, "");
                    fsResultBCM = new FileStream(saveFileDialog.FileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                }    
                else
                    return;

                gProgOptions.sBCMresDirPath = Path.GetDirectoryName(saveFileDialog.FileName);
                WriteProgramOptions();

                if (null != brOrigBCM) brOrigBCM.Close();
                if (null != brOrigECU) brOrigECU.Close();
                if (null != brResultBCM) brResultBCM.Close();
                if (null != bwResultBCM) bwResultBCM.Close();

                brOrigBCM = new BinaryReader(fsOrigBCM);
                brOrigECU = new BinaryReader(fsOrigECU);
                brResultBCM = new BinaryReader(fsResultBCM);
                bwResultBCM = new BinaryWriter(fsResultBCM);

                resultBcmFilePath = saveFileDialog.FileName;

                DoReplaceBytes(selectedECUType);

                bwResultBCM.Flush();
                fsResultBCM.Flush();
                bwResultBCM.Close();
                fsResultBCM.Close();
            }
            catch
            {
                MessageBox.Show(
                        "Проблема с доступом к файлам, или неверный формат файлов." +
                        "\rЗакройте другие программы, работающие с этими файлами," +
                        "\rили выберите другие исходные файлы.",
                        "Ошибка",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void CopyModifiedBlockAndUpdateCheckSum()
        {
            Int16 checkSumCalculated = 0;

            CalcCheckSumInEpromFileBuff(EpromMarkup.OFFSET_VIN, EpromMarkup.SIZE_VIN, ref checkSumCalculated);
            SetCheckSumIntoEpromFileBuff(EpromMarkup.OFFSET_VIN_CHECKSUM, checkSumCalculated);

            CalcCheckSumInEpromFileBuff(currentMarkup.OFFSET_DATA_BLOCK1, currentMarkup.SIZE_DATA_BLOCK, ref checkSumCalculated);
            SetCheckSumIntoEpromFileBuff(currentMarkup.OFFSET_DATA_BLOCK1_CHECKSUM, checkSumCalculated);

            Array.Copy(epromFile, currentMarkup.OFFSET_DATA_BLOCK1, epromFile, currentMarkup.OFFSET_DATA_BLOCK2, currentMarkup.SIZE_DATA_BLOCK + 3);
            Array.Copy(epromFile, currentMarkup.OFFSET_DATA_BLOCK1, epromFile, currentMarkup.OFFSET_DATA_BLOCK3, currentMarkup.SIZE_DATA_BLOCK + 3);

            bEpromChanged = true;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void buttonCheckSum_Click(object sender, EventArgs e)
        {
            Int16 checkSumCalculated = 0;

            if (checkBoxVIN.Checked)
            {
                CalcCheckSumInEpromFileBuff(EpromMarkup.OFFSET_VIN, EpromMarkup.SIZE_VIN, ref checkSumCalculated);
                SetCheckSumIntoEpromFileBuff(EpromMarkup.OFFSET_VIN_CHECKSUM , checkSumCalculated);
                bEpromChanged = true;
            }

            if (checkBoxBlock1.Checked && currentMarkup.OFFSET_DATA_BLOCK1 > 0)
            {
                CalcCheckSumInEpromFileBuff(currentMarkup.OFFSET_DATA_BLOCK1, currentMarkup.SIZE_DATA_BLOCK, ref checkSumCalculated);
                SetCheckSumIntoEpromFileBuff(currentMarkup.OFFSET_DATA_BLOCK1_CHECKSUM, checkSumCalculated);
                bEpromChanged = true;
            }

            if (checkBoxBlock2.Checked && currentMarkup.OFFSET_DATA_BLOCK2 > 0)
            {
                CalcCheckSumInEpromFileBuff(currentMarkup.OFFSET_DATA_BLOCK2, currentMarkup.SIZE_DATA_BLOCK, ref checkSumCalculated);
                SetCheckSumIntoEpromFileBuff(currentMarkup.OFFSET_DATA_BLOCK2_CHECKSUM, checkSumCalculated);
                bEpromChanged = true;
            }

            if (checkBoxBlock3.Checked && currentMarkup.OFFSET_DATA_BLOCK3 > 0)
            {
                CalcCheckSumInEpromFileBuff(currentMarkup.OFFSET_DATA_BLOCK3, currentMarkup.SIZE_DATA_BLOCK, ref checkSumCalculated);
                SetCheckSumIntoEpromFileBuff(currentMarkup.OFFSET_DATA_BLOCK3_CHECKSUM, checkSumCalculated);
                bEpromChanged = true;
            }

            string msg = SetStringLanguageAccording(
                "Завершено. Не забудьте сохранить файл.",
                "Finished. Do not forget to save file");

            string hdr = SetStringLanguageAccording("Продолжить", "Continue");

            MessageBox.Show(
                    msg,
                    hdr,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }
    }
}
