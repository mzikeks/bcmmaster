﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BCMmaster.MarkupParsing;


namespace BCMmaster
{
    public partial class MainForm : Form
    {       

        //
        // то, что отображается в списках. поле index не отображается, а указывает на позицию дескриптора в массиве
        //
        public struct stringsMultyColumnsData
        {
            public string FirstCol;
            public string SecondCol;
            public string ThirdCol;
            public string FourthCol;
            public string FifthCol;
            public int index;
        };

        enum CarModels
        {
            Peugeot_Boxer,
            Iveco_Daily,
            Fiat_Ducato,
            Citroen_Jumper,
            Unknown
        }

        string[] CarModelsStrList = {
            "Peugeot Boxer",
            "Iveco_Daily",
            "Fiat Ducato",
            "Citroen Jumper",
            "Unknown" };

        CarModels currentCarModel = CarModels.Unknown;


        public byte[] epromFile = new byte[EpromMarkup.MAX_EPROM_FILE_LENGTH];

        public static string gVINStr = "n/a";
        public static string gModelStr = "n/a";

        public static string currFileName = "defaultEprom.bin";

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //------------------------------------------------------------------------------------------------------------------------------------------------
        void CalcCheckSumInEpromFileBuff(
            int blockStartAddr,
            int blockSize,
            ref Int16 checkSum)
        {
            checkSum = 0;

            int blockEndAddr = blockStartAddr + blockSize;

            if (blockEndAddr >= EpromMarkup.MAX_EPROM_FILE_LENGTH)
            {
                checkSum = 0;
                return;
            }

            byte hiByte = 0;
            byte loByte = 0;

            for (int i = blockStartAddr; i < blockEndAddr; i++)
            {
                checkSum += epromFile[i];
            }

            //
            // swap bytes
            //
            hiByte = (byte)(checkSum >> 8);
            loByte = (byte)(checkSum & 0xFF);
            checkSum = (Int16)((loByte << 8) | hiByte);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void SetCheckSumIntoEpromFileBuff(
            int offset,
            Int16 checkSum)
        {
            byte hiByte = 0;
            byte loByte = 0;

            hiByte = (byte)(checkSum >> 8);
            loByte = (byte)(checkSum & 0xFF);

            epromFile[offset] = loByte;
            epromFile[offset + 1] = hiByte;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // загрузить eprom файл в память
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        bool ReadEpromFile(string path)
        {
            Stream srcStream = null;

            try
            {
                srcStream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

                BinaryReader brFile = new BinaryReader(srcStream);
                brFile.Read(epromFile, 0, EpromMarkup.MAX_EPROM_FILE_LENGTH);
            }
            catch
            {
                if (null != srcStream) srcStream.Close();
                return false;
            }

            return true;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        bool PrepareAndTryToReadEpromFile(bool bOpenNewFile)
        {
            bool bReadOK = false;

            if (!bOpenNewFile &&
                null != gProgOptions.sEpromDirPath &&
                null != gProgOptions.sEpromFilePath)
            {
                bReadOK = ReadEpromFile(gProgOptions.sEpromFilePath);
                currFileName = gProgOptions.sEpromFilePath;
                return bReadOK;
            }

            string s = gProgOptions.sEpromDirPath;

            if (null == s)
                s = Directory.GetCurrentDirectory();

            FileDialog fDlg = new OpenFileDialog();

            fDlg.InitialDirectory = s;

            //
            // загрузить епром в память
            //
            if (fDlg.ShowDialog() == DialogResult.OK)
            {
                currFileName = fDlg.FileName;
                bReadOK = ReadEpromFile(currFileName);
            }

            gProgOptions.sEpromFilePath = currFileName;
            gProgOptions.sEpromDirPath = Path.GetDirectoryName(currFileName);

            return bReadOK;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        Int16 GetShortFromByteBuff(byte[] bufferBase, int offset)
        {
            try
            {
                byte hiByte = 0;
                byte loByte = 0;

                hiByte = bufferBase[offset];
                loByte = bufferBase[offset + 1];

                return ((Int16)((loByte << 8) | hiByte));
            }
            catch
            {
                return 0;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        Int16 GetStringFromByteBuff(byte[] bufferBase, int offset)
        {
            try
            {
                byte hiByte = 0;
                byte loByte = 0;

                hiByte = bufferBase[offset];
                loByte = bufferBase[offset + 1];

                return ((Int16)((loByte << 8) | hiByte));
            }
            catch
            {
                return 0;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        bool CheckEpromIntegrity()
        {
            bool allOK = true;

            Int16 checkSumCalculated = 0;
            Int16 checkSumFromFile = 0;

            //
            // check VIN
            //
            CalcCheckSumInEpromFileBuff(EpromMarkup.OFFSET_VIN, EpromMarkup.SIZE_VIN, ref checkSumCalculated);
            checkSumFromFile = GetShortFromByteBuff(epromFile, EpromMarkup.OFFSET_VIN_CHECKSUM);

            string msg;
            string hdr;

            if (checkSumFromFile != checkSumCalculated)
            {
                msg = SetStringLanguageAccording("VIN имеет неправильную контрольную сумму.\rВозможно файл поврежден.",
                        "VIN checksum wrong. File corrupted.");
                hdr = SetStringLanguageAccording("Ошибка", "Error");

                MessageBox.Show(
                    msg,
                    hdr,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

                allOK = false;
            }


            SetCurrentCarModel();
            //
            // check DataBlock1
            //
            if (currentMarkup.OFFSET_DATA_BLOCK1 > 0)
            {
                CalcCheckSumInEpromFileBuff(currentMarkup.OFFSET_DATA_BLOCK1, currentMarkup.SIZE_DATA_BLOCK, ref checkSumCalculated);
                checkSumFromFile = GetShortFromByteBuff(epromFile, currentMarkup.OFFSET_DATA_BLOCK1_CHECKSUM);

                if (checkSumFromFile != checkSumCalculated)
                {
                    msg = SetStringLanguageAccording("Блок данных №1 имеет неправильную контрольную сумму\rВозможно файл поврежден.",
                        "DataBlock1 checksum wrong. File corrupted.");
                    hdr = SetStringLanguageAccording("Ошибка", "Error");

                    MessageBox.Show(
                            msg,
                            hdr,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);

                    allOK = false;
                }
            }


            //
            // check DataBlock2
            //
            if (currentMarkup.OFFSET_DATA_BLOCK2 > 0)
            {
                CalcCheckSumInEpromFileBuff(currentMarkup.OFFSET_DATA_BLOCK2, currentMarkup.SIZE_DATA_BLOCK, ref checkSumCalculated);
                checkSumFromFile = GetShortFromByteBuff(epromFile, currentMarkup.OFFSET_DATA_BLOCK2_CHECKSUM);

                if (checkSumFromFile != checkSumCalculated)
                {
                    msg = SetStringLanguageAccording("Блок данных №2 имеет неправильную контрольную сумму\rВозможно файл поврежден.",
                        "DataBlock2 checksum wrong. File corrupted.");
                    hdr = SetStringLanguageAccording("Ошибка", "Error");

                    MessageBox.Show(
                            msg,
                            hdr,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);

                    allOK = false;
                }
            }


            //
            // check DataBlock3
            //
            if (currentMarkup.OFFSET_DATA_BLOCK3 > 0)
            {
                CalcCheckSumInEpromFileBuff(currentMarkup.OFFSET_DATA_BLOCK3, currentMarkup.SIZE_DATA_BLOCK, ref checkSumCalculated);
                checkSumFromFile = GetShortFromByteBuff(epromFile, currentMarkup.OFFSET_DATA_BLOCK3_CHECKSUM);

                if (checkSumFromFile != checkSumCalculated)
                {
                    msg = SetStringLanguageAccording("Блок данных №3 имеет неправильную контрольную сумму\rВозможно файл поврежден.",
                        "DataBlock3 checksum wrong. File corrupted.");
                    hdr = SetStringLanguageAccording("Ошибка", "Error");

                    MessageBox.Show(
                            msg,
                            hdr,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);

                    allOK = false;
                }
            }


            return allOK;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        string SetValueTypeAccording(
            int offset,
            int length,
            byte[] inBuffer,
            byte[] outBuffer)
        {
            string str = "";


            for (int i = 0; i < length; i++)
            {
                outBuffer[i] = inBuffer[offset + i];
            }

            str = BitConverter.ToString(outBuffer);

            return str;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        string SetStringEncodingTypeAccording(
            paramEncoding paramEncodingType,
            byte[] buffer,
            int offset,
            int length,
            int mask,
            byte[] outBuffer,
            ref int caseSelector
            )
        {
            string str = "";

            byte b = epromFile[offset];
            byte hb = epromFile[offset + 1];
            UInt16 w = 0;

            Array.Copy(buffer, offset, outBuffer, 0, length);

            switch (paramEncodingType)
            {
                case (paramEncoding.String):

                    str = Encoding.UTF8.GetString(buffer, offset, length);
                    break;

                case (paramEncoding.Bits):

                    str = BitConverter.ToString(buffer, offset, length);
                    break;

                case (paramEncoding.maskBits):

                    if ((0x341 == offset) || (0x344 == offset))
                        if (0 == b)
                            b = epromFile[offset + 1];

                    if (0 != (b & mask))
                    {
                        str = SetStringLanguageAccording("Имеется", "Present");
                        caseSelector = 1;
                    }
                    else
                    {
                        str = SetStringLanguageAccording("Отсутствует", "Absent");
                        caseSelector = 2;
                    }

                    break;

                case (paramEncoding.Word):

                    w = (UInt16)((b << 8) | hb);
                    str = w.ToString();

                    break;

                case (paramEncoding.WordInverted):

                    w = (UInt16)((hb << 8) | b);
                    str = w.ToString();

                    break;

                case (paramEncoding.TwoBit):

                    str = b.ToString();

                    switch (offset)
                    {
                        case (0x29C):

                            if (0 != (0x02 & b))
                            {
                                str = SetStringLanguageAccording("Механическая коробка передач PSA MLGU 6M", "Manual transmission PSA MLGU 6M");
                                caseSelector = 1;
                            }
                            else if (0 != (0x04 & b))
                            {
                                str = SetStringLanguageAccording("Механическая коробка передач с электронным управлением MTA M40 / M38 – 6M", "Manual transmission MTA M40 / M38 – 6M");
                                caseSelector = 2;
                            }

                            break;

                        case (0x2A1):

                            if (0 != (0x02 & b))
                            {
                                str = SetStringLanguageAccording("Длинная колесная база: от 3.8м до 4.05м", "Long base 3.8 to 4.05m");
                                caseSelector = 1;
                            }
                            else if (0 != (0x04 & b))
                            {
                                str = SetStringLanguageAccording("Сверхдлинная колесная база: 4.05м", "Extended long base 4.05m");
                                caseSelector = 2;
                            }

                            break;
                    }

                    break;

                case (paramEncoding.Byte):

                    str = b.ToString();

                    switch (offset)
                    {
                        //DataBlockParamsList[45] = new DataParamDescriptor(0x238, 1, 0x40, "Количество и расположение задних противотуманных фонарей", "Rear fog lights number and position", paramEncoding.Byte, listType.EquipmentList, 10, ""); // 2 противотуманных фонаря (слева и справа)
                        case (0x238):

                            if (0x40 == (0x40 & b))
                            {
                                str = SetStringLanguageAccording("Два противотуманных фонаря (слева и справа)", "Two pts left and right");
                                caseSelector = 1;
                            }
                            else
                            {
                                str = SetStringLanguageAccording("Один противотуманный фонарь", "One pt");
                                caseSelector = 2;
                            }

                            break;

                        case (0x26F):

                            if (2 == b)
                            {
                                str = SetStringLanguageAccording("Тип 2 - 90л", "Type 2 - 90l");
                                caseSelector = 1;
                            }
                            else if (3 == b)
                            {
                                str = SetStringLanguageAccording("Тип 3 - 120л", "Type 3 - 120l");
                                caseSelector = 2;
                            }
                            else
                            {
                                caseSelector = 2;
                                str = SetStringLanguageAccording("Тип " + b.ToString(), "Type " + b.ToString());
                            }

                            break;

                        case (0x29C):

                            if (0x10 == (0x10 & b))
                            {
                                str = SetStringLanguageAccording("Антиблокировочная система (ABS)", "ABS");
                                caseSelector = 1;
                            }
                            else if ((0x30 == (0x30 & b)) || (0x20 == (0x30 & b)))
                            {
                                str = SetStringLanguageAccording("ESP c функцией помощи при начале движения на склоне и наличием кнопки противобуксовочной системы", "ESP with hill assistant and traction button control");
                                caseSelector = 2;
                            }
                            else if (0 == (0x40 & b))
                            {
                                str = SetStringLanguageAccording("ESP c функцией помощи при начале движения на склоне и наличием кнопки повышения тягового усилия", "ESP with hill assistant and power button control");
                                caseSelector = 3;
                            }

                            //else
                            //    str = SetStringLanguageAccording("Тип ABS/ESP " + b.ToString(), "ABS/ESP type " + b.ToString());

                            break;

                        case (0x29D):

                            if ((0 != (b & mask)))
                            {
                                str = SetStringLanguageAccording("Справа", "Right side");
                                caseSelector = 1;
                            }
                            else
                            {
                                str = SetStringLanguageAccording("Слева", "Left side");
                                caseSelector = 2;
                            }

                            break;

                        case (0x29E):
                            
                            if (0x70 == (0x70 & b)) 
                            {
                                str = SetStringLanguageAccording("Двигатель SOFIM 3.0 JTD мощностью 180 л.с.", "Engine - SOFIM 3.0 JTD 180hp");
                                caseSelector = 1;
                            }
                            else if (0x40 == (0x70 & b)) 
                            {
                                str = SetStringLanguageAccording("Двигатель SOFIM 2.3 JTD мощностью 130 л.с. и нормами выбросов EURO4 или EURO5", "Engine - SOFIM 2.3 JTD мощностью 130hp");
                                caseSelector = 2;
                            }
                            else if (0x20 == (0x70 & b))
                            {
                                str = SetStringLanguageAccording("Двигатель PUMA 2.2 JTD мощностью 130/150 л.с. и нормами выбросов EURO4 или EURO5", "Engine - PUMA 2.2 JTD 130/150hp and EURO4 or EURO5");
                                caseSelector = 3;
                            }
                            else if (0x10 == (0x70 & b))
                            {
                                str = SetStringLanguageAccording("Двигатель PUMA 2.2 JTD мощностью 100 л.с", "Engine - PUMA 2.2 JTD 100hp");
                                caseSelector = 4;
                            }

                            break;

                        case (0x2A0):

                            if (0x30 == (0x30 & b))
                            {
                                str = SetStringLanguageAccording("Ситроен", "Citroen");
                                caseSelector = 2;
                            }
                            else if (0x20 == (0x20 & b))
                            {
                                str = SetStringLanguageAccording("Пежо", "Peugeot");
                                caseSelector = 1;
                            }
                            else if (0x10 == (0x10 & b))
                            {
                                str = SetStringLanguageAccording("Фиат", "Fiat");
                                caseSelector = 3;
                            }
                            //else
                            //    str = SetStringLanguageAccording("Марка автомобиля " + b.ToString(), "Mark " + b.ToString());

                            break;

                        case (0x2A1):

                            caseSelector = 1;

                            if (0 != (0x30 & b))
                            {
                                str = SetStringLanguageAccording("3.3 тонн", "3.3 tons");
                                caseSelector = 1;
                            }
                            else if (0 != (0x40 & b))
                            {
                                str = SetStringLanguageAccording("3.5 тонн тяжелое шасси", "3.5 tons heavy chassis");
                                caseSelector = 2;
                            }
                            else
                                str = SetStringLanguageAccording("Неизвестное шасси " + b.ToString(), "Unknown chassis" + b.ToString());

                            break;

                        case (0x2A4):

                            if (0xFF == b)
                            {
                                str = SetStringLanguageAccording("Скорость не ограничена", "No limits");
                                caseSelector = 1;
                            }
                            else
                            {
                                str = SetStringLanguageAccording("Ограничение " + b.ToString(), "Limit is " + b.ToString());
                                caseSelector = 2;
                            }

                            break;

                        case (0x2A6):

                            caseSelector = 1;

                            if (0x02 == (0x07 & b))
                            {
                                str = "4,933 (15x74)";
                                caseSelector = 1;
                            }
                            else if (0x05 == (0x07 & b))
                            {
                                str = "5,231 (13x68)";
                                caseSelector = 2;
                            }
                            else
                                str = b.ToString();

                            break;

                        case (0x33E):

                            if (0 == b) 
                                b = epromFile[offset + 1];

                            if (0x02 == (0x03 & b))
                            {
                                str = SetStringLanguageAccording("Атермическое ветровое стекло", "Athermal");
                                caseSelector = 1;
                            }
                            else //if (0x01 == (0x03 & b))
                            {
                                str = SetStringLanguageAccording("Стандартное ветровое стекло", "Regular");
                                caseSelector = 2;
                            }

                            break;

                        case (0x348):

                            caseSelector = 1;

                            if (0x20 == (0x20 & b))
                            {
                                str = "21W/5W";
                                caseSelector = 1;
                            }
                            else if (0 == (0x20 & b))
                            {
                                str = "16W";
                                caseSelector = 2;
                            }
                            else
                                str = (0x20 & b).ToString();

                            break;
                    }

                    break;

                case (paramEncoding.maskBitsYesNo):

                    if (0 != (b & mask))
                    {
                        str = SetStringLanguageAccording("Да", "Yes");
                        caseSelector = 1;
                    }
                    else
                    {
                        str = SetStringLanguageAccording("Нет", "No");
                        caseSelector = 2;
                    }

                    break;

                case (paramEncoding.maskBitsNoYes):

                    if (0 == (b & mask))
                    {
                        str = SetStringLanguageAccording("Да", "Yes");
                        caseSelector = 1;
                    }
                    else
                    {
                        str = SetStringLanguageAccording("Нет", "No");
                        caseSelector = 2;
                    }

                    break;

                case paramEncoding.Date:
                    str = buffer[offset].ToString("X2") + buffer[offset + 1].ToString("X2") + "/" + buffer[offset + 2].ToString("X2") + "/" + buffer[offset + 3].ToString("X2");
                    break;

                default:
                    str = "......"; //in development";
                    break;
            }

            return str;
        }
    }
}