﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BCMmaster.MarkupParsing;

namespace BCMmaster
{
    public partial class EditParameters : Form
    {
        MainForm gMainForm;

        /*
        DialogResult EditParameters()
        {
            InitializeComponent();
            fillFormAtLoad();
        }
        */

        public static int gCurrComboIndex = 0;
        public static int gCurrParamsIndex = 0;

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        public void fillNewParamsCombo(int paramsIndex)
        {
            int paramLength = gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[paramsIndex].paramLength;
            string currHexValue = BitConverter.ToString(gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[paramsIndex].currDataBytes);
            textBoxNewHexValue.Text = currHexValue.Substring(0, Math.Max(3 * paramLength - 1, 2));

            gCurrComboIndex = gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[paramsIndex].currCaseSelector - 1;
            gCurrParamsIndex = paramsIndex;

            switch (gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[paramsIndex].encodingType)
            {

                case (paramEncoding.Bits):
                    
                    // только iso строка
                    comboBoxNewValue.Enabled = false;
                    textBoxNewHexValue.Enabled = true;
                    
                    break;

                case (paramEncoding.Date):
                    
                    // только дата
                    comboBoxNewValue.Enabled = false;
                    textBoxNewHexValue.Enabled = true;
                    
                    break;

                case (paramEncoding.TwoBit):

                    switch (gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[paramsIndex].paramOffset)
                    {
                        case (0x29C):
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Механическая коробка передач PSA MLGU 6M", "Manual transmission PSA MLGU 6M"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Механическая коробка передач с электронным управлением MTA M40 / M38 – 6M", "Manual transmission MTA M40 / M38 – 6M"));
                            textBoxNewHexValue.Enabled = false;

                            break;

                        case (0x2A1):
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Длинная колесная база: от 3.8м до 4.05м", "Long base 3.8 to 4.05m"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Сверхдлинная колесная база: 4.05м", "Extended long base 4.05m"));
                            textBoxNewHexValue.Enabled = false;

                            break;

                        default:
                            break;
                    }

                    break;

                case (paramEncoding.Byte):

                    switch (gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[paramsIndex].paramOffset)
                    {
                        case (0x238):
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Два противотуманных фонаря (слева и справа)", "Two pts left and right"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Один противотуманный фонарь", "One pt"));
                            textBoxNewHexValue.Enabled = false;

                            break;

                        case (0x26F):
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Тип 2 - 90л", "Type 2 - 90l"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Тип 3 - 120л", "Type 3 - 120l"));
                            textBoxNewHexValue.Enabled = false;

                            break;
                            
                        case (0x29C):
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Антиблокировочная система (ABS)", "ABS"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("ESP c функцией помощи при начале движ. на склоне и наличием кнопки противобуксовочной системы", "ESP with hill assistant and traction button control"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("ESP c функцией помощи при начале движ. на склоне и наличием кнопки повышения тягового усилия", "ESP with hill assistant and power button control"));
                            textBoxNewHexValue.Enabled = false;

                            break;

                        case (0x29D):
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Справа", "Right side"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Слева", "Left side"));
                            textBoxNewHexValue.Enabled = false;

                            break;

                        case (0x29E):
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Двигатель SOFIM 3.0 JTD мощностью 180 л.с.", "Engine - SOFIM 3.0 JTD 180hp"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Двигатель SOFIM 2.3 JTD мощностью 130 л.с. и нормами выбросов EURO4 или EURO5", "Engine - SOFIM 2.3 JTD мощностью 130hp"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Двигатель PUMA 2.2 JTD мощностью 130/150 л.с. и нормами выбросов EURO4 или EURO5", "Engine - PUMA 2.2 JTD 130/150hp and EURO4 or EURO5"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Двигатель PUMA 2.2 JTD мощностью 100 л.с", "Engine - PUMA 2.2 JTD 100hp"));
                            textBoxNewHexValue.Enabled = false;

                            break;

                        case (0x2A0):
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Пежо", "Peugeot"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Ситроен", "Citroen"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Фиат", "Fiat"));
                            textBoxNewHexValue.Enabled = false;

                            break;

                        case (0x2A1):
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("3.3 тонн", "3.3 tons"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("3.5 тонн тяжелое шасси", "3.5 tons heavy chassis"));
                            textBoxNewHexValue.Enabled = false;

                            break;

                        case (0x2A4):
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Скорость не ограничена", "No limits"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Ограничение: ", "Limit is: "));

                            textBoxNewHexValue.Enabled = true;
                            textBoxNewHexValue.Text = gMainForm.SetStringLanguageAccording("Введите здесь", "enter here");

                            break;

                        case (0x266):
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Коррекция: ", "Correction is: "));

                            textBoxNewHexValue.Enabled = true;
                            textBoxNewHexValue.Text = gMainForm.SetStringLanguageAccording("Введите здесь", "enter here");

                            break;

                        case (0x2A6):
                            comboBoxNewValue.Items.Add("4,933 (15x74)");
                            comboBoxNewValue.Items.Add("5, 231(13x68)");
                            textBoxNewHexValue.Enabled = false;

                            break;

                        case (0x33E):
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Атермическое ветровое стекло", "Athermal"));
                            comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Стандартное ветровое стекло", "Regular"));
                            textBoxNewHexValue.Enabled = false;
                            ///!!!!b = epromFile[offset + 1];

                            break;

                        case (0x348):
                            comboBoxNewValue.Items.Add("21W/5W");
                            comboBoxNewValue.Items.Add("16W");
                            textBoxNewHexValue.Enabled = false;

                            break;

                        default:
                            comboBoxNewValue.Enabled = false;
                            textBoxNewHexValue.Enabled = false;
                            break;

                    }

                    break;

                case (paramEncoding.maskBitsYesNo):
                    comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Да", "Yes"));
                    comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Нет", "No"));

                    textBoxNewHexValue.Enabled = false;

                    break;
                case (paramEncoding.maskBitsNoYes):
                    comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Да", "Yes"));
                    comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Нет", "No"));

                    textBoxNewHexValue.Enabled = false;

                    break;

                case (paramEncoding.maskBits):
                    comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Имеется", "Present"));
                    comboBoxNewValue.Items.Add(gMainForm.SetStringLanguageAccording("Отсутствует", "Absent"));

                    textBoxNewHexValue.Enabled = false;

                    break;

                case (paramEncoding.Word):
                    // только типоразмер шин
                    comboBoxNewValue.Enabled = false;

                    textBoxNewHexValue.Enabled = true;

                    break;

                case (paramEncoding.WordInverted):
                    // только пробег в км до ТО

                    comboBoxNewValue.Enabled = false;
                    textBoxNewHexValue.Enabled = true;

                    break;

                case (paramEncoding.String):
                    comboBoxNewValue.Enabled = false;

                    textBoxNewHexValue.Enabled = true;
                    textBoxNewHexValue.Text = gMainForm.SetStringLanguageAccording("Введите строку здесь:", "Enter string here:");

                    break;

                case (paramEncoding.Unknown):
                    comboBoxNewValue.Enabled = false;
                    textBoxNewHexValue.Enabled = false;
                    textBoxNewHexValue.Text = (gMainForm.SetStringLanguageAccording("Не подлежит модификации", "Not editable"));

                    break;

                default:
                    break;
            }

            if (comboBoxNewValue.Enabled)
            {
                comboBoxNewValue.SelectedIndex = gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[paramsIndex].currCaseSelector-1;
            }
        }

        List<byte[]> gNewDataBytes = new List<byte[]> { new byte[PropertiesMarkup.MAX_DATA_SIZE_IN_BYTES] };
        List<DataParamDescriptor> descrToChange = new List<DataParamDescriptor>();

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void CalcParamNewHexValue()
        {
            //
            // DataBlockParamsList[gCurrParamsIndex] 
            //
            //public int paramOffset;
            //public int paramLength;
            //public int bitMask;
            //public string paramDescriptionRu;
            //public string paramDescriptionEn;
            //public paramEncoding encodingType;
            //public listType listTab;
            //public int positionInList;
            //public string additionalInfo;
            //public byte[] currDataBytes;
            //public int currCaseSelector;
            int nDescr = 1;

            if (gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[gCurrParamsIndex].linked != null)
            {
                nDescr += gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[gCurrParamsIndex].linked.Count();
            }

            gNewDataBytes = new List<byte[]> { new byte[PropertiesMarkup.MAX_DATA_SIZE_IN_BYTES] };
            descrToChange = new List<DataParamDescriptor> { gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[gCurrParamsIndex] };


            for (int i = 1; i < nDescr; i++)
            {
                descrToChange.Add(gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[gCurrParamsIndex].linked[i - 1]);
                gNewDataBytes.Add(new byte[PropertiesMarkup.MAX_DATA_SIZE_IN_BYTES]);
            }

            for (int i = 0; i < nDescr; i++)
            {
                var descr = descrToChange[i];
                byte b = gMainForm.epromFile[descr.paramOffset];

                switch (descr.encodingType)
                {
                    case (paramEncoding.Bits):
                    case (paramEncoding.Date):
                    default:
                    //
                    // нет альтернативных вариантов при смене селектора комбобокс или нет 
                    // селектора комбобокс
                    //
                        break;

                    case (paramEncoding.TwoBit):

                        switch (descr.paramOffset)
                        {
                            case (0x29C):
                                // если было ("Механическая коробка передач PSA MLGU 6M", "Manual transmission PSA MLGU 6M"));                                          
                                // то сделать ("Механическая коробка передач с электронным управлением MTA M40 / M38 – 6M", "Manual transmission MTA M40 / M38 – 6M")); 
                                // и наоборот

                                if (1 == comboBoxNewValue.SelectedIndex) 
                                {
                                    // было xxxx 0x1x
                                    // надо xxxx 1x0x
                                    b |= 0x04;
                                    b &= 0xFD;

                                }
                                else
                                {
                                    b |= 0x02;
                                    b &= 0xFB;
                                }

                                break;

                            case (0x2A1):
                                //"Длинная колесная база: от 3.8м до 4.05м", "Long base 3.8 to 4.05m"
                                //"Сверхдлинная колесная база: 4.05м", "Extended long base 4.05m"

                                if (1 == comboBoxNewValue.SelectedIndex)
                                {
                                    // было xxxx 0x1x
                                    // надо xxxx 1x0x
                                    b |= 0x04;
                                    b &= 0xFD;

                                }
                                else
                                {
                                    b |= 0x02;
                                    b &= 0xFB;
                                }

                                break;

                            default:
                                break;
                        }

                        gNewDataBytes[i][0] = b;

                        break;

                    case (paramEncoding.Byte):

                        switch (descr.paramOffset)
                        { 
                            //" Два противотуманных фонаря (слева и справа)", "Two pts left and right"
                            // "Один противотуманный фонарь", "One pt"
                            case (0x238):

                                if (0 == comboBoxNewValue.SelectedIndex)
                                {
                                    b |= 0x40;
                                }
                                else
                                {
                                    b &= 0xBF;
                                }

                                gNewDataBytes[i][0] = b;

                                break;

                            case (0x26F):
                                //"Тип 2 - 90л", "Type 2 - 90l"
                                //"Тип 3 - 120л", "Type 3 - 120l"

                                if (1 == comboBoxNewValue.SelectedIndex)
                                {
                                    // было xxxx xx10
                                    // надо xxxx xx11
                                    b |= 0x03;
                                }
                                else
                                {
                                    b |= 0x02;
                                    b &= 0xFE;
                                }

                                gNewDataBytes[i][0] = b;

                                break;

                            case (0x29C):
                                //0x10 "Антиблокировочная система (ABS)", "ABS"
                                //0x20 "Система динамической стабилизации (ESP) c функцией помощи при начале движения на склоне и наличием кнопки противобуксовочной системы", "ESP with hill assistant and traction button control"
                                //0x40 "Система динамической стабилизации (ESP) c функцией помощи при начале движения на склоне и наличием кнопки повышения тягового усилия", "ESP with hill assistant and power button control"

                                if (0 == comboBoxNewValue.SelectedIndex)
                                {
                                    // надо x001 xxxx
                                    b |= 0x10;
                                }
                                else if (1 == comboBoxNewValue.SelectedIndex)
                                {
                                    // надо x010 xxxx
                                    b |= 0x20;
                                    b &= 0xAF;
                                }
                                else
                                {
                                    // надо x100 xxxx
                                    b |= 0x40;
                                    b &= 0xCF;
                                }

                                gNewDataBytes[i][0] = b;

                                break;

                            case (0x29D):
                                // "Справа", "Right side"
                                // "Слева", "Left side"
                                // маска 0x10

                                if (0 == comboBoxNewValue.SelectedIndex)
                                {
                                    b |= 0x10;
                                }
                                else
                                {
                                    b &= 0xEF;
                                }

                                gNewDataBytes[i][0] = b;

                                break;

                            case (0x29E):
                                //"Двигатель SOFIM 3.0 JTD мощностью 180 л.с.", "Engine - SOFIM 3.0 JTD 180hp"
                                //"Двигатель PUMA 2.2 JTD мощностью 130/150 л.с. и нормами выбросов EURO4 или EURO5", "Engine - PUMA 2.2 JTD 130/150hp and EURO4 or EURO5"
                                //"Двигатель PUMA 2.2 JTD мощностью 100 л.с", "Engine - PUMA 2.2 JTD 100hp"

                                if (3 == comboBoxNewValue.SelectedIndex)
                                {
                                    b |= 0x10;
                                    b &= 0x1F;
                                }
                                else if (2 == comboBoxNewValue.SelectedIndex)
                                {
                                    b |= 0x20;
                                    b &= 0x2F;
                                }
                                else if (1 == comboBoxNewValue.SelectedIndex)
                                {
                                    b |= 0x40;
                                    b &= 0x4F;
                                }
                                else
                                {
                                    b |= 0x70;
                                    b &= 0x7F;
                                }

                                gNewDataBytes[i][0] = b;

                                break;

                            case (0x2A0):
                                //0x2X "Пежо", "Peugeot"
                                //0x3X "Ситроен", "Citroen"
                                //0x1X "Фиат", "Fiat"

                                if (2 == comboBoxNewValue.SelectedIndex)
                                {
                                    // надо xx01 xxxx
                                    b |= 0x10;
                                    b &= 0xDF;

                                }
                                else if (0 == comboBoxNewValue.SelectedIndex)
                                {
                                    // надо x001 xxxx
                                    b |= 0x20;
                                    b &= 0x7F;
                                }
                                else
                                {
                                    // надо x111 xxxx
                                    b |= 0x70;
                                }

                                gNewDataBytes[i][0] = b;

                                break;

                            case (0x2A1):
                                //"3.3 тонн", "3.3 tons"
                                //"3.5 тонн тяжелое шасси", "3.5 tons heavy chassis"

                                if (1 == comboBoxNewValue.SelectedIndex)
                                {
                                    b |= 0x40;
                                    b &= 0xCF;
                                }
                                else 
                                {
                                    b |= 0x30;
                                    b &= 0xBF;
                                }

                                gNewDataBytes[i][0] = b;

                                break;

                            case (0x2A4):
                                //ff Скорость не ограничена", "No limits"
                                //"Ограничение: ", "Limit is: "

                                if (0 == comboBoxNewValue.SelectedIndex)
                                {
                                    b |= 0xFF;
                                    gNewDataBytes[i][0] = b;
                                }
                                else 
                                {
                                    textBoxNewHexValue.Enabled = true;
                                    textBoxNewHexValue.Text = b.ToString();
                                    gNewDataBytes[i][0] = Convert.ToByte(textBoxNewHexValue.Text);
                                }
                          
                                break;

                            case (0x266):
                            
                                textBoxNewHexValue.Enabled = true;
                                textBoxNewHexValue.Text = b.ToString();
                                gNewDataBytes[i][0] = Convert.ToByte(textBoxNewHexValue.Text);
                            
                                break;

                            case (0x2A6):
                                // "4,933 (15x74)"
                                // "5, 231(13x68)"

                                if (1 == comboBoxNewValue.SelectedIndex)
                                {
                                    b |= 0x05;
                                    b &= 0xFD;
                                }
                                else
                                {
                                    b |= 0x02;
                                    b &= 0xFA;
                                }

                                gNewDataBytes[i][0] = b;

                                break;

                            case (0x33E):
                                //"Атермическое ветровое стекло", "Athermal"
                                //"Стандартное ветровое стекло", "Regular"

                                if (1 == comboBoxNewValue.SelectedIndex)
                                {
                                    b |= 0x01;
                                    b &= 0xFD;
                                }
                                else 
                                {
                                    b |= 0x02;
                                    b &= 0xFE;
                                }

                                gNewDataBytes[i][0] = b;

                                break;

                            case (0x348):
                                //21W/5W"
                                //"16W"

                                if (0 == comboBoxNewValue.SelectedIndex)
                                {
                                    b |= 0x20;
                                    b &= 0xEF;
                                }
                                else 
                                {
                                    b &= 0xCF;
                                }

                                gNewDataBytes[i][0] = b;

                                break;

                            default:
                                comboBoxNewValue.Enabled = false;
                                textBoxNewHexValue.Enabled = false;
                                break;
                        }

                        break;

                    case (paramEncoding.Unknown):

                        comboBoxNewValue.Enabled = false;
                        comboBoxNewValue.Text = "";

                        textBoxNewHexValue.Enabled = false;
                        textBoxNewHexValue.Text = (gMainForm.SetStringLanguageAccording("Не подлежит модификации", "Not editable"));

                        break;

                    case (paramEncoding.maskBits):

                        // "Имеется", "Present"
                        // "Отсутствует", "Absent"

                        if (0 == comboBoxNewValue.SelectedIndex)
                        {
                            b |= (byte)descr.bitMask;
                        }
                        else //if (0 == comboBoxNewValue.SelectedIndex)
                        {
                            b &= (byte)(0xFF - descr.bitMask);
                        }

                        gNewDataBytes[i][0] = b;

                        textBoxNewHexValue.Enabled = false;

                        break;                    
                    
                    case (paramEncoding.maskBitsYesNo):
                        //"Да", "Yes";
                        //"Нет", "No";

                        if (0 == comboBoxNewValue.SelectedIndex)
                        {
                            b |= (byte)descr.bitMask;
                        }
                        else //if (0 == comboBoxNewValue.SelectedIndex)
                        {
                            b &= (byte)(0xFF - descr.bitMask);
                        }

                        gNewDataBytes[i][0] = b;

                        textBoxNewHexValue.Enabled = false;

                        break;
                    case (paramEncoding.maskBitsNoYes):
                        //"Да", "Yes";
                        //"Нет", "No";

                        if (0 != comboBoxNewValue.SelectedIndex)
                        {
                            b |= (byte)descr.bitMask;
                        }
                        else //if (0 == comboBoxNewValue.SelectedIndex)
                        {
                            b &= (byte)(0xFF - descr.bitMask);
                        }

                        gNewDataBytes[i][0] = b;

                        textBoxNewHexValue.Enabled = false;

                        break;
                }
            }
            
            
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void setNewHexValueAtTextBox()
        {
            int paramLength = gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[gCurrParamsIndex].paramLength;
            string newHexValue = BitConverter.ToString(gNewDataBytes[0]);
            textBoxNewHexValue.Text = newHexValue.Substring(0, Math.Max(3 * paramLength - 1, 2));
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void comboBoxNewValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcParamNewHexValue();
            setNewHexValueAtTextBox();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        public void fillFormAtLoad()
        {
            ListView currentList = new ListView();

            currentList = new ListView();

            gMainForm.GetCurrentList(ref currentList);

            this.Text = gMainForm.SetStringLanguageAccording("Изменить параметр", "Parameter change");

            groupBoxParam.Text = gMainForm.SetStringLanguageAccording("Параметр", "Parameter");
            groupBoxValue.Text = gMainForm.SetStringLanguageAccording("Значение", "Value");
            groupBoxOffset.Text = gMainForm.SetStringLanguageAccording("Смещение", "Offset");
            groupBoxSize.Text = gMainForm.SetStringLanguageAccording("Размер, б", "Size, b");
            groupBoxMask.Text = gMainForm.SetStringLanguageAccording("Маска", "Mask");
            groupBoxValueHex.Text = gMainForm.SetStringLanguageAccording("Значение, hex", "Value, hex");
            groupBoxNewValue.Text = gMainForm.SetStringLanguageAccording("Новое значение", "New value");

            int selectedListIndex = currentList.SelectedIndices[0];
            int selectedDescriptorIndex = gMainForm.gCurrentMCData[selectedListIndex].index;

            labelParameterDescription.Text = currentList.Items[selectedListIndex].Text; 
            labelParamValue.Text =  currentList.Items[selectedListIndex].SubItems[1].Text;

            int paramOffset = gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[selectedDescriptorIndex].paramOffset;

            labelParameterOffset.Text = "0x" + paramOffset.ToString("X4");

            int paramLength = gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[selectedDescriptorIndex].paramLength;

            labelParameterSize.Text = paramLength.ToString();

            labelParameterMask.Text = "0x" + gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[selectedDescriptorIndex].bitMask.ToString("X2");

            string strHexValue = "";

            for (int i = 0; i<paramLength; i++)
            {
                strHexValue += gMainForm.epromFile[paramOffset + i].ToString("X2") + " ";
            }

            labelParameterHexValue.Text = strHexValue;

            comboBoxNewValue.Text = gMainForm.SetStringLanguageAccording("Выберите новое значение", "New value");
            textBoxNewHexValue.Text = gMainForm.SetStringLanguageAccording("Или введите в hex виде", "Or enter new hex value string");

            fillNewParamsCombo(selectedDescriptorIndex);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        public EditParameters(MainForm parent)
        {
            InitializeComponent();
            gMainForm = parent;
            descrToChange = new List<DataParamDescriptor> { gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[gCurrParamsIndex] };
            fillFormAtLoad();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void buttonOK_Click(object sender, EventArgs e)
        {
            string[] split; 
            string newBlockStr = "", msg = "", hdr = "";

            try
            {
                //if (textBoxNewHexValue.Enabled)
                //
                // вводили новое значение в textBoxNewHexValue руками, преобразовать введенную строку к массиву байт согласно длины параметра
                //
                {
                    split = (textBoxNewHexValue.Text + " ").Split(new Char[] { ' ', ',', '.', '_', '-' });

                    int i = 0;
                    foreach (string s in split)
                    {
                        gNewDataBytes[0][i] = Convert.ToByte(s, 16);
                        newBlockStr += "0x" + gNewDataBytes[0][i++].ToString("X2") + " ";
                        if (i >= gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[gCurrParamsIndex].paramLength)
                            break;
                    }
                }
            }
            catch
            {
                if (textBoxNewHexValue.Enabled)
                {
                    msg = gMainForm.SetStringLanguageAccording(
                        "Проверьте правильность ввода (например, 0A F4 79)",
                        "Check the string (for example, 0A F4 79)");
                    hdr = gMainForm.SetStringLanguageAccording("Ошибка формата", "Input format error");

                    MessageBox.Show(
                        msg,
                        hdr,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }

                return;
            }

            msg = gMainForm.SetStringLanguageAccording(
                "Скопировать байт(ы)\r" + newBlockStr + "\rпо смещению 0x" + gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[gCurrParamsIndex].paramOffset.ToString("X4"),
                "Copy byte(s)\r" + newBlockStr + "\rto 0x" + gMainForm.currentMarkup.propertiesMarkup.DataBlockParamsList[gCurrParamsIndex].paramOffset.ToString("X4") + " offset?");
            hdr = gMainForm.SetStringLanguageAccording("Внимание", "Warning");


            if (DialogResult.Yes == MessageBox.Show(
                            msg,
                            hdr,
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question))
                //
                // скопировать введенные байты в eprom
                CopyToArray();


            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void CopyToArray()
        {
            for (int i = 0; i < descrToChange.Count; i++)
            {
                Array.Copy(
                    gNewDataBytes[i],
                    0,
                    gMainForm.epromFile,
                    descrToChange[i].paramOffset,
                    descrToChange[i].paramLength);
            }
        }

    }
}
