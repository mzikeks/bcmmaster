﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BCMmaster.MarkupParsing;

namespace BCMmaster
{
    public partial class MainForm : Form
    {
        public const int MAX_ROWS_AT_ID_TAB = 10;
        public const int MAX_DATA_SIZE_IN_BYTES = 18;


        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void InsertImgInPictureBox()
        {
            switch (currentCarModel)
            {
                case (CarModels.Peugeot_Boxer):
                    pictureBoxImg.Image = Properties.Resources.puegeot1;
                    break;

                case (CarModels.Iveco_Daily):
                    pictureBoxImg.Image = Properties.Resources.iveco1;
                    break;

                case (CarModels.Fiat_Ducato):
                    pictureBoxImg.Image = Properties.Resources.fiat1;
                    break;

                case (CarModels.Citroen_Jumper):
                    pictureBoxImg.Image = Properties.Resources.citro1;
                    break;

                default:
                    pictureBoxImg.Image = Properties.Resources.unknown;
                    break;
            }
        }


        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void FillIdentificationListViewFromParsedList()
        {
            stringsMultyColumnsData[] strList = new stringsMultyColumnsData[MAX_ROWS_AT_ID_TAB];

            ListViewItem lvi;

            listViewID.Items.Clear();

            int i = 0, j = 0, index = 0;

            for (i = 0; i < PropertiesMarkup.MAX_PARAMS_IN_DATA_BLOCK; i++)
            //
            // в цикле по всем параметрам
            //
            {
                if (null == currentMarkup.propertiesMarkup.DataBlockParamsList[i].additionalInfo)
                    //
                    // пустой элемент - конец массива параметров, выходим 
                    //
                    break;

                if (currentMarkup.propertiesMarkup.DataBlockParamsList[i].listTab == listType.IDList)
                //
                // если параметр для этого (IDList) списка 
                //
                {
                    index = currentMarkup.propertiesMarkup.DataBlockParamsList[i].positionInList;
                    if (index >= MAX_ROWS_AT_ID_TAB)
                        continue;

                    strList[index].FirstCol = SetStringLanguageAccording(currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramDescriptionRu,
                           currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramDescriptionEn);

                    strList[index].SecondCol = SetStringEncodingTypeAccording(
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].encodingType,
                        epromFile,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramOffset,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramLength,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].bitMask,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].currDataBytes,
                        ref currentMarkup.propertiesMarkup.DataBlockParamsList[i].currCaseSelector);

                    strList[index].FourthCol = SetValueTypeAccording(
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramOffset,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramLength,
                        epromFile,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].currDataBytes);

                    strList[index].index = i;

                    j++;
                }
            }

            for (i = 0; i < j; i++)
            {
                lvi = new ListViewItem(strList[i].FirstCol);
                lvi.SubItems.Add(strList[i].SecondCol);
                lvi.SubItems.Add(strList[i].ThirdCol);
                listViewID.Items.Add(lvi);
            }

        }

        
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void FillIdentificationMainListView()
        {
            string str;

            str = SetStringLanguageAccording("Автомобиль: ", "Car model: ");

            listViewIDMainInfo.Items.Clear();

            ListViewItem lvi = new ListViewItem(str + gModelStr);
            listViewIDMainInfo.Items.Add(lvi);

            lvi = new ListViewItem("VIN: " + gVINStr);
            listViewIDMainInfo.Items.Add(lvi);
            listViewIDMainInfo.Update();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void InsertLogoInPictureBox()
        {
 
            switch (currentCarModel)
            {
                case (CarModels.Peugeot_Boxer):
                    pictureBoxLogo.Image = Properties.Resources.puegeot_logo;
                    break;

                case (CarModels.Iveco_Daily):
                    pictureBoxLogo.Image = Properties.Resources.iveco_logo;
                    break;

                case (CarModels.Fiat_Ducato):
                    pictureBoxLogo.Image = Properties.Resources.fiat_logo;
                    break;

                case (CarModels.Citroen_Jumper):
                    pictureBoxLogo.Image = Properties.Resources.citroen_logo;
                    break;

                default:
                    pictureBoxLogo.Image = Properties.Resources.unknown_logo;
                    break;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void IdentifyCarModel(string sVIN, out EpromMarkup currentMarkup)
        {
            string sVinStart = sVIN.Substring(0,3);

            switch (sVinStart)
            {
                case "ZCF":
                    currentMarkup = EpromMarkup.GetMarkup("BCS20I");

                    var configurationIndex = 8;
                    var configurationValue = SetStringEncodingTypeAccording(
                        currentMarkup.propertiesMarkup.DataBlockParamsList[configurationIndex].encodingType,
                        epromFile,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[configurationIndex].paramOffset,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[configurationIndex].paramLength,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[configurationIndex].bitMask,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[configurationIndex].currDataBytes,
                        ref currentMarkup.propertiesMarkup.DataBlockParamsList[configurationIndex].currCaseSelector);

                    if (configurationValue == "11111111111")
                    {
                        currentMarkup = EpromMarkup.GetMarkup("BCS20J");
                    }
                    currentCarModel = CarModels.Iveco_Daily;
                    gModelStr = "Iveco Daily";
                    break;

                case "ZFA":
                case "Z7G":
                    currentMarkup = EpromMarkup.GetMarkup("BC250I");
                    currentCarModel = CarModels.Fiat_Ducato;
                    gModelStr = "Fiat Ducato";
                    break;

                case "VF3":
                case "VF7":
                    currentMarkup = EpromMarkup.GetMarkup("BC250I");
                    if (0x21 == epromFile[EpromMarkup.OFFSET_CAR_MODEL_BYTE])
                    {
                        currentCarModel = CarModels.Peugeot_Boxer;
                        gModelStr = "Peugeot Boxer";
                    }
                    else if (0x31 == epromFile[EpromMarkup.OFFSET_CAR_MODEL_BYTE])
                    {
                        currentCarModel = CarModels.Citroen_Jumper;
                        gModelStr = "Citroen Jumper";
                    }
                    break;

                default:
                    currentMarkup = EpromMarkup.GetMarkup("BCS20I");
                    currentCarModel = CarModels.Unknown;
                    gModelStr = "Can't identify";
                    break;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void UpdateInformationTab()
        {

        }


    }// class MainForm

}
