﻿namespace BCMmaster
{
    partial class EditParameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditParameters));
            this.groupBoxOffset = new System.Windows.Forms.GroupBox();
            this.labelParameterOffset = new System.Windows.Forms.Label();
            this.groupBoxSize = new System.Windows.Forms.GroupBox();
            this.labelParameterSize = new System.Windows.Forms.Label();
            this.groupBoxValueHex = new System.Windows.Forms.GroupBox();
            this.labelParameterHexValue = new System.Windows.Forms.Label();
            this.groupBoxParam = new System.Windows.Forms.GroupBox();
            this.labelParameterDescription = new System.Windows.Forms.Label();
            this.groupBoxValue = new System.Windows.Forms.GroupBox();
            this.labelParamValue = new System.Windows.Forms.Label();
            this.comboBoxNewValue = new System.Windows.Forms.ComboBox();
            this.textBoxNewHexValue = new System.Windows.Forms.TextBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.groupBoxMask = new System.Windows.Forms.GroupBox();
            this.labelParameterMask = new System.Windows.Forms.Label();
            this.groupBoxNewValue = new System.Windows.Forms.GroupBox();
            this.groupBoxOffset.SuspendLayout();
            this.groupBoxSize.SuspendLayout();
            this.groupBoxValueHex.SuspendLayout();
            this.groupBoxParam.SuspendLayout();
            this.groupBoxValue.SuspendLayout();
            this.groupBoxMask.SuspendLayout();
            this.groupBoxNewValue.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxOffset
            // 
            this.groupBoxOffset.Controls.Add(this.labelParameterOffset);
            this.groupBoxOffset.Location = new System.Drawing.Point(8, 96);
            this.groupBoxOffset.Name = "groupBoxOffset";
            this.groupBoxOffset.Size = new System.Drawing.Size(94, 43);
            this.groupBoxOffset.TabIndex = 2;
            this.groupBoxOffset.TabStop = false;
            this.groupBoxOffset.Text = "Смещение";
            // 
            // labelParameterOffset
            // 
            this.labelParameterOffset.AutoSize = true;
            this.labelParameterOffset.Location = new System.Drawing.Point(22, 18);
            this.labelParameterOffset.Name = "labelParameterOffset";
            this.labelParameterOffset.Size = new System.Drawing.Size(49, 16);
            this.labelParameterOffset.TabIndex = 0;
            this.labelParameterOffset.Text = "0x1234";
            // 
            // groupBoxSize
            // 
            this.groupBoxSize.Controls.Add(this.labelParameterSize);
            this.groupBoxSize.Location = new System.Drawing.Point(108, 96);
            this.groupBoxSize.Name = "groupBoxSize";
            this.groupBoxSize.Size = new System.Drawing.Size(93, 43);
            this.groupBoxSize.TabIndex = 3;
            this.groupBoxSize.TabStop = false;
            this.groupBoxSize.Text = "Размер, б";
            // 
            // labelParameterSize
            // 
            this.labelParameterSize.AutoSize = true;
            this.labelParameterSize.Location = new System.Drawing.Point(35, 18);
            this.labelParameterSize.Name = "labelParameterSize";
            this.labelParameterSize.Size = new System.Drawing.Size(15, 16);
            this.labelParameterSize.TabIndex = 0;
            this.labelParameterSize.Text = "3";
            // 
            // groupBoxValueHex
            // 
            this.groupBoxValueHex.Controls.Add(this.labelParameterHexValue);
            this.groupBoxValueHex.Location = new System.Drawing.Point(320, 96);
            this.groupBoxValueHex.Name = "groupBoxValueHex";
            this.groupBoxValueHex.Size = new System.Drawing.Size(270, 43);
            this.groupBoxValueHex.TabIndex = 4;
            this.groupBoxValueHex.TabStop = false;
            this.groupBoxValueHex.Text = "Значение, Hex ";
            // 
            // labelParameterHexValue
            // 
            this.labelParameterHexValue.AutoSize = true;
            this.labelParameterHexValue.Location = new System.Drawing.Point(11, 18);
            this.labelParameterHexValue.Name = "labelParameterHexValue";
            this.labelParameterHexValue.Size = new System.Drawing.Size(58, 16);
            this.labelParameterHexValue.TabIndex = 0;
            this.labelParameterHexValue.Text = "01 02 FF";
            // 
            // groupBoxParam
            // 
            this.groupBoxParam.Controls.Add(this.labelParameterDescription);
            this.groupBoxParam.Location = new System.Drawing.Point(8, 10);
            this.groupBoxParam.Name = "groupBoxParam";
            this.groupBoxParam.Size = new System.Drawing.Size(582, 43);
            this.groupBoxParam.TabIndex = 5;
            this.groupBoxParam.TabStop = false;
            this.groupBoxParam.Text = "Параметр";
            // 
            // labelParameterDescription
            // 
            this.labelParameterDescription.AutoSize = true;
            this.labelParameterDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelParameterDescription.Location = new System.Drawing.Point(6, 18);
            this.labelParameterDescription.Name = "labelParameterDescription";
            this.labelParameterDescription.Size = new System.Drawing.Size(166, 16);
            this.labelParameterDescription.TabIndex = 0;
            this.labelParameterDescription.Text = "Описание параметра";
            // 
            // groupBoxValue
            // 
            this.groupBoxValue.Controls.Add(this.labelParamValue);
            this.groupBoxValue.Location = new System.Drawing.Point(8, 53);
            this.groupBoxValue.Name = "groupBoxValue";
            this.groupBoxValue.Size = new System.Drawing.Size(582, 43);
            this.groupBoxValue.TabIndex = 6;
            this.groupBoxValue.TabStop = false;
            this.groupBoxValue.Text = "Значение";
            // 
            // labelParamValue
            // 
            this.labelParamValue.AutoSize = true;
            this.labelParamValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelParamValue.Location = new System.Drawing.Point(6, 18);
            this.labelParamValue.Name = "labelParamValue";
            this.labelParamValue.Size = new System.Drawing.Size(166, 16);
            this.labelParamValue.TabIndex = 0;
            this.labelParamValue.Text = "Значение параметра";
            // 
            // comboBoxNewValue
            // 
            this.comboBoxNewValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxNewValue.Location = new System.Drawing.Point(9, 17);
            this.comboBoxNewValue.Name = "comboBoxNewValue";
            this.comboBoxNewValue.Size = new System.Drawing.Size(566, 24);
            this.comboBoxNewValue.TabIndex = 7;
            this.comboBoxNewValue.SelectedIndexChanged += new System.EventHandler(this.comboBoxNewValue_SelectedIndexChanged);
            // 
            // textBoxNewHexValue
            // 
            this.textBoxNewHexValue.Location = new System.Drawing.Point(9, 50);
            this.textBoxNewHexValue.Name = "textBoxNewHexValue";
            this.textBoxNewHexValue.Size = new System.Drawing.Size(566, 22);
            this.textBoxNewHexValue.TabIndex = 8;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(515, 229);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 9;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(434, 229);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 10;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // groupBoxMask
            // 
            this.groupBoxMask.Controls.Add(this.labelParameterMask);
            this.groupBoxMask.Location = new System.Drawing.Point(208, 96);
            this.groupBoxMask.Name = "groupBoxMask";
            this.groupBoxMask.Size = new System.Drawing.Size(106, 42);
            this.groupBoxMask.TabIndex = 11;
            this.groupBoxMask.TabStop = false;
            this.groupBoxMask.Text = "Маска";
            // 
            // labelParameterMask
            // 
            this.labelParameterMask.AutoSize = true;
            this.labelParameterMask.Location = new System.Drawing.Point(28, 18);
            this.labelParameterMask.Name = "labelParameterMask";
            this.labelParameterMask.Size = new System.Drawing.Size(35, 16);
            this.labelParameterMask.TabIndex = 0;
            this.labelParameterMask.Text = "0x70";
            // 
            // groupBoxNewValue
            // 
            this.groupBoxNewValue.Controls.Add(this.textBoxNewHexValue);
            this.groupBoxNewValue.Controls.Add(this.comboBoxNewValue);
            this.groupBoxNewValue.Location = new System.Drawing.Point(8, 139);
            this.groupBoxNewValue.Name = "groupBoxNewValue";
            this.groupBoxNewValue.Size = new System.Drawing.Size(582, 84);
            this.groupBoxNewValue.TabIndex = 12;
            this.groupBoxNewValue.TabStop = false;
            this.groupBoxNewValue.Text = "Новое значение";
            // 
            // EditParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(598, 260);
            this.Controls.Add(this.groupBoxNewValue);
            this.Controls.Add(this.groupBoxMask);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.groupBoxValue);
            this.Controls.Add(this.groupBoxParam);
            this.Controls.Add(this.groupBoxValueHex);
            this.Controls.Add(this.groupBoxSize);
            this.Controls.Add(this.groupBoxOffset);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditParameters";
            this.Text = "Изменение значения параметра";
            this.groupBoxOffset.ResumeLayout(false);
            this.groupBoxOffset.PerformLayout();
            this.groupBoxSize.ResumeLayout(false);
            this.groupBoxSize.PerformLayout();
            this.groupBoxValueHex.ResumeLayout(false);
            this.groupBoxValueHex.PerformLayout();
            this.groupBoxParam.ResumeLayout(false);
            this.groupBoxParam.PerformLayout();
            this.groupBoxValue.ResumeLayout(false);
            this.groupBoxValue.PerformLayout();
            this.groupBoxMask.ResumeLayout(false);
            this.groupBoxMask.PerformLayout();
            this.groupBoxNewValue.ResumeLayout(false);
            this.groupBoxNewValue.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxOffset;
        private System.Windows.Forms.GroupBox groupBoxSize;
        private System.Windows.Forms.GroupBox groupBoxValueHex;
        private System.Windows.Forms.GroupBox groupBoxParam;
        private System.Windows.Forms.GroupBox groupBoxValue;
        private System.Windows.Forms.ComboBox comboBoxNewValue;
        private System.Windows.Forms.TextBox textBoxNewHexValue;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label labelParameterOffset;
        private System.Windows.Forms.Label labelParameterSize;
        private System.Windows.Forms.Label labelParameterHexValue;
        private System.Windows.Forms.Label labelParameterDescription;
        private System.Windows.Forms.Label labelParamValue;
        private System.Windows.Forms.GroupBox groupBoxMask;
        private System.Windows.Forms.Label labelParameterMask;
        private System.Windows.Forms.GroupBox groupBoxNewValue;
    }
}