﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BCMmaster.MarkupParsing;

namespace BCMmaster
{
    public partial class MainForm : Form
    {
        public const int MAX_ROWS_AT_SAFETY_TAB = 10;

        stringsMultyColumnsData[] strListSafety = new stringsMultyColumnsData[MAX_ROWS_AT_SAFETY_TAB];

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void FillSafetyListViewFromParsedList()
        {
            ListViewItem lvi;

            listViewSafety.Items.Clear();
            strListSafety = new stringsMultyColumnsData[MAX_ROWS_AT_CONFIG_TAB];

            int i = 0, j = 0, index = 0;

            for (i = 0; i < PropertiesMarkup.MAX_PARAMS_IN_DATA_BLOCK; i++)
            //
            // в цикле по всем параметрам
            //
            {
                if (null == currentMarkup.propertiesMarkup.DataBlockParamsList[i].additionalInfo)
                    //
                    // пустой элемент - конец массива параметров, выходим 
                    //
                    break;

                if (currentMarkup.propertiesMarkup.DataBlockParamsList[i].listTab == listType.PassiveSafetyList)
                //
                // если параметр для этого (PassiveSafetyList) списка 
                //
                {
                    index = currentMarkup.propertiesMarkup.DataBlockParamsList[i].positionInList;
                    if (index >= MAX_ROWS_AT_SAFETY_TAB)
                        continue;

                    strListSafety[index].FirstCol = SetStringLanguageAccording(currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramDescriptionRu,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramDescriptionEn);

                    strListSafety[index].SecondCol = SetStringEncodingTypeAccording(
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].encodingType,
                        epromFile,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramOffset,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramLength,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].bitMask,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].currDataBytes,
                        ref currentMarkup.propertiesMarkup.DataBlockParamsList[i].currCaseSelector);

                    strListSafety[index].ThirdCol = currentMarkup.propertiesMarkup.DataBlockParamsList[i].additionalInfo;

                    strListSafety[index].FourthCol = SetValueTypeAccording(
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramOffset,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramLength,
                        epromFile,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].currDataBytes);

                    strListSafety[index].index = i;

                    j++;
                }
            }

            for (i = 0; i < j; i++)
            {
                lvi = new ListViewItem(strListSafety[i].FirstCol);
                lvi.SubItems.Add(strListSafety[i].SecondCol);
                lvi.SubItems.Add(strListSafety[i].ThirdCol);

                listViewSafety.Items.Add(lvi);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void listViewSafety_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            EditSelectedParameter();
        }


    }
}
