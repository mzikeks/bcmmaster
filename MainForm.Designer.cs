﻿namespace BCMmaster
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readEpromToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savepromToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savepromAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.languageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadLastEpromOnStartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setEpromDirtoolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.saveOptionsNowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listViewIDMainInfo = new System.Windows.Forms.ListView();
            this.listViewID = new System.Windows.Forms.ListView();
            this.column1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.column2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBoxImg = new System.Windows.Forms.PictureBox();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.listViewConfig = new System.Windows.Forms.ListView();
            this.columnConfig1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnConfig2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnConfig3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.listViewEquipment = new System.Windows.Forms.ListView();
            this.columnEquipment1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnEquipment2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnEquipment3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.listViewSafety = new System.Windows.Forms.ListView();
            this.columnSafety1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSafety2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSafety3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxBCMClone = new System.Windows.Forms.GroupBox();
            this.buttonBCMClone = new System.Windows.Forms.Button();
            this.groupBoxECUBCM = new System.Windows.Forms.GroupBox();
            this.SyncControlSum_checkBox = new System.Windows.Forms.CheckBox();
            this.buttonECUBCM = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxVin = new System.Windows.Forms.TextBox();
            this.buttonVinChange = new System.Windows.Forms.Button();
            this.groupBoxCheckSum = new System.Windows.Forms.GroupBox();
            this.buttonCheckSum = new System.Windows.Forms.Button();
            this.checkBoxVIN = new System.Windows.Forms.CheckBox();
            this.checkBoxFile = new System.Windows.Forms.CheckBox();
            this.checkBoxBlock1 = new System.Windows.Forms.CheckBox();
            this.checkBoxBlock3 = new System.Windows.Forms.CheckBox();
            this.checkBoxBlock2 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonFind = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBoxSearch = new System.Windows.Forms.ToolStripTextBox();
            this.menuStrip1.SuspendLayout();
            this.tabControlMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBoxBCMClone.SuspendLayout();
            this.groupBoxECUBCM.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxCheckSum.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1109, 31);
            this.menuStrip1.Stretch = false;
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readEpromToolStripMenuItem,
            this.savepromToolStripMenuItem,
            this.savepromAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(49, 27);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // readEpromToolStripMenuItem
            // 
            this.readEpromToolStripMenuItem.Name = "readEpromToolStripMenuItem";
            this.readEpromToolStripMenuItem.Size = new System.Drawing.Size(205, 28);
            this.readEpromToolStripMenuItem.Text = "&Open eprom";
            this.readEpromToolStripMenuItem.Click += new System.EventHandler(this.readEpromToolStripMenuItem_Click);
            // 
            // savepromToolStripMenuItem
            // 
            this.savepromToolStripMenuItem.Enabled = false;
            this.savepromToolStripMenuItem.Name = "savepromToolStripMenuItem";
            this.savepromToolStripMenuItem.Size = new System.Drawing.Size(205, 28);
            this.savepromToolStripMenuItem.Text = "&Save eprom";
            this.savepromToolStripMenuItem.Click += new System.EventHandler(this.savepromToolStripMenuItem_Click);
            // 
            // savepromAsToolStripMenuItem
            // 
            this.savepromAsToolStripMenuItem.Enabled = false;
            this.savepromAsToolStripMenuItem.Name = "savepromAsToolStripMenuItem";
            this.savepromAsToolStripMenuItem.Size = new System.Drawing.Size(205, 28);
            this.savepromAsToolStripMenuItem.Text = "Save eprom &as";
            this.savepromAsToolStripMenuItem.Click += new System.EventHandler(this.savepromAsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(205, 28);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.languageToolStripMenuItem,
            this.loadLastEpromOnStartToolStripMenuItem,
            this.setEpromDirtoolStrip,
            this.saveOptionsNowToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(84, 27);
            this.optionsToolStripMenuItem.Text = "&Options";
            // 
            // languageToolStripMenuItem
            // 
            this.languageToolStripMenuItem.Name = "languageToolStripMenuItem";
            this.languageToolStripMenuItem.Size = new System.Drawing.Size(281, 28);
            this.languageToolStripMenuItem.Text = "&Language EN";
            this.languageToolStripMenuItem.Click += new System.EventHandler(this.languageToolStripMenuItem_Click);
            // 
            // loadLastEpromOnStartToolStripMenuItem
            // 
            this.loadLastEpromOnStartToolStripMenuItem.CheckOnClick = true;
            this.loadLastEpromOnStartToolStripMenuItem.Name = "loadLastEpromOnStartToolStripMenuItem";
            this.loadLastEpromOnStartToolStripMenuItem.Size = new System.Drawing.Size(281, 28);
            this.loadLastEpromOnStartToolStripMenuItem.Text = "Load last eprom on start";
            this.loadLastEpromOnStartToolStripMenuItem.Click += new System.EventHandler(this.loadLastEpromOnStartToolStripMenuItem_Click);
            // 
            // setEpromDirtoolStrip
            // 
            this.setEpromDirtoolStrip.Enabled = false;
            this.setEpromDirtoolStrip.Name = "setEpromDirtoolStrip";
            this.setEpromDirtoolStrip.Size = new System.Drawing.Size(281, 28);
            this.setEpromDirtoolStrip.Text = "Set eprom directory";
            this.setEpromDirtoolStrip.ToolTipText = "N/A";
            this.setEpromDirtoolStrip.Visible = false;
            this.setEpromDirtoolStrip.Click += new System.EventHandler(this.toolStripSetEpromDir_Click);
            // 
            // saveOptionsNowToolStripMenuItem
            // 
            this.saveOptionsNowToolStripMenuItem.Name = "saveOptionsNowToolStripMenuItem";
            this.saveOptionsNowToolStripMenuItem.Size = new System.Drawing.Size(281, 28);
            this.saveOptionsNowToolStripMenuItem.Text = "Save Options now";
            this.saveOptionsNowToolStripMenuItem.Click += new System.EventHandler(this.saveOptionsNowToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(59, 27);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(141, 28);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPage1);
            this.tabControlMain.Controls.Add(this.tabPage2);
            this.tabControlMain.Controls.Add(this.tabPage3);
            this.tabControlMain.Controls.Add(this.tabPage4);
            this.tabControlMain.Controls.Add(this.tabPage5);
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.Enabled = false;
            this.tabControlMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControlMain.Location = new System.Drawing.Point(0, 61);
            this.tabControlMain.Margin = new System.Windows.Forms.Padding(4);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(1109, 693);
            this.tabControlMain.TabIndex = 3;
            this.tabControlMain.Visible = false;
            this.tabControlMain.SelectedIndexChanged += new System.EventHandler(this.tabControlMain_SelectedIndexChanged);
            this.tabControlMain.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tabControlMain_KeyDown);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage1.Controls.Add(this.listViewIDMainInfo);
            this.tabPage1.Controls.Add(this.listViewID);
            this.tabPage1.Controls.Add(this.pictureBoxImg);
            this.tabPage1.Controls.Add(this.pictureBoxLogo);
            this.tabPage1.Location = new System.Drawing.Point(4, 33);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(1101, 656);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Identification";
            // 
            // listViewIDMainInfo
            // 
            this.listViewIDMainInfo.BackColor = System.Drawing.SystemColors.Control;
            this.listViewIDMainInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listViewIDMainInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listViewIDMainInfo.ForeColor = System.Drawing.SystemColors.WindowText;
            this.listViewIDMainInfo.FullRowSelect = true;
            this.listViewIDMainInfo.HideSelection = false;
            this.listViewIDMainInfo.Location = new System.Drawing.Point(123, 7);
            this.listViewIDMainInfo.Margin = new System.Windows.Forms.Padding(4);
            this.listViewIDMainInfo.Name = "listViewIDMainInfo";
            this.listViewIDMainInfo.Size = new System.Drawing.Size(770, 99);
            this.listViewIDMainInfo.TabIndex = 3;
            this.listViewIDMainInfo.UseCompatibleStateImageBehavior = false;
            this.listViewIDMainInfo.View = System.Windows.Forms.View.List;
            this.listViewIDMainInfo.Visible = false;
            // 
            // listViewID
            // 
            this.listViewID.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewID.BackColor = System.Drawing.Color.AliceBlue;
            this.listViewID.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.column1,
            this.column2});
            this.listViewID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listViewID.FullRowSelect = true;
            this.listViewID.GridLines = true;
            this.listViewID.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewID.HideSelection = false;
            this.listViewID.Location = new System.Drawing.Point(5, 114);
            this.listViewID.Margin = new System.Windows.Forms.Padding(4);
            this.listViewID.Name = "listViewID";
            this.listViewID.Scrollable = false;
            this.listViewID.Size = new System.Drawing.Size(1082, 533);
            this.listViewID.TabIndex = 2;
            this.listViewID.UseCompatibleStateImageBehavior = false;
            this.listViewID.View = System.Windows.Forms.View.Details;
            this.listViewID.Visible = false;
            this.listViewID.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listViewID_DrawColumnHeader);
            this.listViewID.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.listViewID_DrawItem);
            // 
            // column1
            // 
            this.column1.Text = "Наименование";
            this.column1.Width = 385;
            // 
            // column2
            // 
            this.column2.Text = "Значение";
            this.column2.Width = 425;
            // 
            // pictureBoxImg
            // 
            this.pictureBoxImg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxImg.Location = new System.Drawing.Point(901, 6);
            this.pictureBoxImg.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxImg.Name = "pictureBoxImg";
            this.pictureBoxImg.Size = new System.Drawing.Size(189, 100);
            this.pictureBoxImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxImg.TabIndex = 1;
            this.pictureBoxImg.TabStop = false;
            this.pictureBoxImg.Visible = false;
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxLogo.Location = new System.Drawing.Point(5, 6);
            this.pictureBoxLogo.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(108, 100);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TabIndex = 0;
            this.pictureBoxLogo.TabStop = false;
            this.pictureBoxLogo.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.listViewConfig);
            this.tabPage2.Location = new System.Drawing.Point(4, 33);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(1101, 656);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Configuration";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // listViewConfig
            // 
            this.listViewConfig.BackColor = System.Drawing.Color.Azure;
            this.listViewConfig.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnConfig1,
            this.columnConfig2,
            this.columnConfig3});
            this.listViewConfig.Enabled = false;
            this.listViewConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listViewConfig.FullRowSelect = true;
            this.listViewConfig.GridLines = true;
            this.listViewConfig.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewConfig.HideSelection = false;
            this.listViewConfig.Location = new System.Drawing.Point(0, 0);
            this.listViewConfig.Margin = new System.Windows.Forms.Padding(4);
            this.listViewConfig.Name = "listViewConfig";
            this.listViewConfig.Size = new System.Drawing.Size(1105, 660);
            this.listViewConfig.TabIndex = 0;
            this.listViewConfig.UseCompatibleStateImageBehavior = false;
            this.listViewConfig.View = System.Windows.Forms.View.Details;
            this.listViewConfig.Visible = false;
            this.listViewConfig.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listViewConfig_KeyDown);
            this.listViewConfig.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewConfig_MouseDoubleClick);
            // 
            // columnConfig1
            // 
            this.columnConfig1.Text = "Наименование";
            this.columnConfig1.Width = 504;
            // 
            // columnConfig2
            // 
            this.columnConfig2.Text = "Значение";
            this.columnConfig2.Width = 327;
            // 
            // columnConfig3
            // 
            this.columnConfig3.Text = "Помощь";
            this.columnConfig3.Width = 319;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.listViewEquipment);
            this.tabPage3.Location = new System.Drawing.Point(4, 33);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1101, 656);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Equipment";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // listViewEquipment
            // 
            this.listViewEquipment.BackColor = System.Drawing.Color.Honeydew;
            this.listViewEquipment.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnEquipment1,
            this.columnEquipment2,
            this.columnEquipment3});
            this.listViewEquipment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewEquipment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listViewEquipment.FullRowSelect = true;
            this.listViewEquipment.GridLines = true;
            this.listViewEquipment.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewEquipment.HideSelection = false;
            this.listViewEquipment.Location = new System.Drawing.Point(0, 0);
            this.listViewEquipment.Margin = new System.Windows.Forms.Padding(4);
            this.listViewEquipment.Name = "listViewEquipment";
            this.listViewEquipment.Size = new System.Drawing.Size(1101, 656);
            this.listViewEquipment.TabIndex = 0;
            this.listViewEquipment.UseCompatibleStateImageBehavior = false;
            this.listViewEquipment.View = System.Windows.Forms.View.Details;
            this.listViewEquipment.Visible = false;
            this.listViewEquipment.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewEquipment_MouseDoubleClick);
            // 
            // columnEquipment1
            // 
            this.columnEquipment1.Text = "Наименование";
            this.columnEquipment1.Width = 510;
            // 
            // columnEquipment2
            // 
            this.columnEquipment2.Text = "Значение";
            this.columnEquipment2.Width = 315;
            // 
            // columnEquipment3
            // 
            this.columnEquipment3.Text = "Помощь";
            this.columnEquipment3.Width = 181;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.listViewSafety);
            this.tabPage4.Location = new System.Drawing.Point(4, 33);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1101, 656);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Passive safety";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // listViewSafety
            // 
            this.listViewSafety.BackColor = System.Drawing.Color.Ivory;
            this.listViewSafety.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnSafety1,
            this.columnSafety2,
            this.columnSafety3});
            this.listViewSafety.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewSafety.Enabled = false;
            this.listViewSafety.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listViewSafety.FullRowSelect = true;
            this.listViewSafety.GridLines = true;
            this.listViewSafety.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewSafety.HideSelection = false;
            this.listViewSafety.Location = new System.Drawing.Point(0, 0);
            this.listViewSafety.Margin = new System.Windows.Forms.Padding(4);
            this.listViewSafety.Name = "listViewSafety";
            this.listViewSafety.Size = new System.Drawing.Size(1101, 656);
            this.listViewSafety.TabIndex = 0;
            this.listViewSafety.UseCompatibleStateImageBehavior = false;
            this.listViewSafety.View = System.Windows.Forms.View.Details;
            this.listViewSafety.Visible = false;
            this.listViewSafety.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewSafety_MouseDoubleClick);
            // 
            // columnSafety1
            // 
            this.columnSafety1.Text = "Наименование";
            this.columnSafety1.Width = 524;
            // 
            // columnSafety2
            // 
            this.columnSafety2.Text = "Значение";
            this.columnSafety2.Width = 441;
            // 
            // columnSafety3
            // 
            this.columnSafety3.Text = "Помощь";
            this.columnSafety3.Width = 240;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel1);
            this.tabPage5.Location = new System.Drawing.Point(4, 33);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1101, 656);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Instruments";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.groupBoxBCMClone);
            this.panel1.Controls.Add(this.groupBoxECUBCM);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBoxCheckSum);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1101, 656);
            this.panel1.TabIndex = 0;
            // 
            // groupBoxBCMClone
            // 
            this.groupBoxBCMClone.Controls.Add(this.buttonBCMClone);
            this.groupBoxBCMClone.Location = new System.Drawing.Point(561, 382);
            this.groupBoxBCMClone.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxBCMClone.Name = "groupBoxBCMClone";
            this.groupBoxBCMClone.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxBCMClone.Size = new System.Drawing.Size(400, 103);
            this.groupBoxBCMClone.TabIndex = 9;
            this.groupBoxBCMClone.TabStop = false;
            this.groupBoxBCMClone.Text = "Клонирование BCM";
            // 
            // buttonBCMClone
            // 
            this.buttonBCMClone.Location = new System.Drawing.Point(243, 68);
            this.buttonBCMClone.Margin = new System.Windows.Forms.Padding(4);
            this.buttonBCMClone.Name = "buttonBCMClone";
            this.buttonBCMClone.Size = new System.Drawing.Size(149, 28);
            this.buttonBCMClone.TabIndex = 0;
            this.buttonBCMClone.Text = "Начать";
            this.buttonBCMClone.UseVisualStyleBackColor = true;
            this.buttonBCMClone.Click += new System.EventHandler(this.buttonBCMClone_Click);
            // 
            // groupBoxECUBCM
            // 
            this.groupBoxECUBCM.Controls.Add(this.SyncControlSum_checkBox);
            this.groupBoxECUBCM.Controls.Add(this.buttonECUBCM);
            this.groupBoxECUBCM.Location = new System.Drawing.Point(148, 382);
            this.groupBoxECUBCM.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxECUBCM.Name = "groupBoxECUBCM";
            this.groupBoxECUBCM.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxECUBCM.Size = new System.Drawing.Size(405, 103);
            this.groupBoxECUBCM.TabIndex = 8;
            this.groupBoxECUBCM.TabStop = false;
            this.groupBoxECUBCM.Text = "Синхронизация ECU-BCM";
            // 
            // SyncControlSum_checkBox
            // 
            this.SyncControlSum_checkBox.AutoSize = true;
            this.SyncControlSum_checkBox.Checked = true;
            this.SyncControlSum_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SyncControlSum_checkBox.Location = new System.Drawing.Point(11, 68);
            this.SyncControlSum_checkBox.Name = "SyncControlSum_checkBox";
            this.SyncControlSum_checkBox.Size = new System.Drawing.Size(210, 28);
            this.SyncControlSum_checkBox.TabIndex = 2;
            this.SyncControlSum_checkBox.Text = "Контрольная сумма";
            this.SyncControlSum_checkBox.UseVisualStyleBackColor = true;
            // 
            // buttonECUBCM
            // 
            this.buttonECUBCM.Location = new System.Drawing.Point(248, 68);
            this.buttonECUBCM.Margin = new System.Windows.Forms.Padding(4);
            this.buttonECUBCM.Name = "buttonECUBCM";
            this.buttonECUBCM.Size = new System.Drawing.Size(149, 28);
            this.buttonECUBCM.TabIndex = 1;
            this.buttonECUBCM.Text = "Начать";
            this.buttonECUBCM.UseVisualStyleBackColor = true;
            this.buttonECUBCM.Click += new System.EventHandler(this.buttonECUBCM_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.textBoxVin);
            this.groupBox1.Controls.Add(this.buttonVinChange);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox1.Location = new System.Drawing.Point(148, 110);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(813, 73);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "VIN";
            // 
            // textBoxVin
            // 
            this.textBoxVin.Location = new System.Drawing.Point(11, 32);
            this.textBoxVin.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxVin.MaxLength = 17;
            this.textBoxVin.Name = "textBoxVin";
            this.textBoxVin.Size = new System.Drawing.Size(301, 29);
            this.textBoxVin.TabIndex = 0;
            this.textBoxVin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxVin_KeyPress);
            this.textBoxVin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxVin_KeyUp);
            // 
            // buttonVinChange
            // 
            this.buttonVinChange.Location = new System.Drawing.Point(656, 37);
            this.buttonVinChange.Margin = new System.Windows.Forms.Padding(4);
            this.buttonVinChange.Name = "buttonVinChange";
            this.buttonVinChange.Size = new System.Drawing.Size(149, 28);
            this.buttonVinChange.TabIndex = 1;
            this.buttonVinChange.Text = "Изменить";
            this.buttonVinChange.UseVisualStyleBackColor = true;
            this.buttonVinChange.Click += new System.EventHandler(this.buttonVinChange_Click);
            // 
            // groupBoxCheckSum
            // 
            this.groupBoxCheckSum.Controls.Add(this.buttonCheckSum);
            this.groupBoxCheckSum.Controls.Add(this.checkBoxVIN);
            this.groupBoxCheckSum.Controls.Add(this.checkBoxFile);
            this.groupBoxCheckSum.Controls.Add(this.checkBoxBlock1);
            this.groupBoxCheckSum.Controls.Add(this.checkBoxBlock3);
            this.groupBoxCheckSum.Controls.Add(this.checkBoxBlock2);
            this.groupBoxCheckSum.Location = new System.Drawing.Point(148, 182);
            this.groupBoxCheckSum.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxCheckSum.Name = "groupBoxCheckSum";
            this.groupBoxCheckSum.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxCheckSum.Size = new System.Drawing.Size(813, 199);
            this.groupBoxCheckSum.TabIndex = 6;
            this.groupBoxCheckSum.TabStop = false;
            this.groupBoxCheckSum.Text = "Контрольная сумма";
            // 
            // buttonCheckSum
            // 
            this.buttonCheckSum.Location = new System.Drawing.Point(656, 164);
            this.buttonCheckSum.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCheckSum.Name = "buttonCheckSum";
            this.buttonCheckSum.Size = new System.Drawing.Size(149, 28);
            this.buttonCheckSum.TabIndex = 6;
            this.buttonCheckSum.Text = "Пересчитать";
            this.buttonCheckSum.UseVisualStyleBackColor = true;
            this.buttonCheckSum.Click += new System.EventHandler(this.buttonCheckSum_Click);
            // 
            // checkBoxVIN
            // 
            this.checkBoxVIN.AutoSize = true;
            this.checkBoxVIN.Checked = true;
            this.checkBoxVIN.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxVIN.Enabled = false;
            this.checkBoxVIN.Location = new System.Drawing.Point(8, 28);
            this.checkBoxVIN.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxVIN.Name = "checkBoxVIN";
            this.checkBoxVIN.Size = new System.Drawing.Size(63, 28);
            this.checkBoxVIN.TabIndex = 3;
            this.checkBoxVIN.Text = "VIN";
            this.checkBoxVIN.UseVisualStyleBackColor = true;
            // 
            // checkBoxFile
            // 
            this.checkBoxFile.AutoSize = true;
            this.checkBoxFile.Checked = true;
            this.checkBoxFile.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFile.Location = new System.Drawing.Point(8, 166);
            this.checkBoxFile.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxFile.Name = "checkBoxFile";
            this.checkBoxFile.Size = new System.Drawing.Size(65, 28);
            this.checkBoxFile.TabIndex = 5;
            this.checkBoxFile.Text = "Все";
            this.checkBoxFile.UseVisualStyleBackColor = true;
            this.checkBoxFile.CheckedChanged += new System.EventHandler(this.checkBoxFile_CheckedChanged);
            // 
            // checkBoxBlock1
            // 
            this.checkBoxBlock1.AutoSize = true;
            this.checkBoxBlock1.Checked = true;
            this.checkBoxBlock1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBlock1.Enabled = false;
            this.checkBoxBlock1.Location = new System.Drawing.Point(8, 63);
            this.checkBoxBlock1.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxBlock1.Name = "checkBoxBlock1";
            this.checkBoxBlock1.Size = new System.Drawing.Size(90, 28);
            this.checkBoxBlock1.TabIndex = 4;
            this.checkBoxBlock1.Text = "Блок 1";
            this.checkBoxBlock1.UseVisualStyleBackColor = true;
            // 
            // checkBoxBlock3
            // 
            this.checkBoxBlock3.AutoSize = true;
            this.checkBoxBlock3.Checked = true;
            this.checkBoxBlock3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBlock3.Enabled = false;
            this.checkBoxBlock3.Location = new System.Drawing.Point(8, 132);
            this.checkBoxBlock3.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxBlock3.Name = "checkBoxBlock3";
            this.checkBoxBlock3.Size = new System.Drawing.Size(90, 28);
            this.checkBoxBlock3.TabIndex = 4;
            this.checkBoxBlock3.Text = "Блок 3";
            this.checkBoxBlock3.UseVisualStyleBackColor = true;
            // 
            // checkBoxBlock2
            // 
            this.checkBoxBlock2.AutoSize = true;
            this.checkBoxBlock2.Checked = true;
            this.checkBoxBlock2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBlock2.Enabled = false;
            this.checkBoxBlock2.Location = new System.Drawing.Point(8, 97);
            this.checkBoxBlock2.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxBlock2.Name = "checkBoxBlock2";
            this.checkBoxBlock2.Size = new System.Drawing.Size(90, 28);
            this.checkBoxBlock2.TabIndex = 4;
            this.checkBoxBlock2.Text = "Блок 2";
            this.checkBoxBlock2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(135, 96);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(843, 400);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonOpen,
            this.toolStripButtonSave,
            this.toolStripButtonFind,
            this.toolStripTextBoxSearch});
            this.toolStrip1.Location = new System.Drawing.Point(0, 31);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1109, 30);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonOpen
            // 
            this.toolStripButtonOpen.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOpen.Image")));
            this.toolStripButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpen.Margin = new System.Windows.Forms.Padding(12, 1, 0, 2);
            this.toolStripButtonOpen.Name = "toolStripButtonOpen";
            this.toolStripButtonOpen.Size = new System.Drawing.Size(100, 27);
            this.toolStripButtonOpen.Text = "Открыть";
            this.toolStripButtonOpen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripButtonOpen.ToolTipText = "Считать eprom из файла";
            this.toolStripButtonOpen.Click += new System.EventHandler(this.toolStripButtonOpen_Click);
            // 
            // toolStripButtonSave
            // 
            this.toolStripButtonSave.Enabled = false;
            this.toolStripButtonSave.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSave.Image")));
            this.toolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSave.Name = "toolStripButtonSave";
            this.toolStripButtonSave.Size = new System.Drawing.Size(118, 27);
            this.toolStripButtonSave.Text = "Сохранить";
            this.toolStripButtonSave.ToolTipText = "Сохранить eprom в файл";
            this.toolStripButtonSave.Click += new System.EventHandler(this.toolStripButtonSave_Click);
            // 
            // toolStripButtonFind
            // 
            this.toolStripButtonFind.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonFind.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFind.Enabled = false;
            this.toolStripButtonFind.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFind.Image")));
            this.toolStripButtonFind.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFind.Name = "toolStripButtonFind";
            this.toolStripButtonFind.Size = new System.Drawing.Size(29, 27);
            this.toolStripButtonFind.Text = "Далее";
            this.toolStripButtonFind.Click += new System.EventHandler(this.toolStripButtonFind_Click);
            // 
            // toolStripTextBoxSearch
            // 
            this.toolStripTextBoxSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripTextBoxSearch.Font = new System.Drawing.Font("Segoe UI", 10.2F);
            this.toolStripTextBoxSearch.Name = "toolStripTextBoxSearch";
            this.toolStripTextBoxSearch.Size = new System.Drawing.Size(132, 30);
            this.toolStripTextBoxSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.toolStripTextBoxSearch_KeyDown);
            this.toolStripTextBoxSearch.TextChanged += new System.EventHandler(this.toolStripTextBoxSearch_TextChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 754);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(1127, 1220);
            this.MinimumSize = new System.Drawing.Size(1127, 481);
            this.Name = "MainForm";
            this.Text = "BCM tools";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControlMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBoxBCMClone.ResumeLayout(false);
            this.groupBoxECUBCM.ResumeLayout(false);
            this.groupBoxECUBCM.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxCheckSum.ResumeLayout(false);
            this.groupBoxCheckSum.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem languageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.ToolStripMenuItem readEpromToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savepromToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savepromAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadLastEpromOnStartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveOptionsNowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setEpromDirtoolStrip;
        private System.Windows.Forms.PictureBox pictureBoxImg;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.ColumnHeader column1;
        private System.Windows.Forms.ColumnHeader column2;
        private System.Windows.Forms.ListView listViewIDMainInfo;
        private System.Windows.Forms.ListView listViewID;
        private System.Windows.Forms.ListView listViewConfig;
        private System.Windows.Forms.ColumnHeader columnConfig1;
        private System.Windows.Forms.ColumnHeader columnConfig2;
        private System.Windows.Forms.ColumnHeader columnConfig3;
        private System.Windows.Forms.ListView listViewEquipment;
        private System.Windows.Forms.ColumnHeader columnEquipment1;
        private System.Windows.Forms.ColumnHeader columnEquipment2;
        private System.Windows.Forms.ColumnHeader columnEquipment3;
        private System.Windows.Forms.ListView listViewSafety;
        private System.Windows.Forms.ColumnHeader columnSafety1;
        private System.Windows.Forms.ColumnHeader columnSafety2;
        private System.Windows.Forms.ColumnHeader columnSafety3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBoxCheckSum;
        private System.Windows.Forms.Button buttonCheckSum;
        private System.Windows.Forms.CheckBox checkBoxVIN;
        private System.Windows.Forms.CheckBox checkBoxFile;
        private System.Windows.Forms.CheckBox checkBoxBlock1;
        private System.Windows.Forms.CheckBox checkBoxBlock3;
        private System.Windows.Forms.CheckBox checkBoxBlock2;
        private System.Windows.Forms.Button buttonVinChange;
        private System.Windows.Forms.TextBox textBoxVin;
        private System.Windows.Forms.GroupBox groupBoxBCMClone;
        private System.Windows.Forms.Button buttonBCMClone;
        private System.Windows.Forms.GroupBox groupBoxECUBCM;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonECUBCM;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonOpen;
        private System.Windows.Forms.ToolStripButton toolStripButtonSave;
        private System.Windows.Forms.ToolStripButton toolStripButtonFind;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxSearch;
        private System.Windows.Forms.CheckBox SyncControlSum_checkBox;
    }
}

