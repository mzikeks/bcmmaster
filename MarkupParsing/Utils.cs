﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCMmaster;

namespace BCMmaster.MarkupParsing
{
    public struct DataParamDescriptor
    {
        public int paramOffset;
        public int paramLength;
        public int bitMask;
        public string paramDescriptionRu;
        public string paramDescriptionEn;
        public paramEncoding encodingType;
        public listType listTab;
        public int positionInList;
        public string additionalInfo;
        public byte[] currDataBytes;
        public int currCaseSelector;
        public DataParamDescriptor[] linked;

        public DataParamDescriptor(
            int _paramOffset,
            int _paramLength,
            int _bitMask,
            string _paramDescriptionRu,
            string _paramDescriptionEn,
            paramEncoding _encodingType,
            listType _listTab,
            int _positionInList,
            string _additionalInfo,
            DataParamDescriptor[] linked = null
            //byte[] currDataBytes,
            //int currCaseSelector
            ) : this()
        {
            paramOffset = _paramOffset;
            paramLength = _paramLength;
            bitMask = _bitMask;
            paramDescriptionRu = _paramDescriptionRu;
            paramDescriptionEn = _paramDescriptionEn;
            encodingType = _encodingType;
            listTab = _listTab;
            positionInList = _positionInList;
            additionalInfo = _additionalInfo;
            currCaseSelector = 0;
            currDataBytes = new byte[PropertiesMarkup.MAX_DATA_SIZE_IN_BYTES];
            this.linked = linked;
        }
    }


    public enum paramEncoding
    {
        String = 0,             // +
        Bits,                   // +
        maskBits,               // +
        maskBitsYesNo,          // +
        Word,                   // +
        WordInverted,           // +
        Byte,                   //
        Date,   // год 2б, месяц 1б, день 1б 0x20 0x14 0x5 0x8 == 2014/05/08
        TwoBit, // как для коробки // +
//      Dword,
        Unknown, 
        maskBitsNoYes,
    };

    public enum listType
    {
        IDList = 0,
        ConfigList,
        EquipmentList,
        PassiveSafetyList,
        Other
    };
}
