﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BCMmaster
{
    public class EpromMarkup
    {
        private static EpromMarkup BCS20I_Markup = CreateBCS20I_Markup();
        private static EpromMarkup BCS20J_Markup = CreateBCS20J_Markup();
        private static EpromMarkup BC250I_Markup = CreateBC250I_Markup();
        private static EpromMarkup BC250L_Markup = null;
        private static EpromMarkup BC250C_Markup = null;


        public PropertiesMarkup propertiesMarkup { get; private set; } = null;

        public EpromMarkup(string name)
        {
            this.EpromName = name;
        }

        public static EpromMarkup GetMarkup(string name)
        {            
            if (name == "BCS20I")
            {
                return BCS20I_Markup;
            }
            else if (name == "BCS20J")
            {
                return BCS20J_Markup;
            }
            else
            {
                return BC250I_Markup;
            }
        }

        private static EpromMarkup CreateBC250I_Markup()
        {
            var eprom = new EpromMarkup("BC250I");
            eprom.propertiesMarkup = PropertiesMarkup.GePropertiestMarkup(eprom.EpromName);

            // редакция программного обеспечения 2 байта 
            eprom.OFFSET_SOFT_VERSION = 0x34; // 

            eprom.SIZE_DATA_BLOCK = 0x151;

            // DataBlock1 длина 0x154==340 байт 0x204 – 0x357 (CRC word offset 0x356)
            eprom.OFFSET_DATA_BLOCK1 = 0x204;
            eprom.OFFSET_DATA_BLOCK1_CHECKSUM = 0x356;

            // DataBlock2 длина 0x154==340 байт 0x368 – 0x4BB  (CRC word offset 0x4BA)
            eprom.OFFSET_DATA_BLOCK2 = 0x368;
            eprom.OFFSET_DATA_BLOCK2_CHECKSUM = 0x4BA;

            // DataBlock3 длина 0x154==340 байт 0x4BC – 0x60F  (CRC word offset 0x60E)
            eprom.OFFSET_DATA_BLOCK3 = 0x4BC;
            eprom.OFFSET_DATA_BLOCK3_CHECKSUM = 0x60E;


            eprom.OFFSET_FILE_CHECKSUM = 0x60E; //???

            return eprom;
        }

        private static EpromMarkup CreateBCS20I_Markup()
        {
            var eprom = new EpromMarkup("BCS20I");
            eprom.propertiesMarkup = PropertiesMarkup.GePropertiestMarkup(eprom.EpromName);

            // редакция программного обеспечения 2 байта 
            eprom.OFFSET_SOFT_VERSION = 0x34; // 


            eprom.SIZE_DATA_BLOCK = 0x159;

            eprom.OFFSET_DATA_BLOCK1 = 0x204;
            eprom.OFFSET_DATA_BLOCK1_CHECKSUM = 0x35E;

            eprom.OFFSET_DATA_BLOCK2 = 0x370;
            eprom.OFFSET_DATA_BLOCK2_CHECKSUM = 0x4CA;

            eprom.OFFSET_DATA_BLOCK3 = 0x4CC;
            eprom.OFFSET_DATA_BLOCK3_CHECKSUM = 0x626;


            eprom.OFFSET_FILE_CHECKSUM = 0x626; //???
            return eprom;
        }

        private static EpromMarkup CreateBCS20J_Markup()
        {
            var eprom = new EpromMarkup("BCS20J");
            eprom.propertiesMarkup = PropertiesMarkup.GePropertiestMarkup(eprom.EpromName);

            // редакция программного обеспечения 2 байта 
            eprom.OFFSET_SOFT_VERSION = 0x34; // 


            eprom.SIZE_DATA_BLOCK = 0x101;

            eprom.OFFSET_DATA_BLOCK1 = 0x204;
            eprom.OFFSET_DATA_BLOCK1_CHECKSUM = 0x306;

            eprom.OFFSET_DATA_BLOCK2 = 0x308;
            eprom.OFFSET_DATA_BLOCK2_CHECKSUM = 0x40A;

            eprom.OFFSET_DATA_BLOCK3 = -1; // Не используется, всего 2 блока
            eprom.OFFSET_DATA_BLOCK3_CHECKSUM = -1;


            eprom.OFFSET_FILE_CHECKSUM = 0x626; //???
            return eprom;
        }


        public string EpromName { get; private set; }
        public static int MAX_EPROM_FILE_LENGTH { get => 8 * 1024; }

        // редакция программного обеспечения
        public int OFFSET_SOFT_VERSION { get; private set; }

        // VIN номер 17 байт 0x1E66-0x1E77 (0x1E78-0x1E79 checkSum), для всех машин одинаковый, вроде как.
        // Так как по нему определяется тип машины
        public static int OFFSET_VIN { get; } = 0x1E60; // + 6 ведущих пробелов
        public static int OFFSET_VIN_STRING { get; } = 0x1E66; // 

        public static int SIZE_VIN { get; } = 24;
        public static int SIZE_VIN_STRING { get; } = 17;

        public static int OFFSET_VIN_CHECKSUM { get; } = 0x1E78;


        public int SIZE_DATA_BLOCK { get; private set; }

        // DataBlock1
        public int OFFSET_DATA_BLOCK1 { get; private set; }
        public int OFFSET_DATA_BLOCK1_CHECKSUM { get; private set; }

        // DataBlock2
        public int OFFSET_DATA_BLOCK2 { get; private set; }
        public int OFFSET_DATA_BLOCK2_CHECKSUM { get; private set; }

        // DataBlock3
        public int OFFSET_DATA_BLOCK3 { get; private set; }
        public int OFFSET_DATA_BLOCK3_CHECKSUM { get; private set; }

        public int OFFSET_FILE_CHECKSUM { get; private set; }

        // Тоже для всех машин один, так как по нему определяется модель.
        public static int OFFSET_CAR_MODEL_BYTE { get; } = 0x2A0;

    }
}
