﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BCMmaster.MarkupParsing;

namespace BCMmaster
{
    public class PropertiesMarkup
    {
        private static PropertiesMarkup BCS20I_PropertiesMarkup = CreateBCS20I_PropertiesMarkup();
        private static PropertiesMarkup BC250I_PropertiesMarkup = CreateBC250I_PropertiesMarkup();
        private static PropertiesMarkup BC250L_PropertiesMarkup = null;
        private static PropertiesMarkup BC250C_PropertiesMarkup = null;

        public DataParamDescriptor[] DataBlockParamsList = new DataParamDescriptor[MAX_PARAMS_IN_DATA_BLOCK];

        public PropertiesMarkup(string epromName)
        {
            this.epromName = epromName;
        }

        public static PropertiesMarkup GePropertiestMarkup(string name)
        {
            if (name == "BCS20I" || name == "BCS20J")
            {
                return CreateBCS20I_PropertiesMarkup();
            }
            else
            {
                return CreateBC250I_PropertiesMarkup();
            }

        }


        private static PropertiesMarkup CreateBCS20I_PropertiesMarkup()
        {
            PropertiesMarkup markup = new PropertiesMarkup("BCS20I");
            // Идентификация
            markup.DataBlockParamsList[0] = new DataParamDescriptor(0x219, 0, 0, "Дата загрузки данных", "Data loading date", paramEncoding.Date, listType.IDList, 7, "");
            markup.DataBlockParamsList[1] = new DataParamDescriptor(0, 0, 0, "Версия оборудования", "Equipment version", paramEncoding.Unknown, listType.IDList, 2, "");
            markup.DataBlockParamsList[2] = new DataParamDescriptor(0x36, 0xB, 0, "Референс ПО", "Soft reference", paramEncoding.String, listType.IDList, 3, "");
            markup.DataBlockParamsList[3] = new DataParamDescriptor(0x4C, 0x4, 0, "Редакция ПО", "Soft edition", paramEncoding.String, listType.IDList, 4, "");
            markup.DataBlockParamsList[4] = new DataParamDescriptor(0x1E3C, 0xA, 0, "Сквозной референс", "Flat reference", paramEncoding.String, listType.IDList, 5, "");
            markup.DataBlockParamsList[5] = new DataParamDescriptor(0x1E47, 0xB, 0, "Референс оборудования", "Equipment reference", paramEncoding.String, listType.IDList, 1, "");
            markup.DataBlockParamsList[6] = new DataParamDescriptor(0X1E59, 0x5, 0, "ISO", "ISO", paramEncoding.Bits, listType.IDList, 0, "");
            markup.DataBlockParamsList[7] = new DataParamDescriptor(0x1E66, 0x11, 0, "VIN", "VIN", paramEncoding.String, listType.IDList, 6, "");

            // Конфигурация
            markup.DataBlockParamsList[8] = new DataParamDescriptor(0x204, 0xB, 0, "Код конфигурации", "Configuration Code", paramEncoding.String, listType.ConfigList, 0, ""); // 08250260106
            markup.DataBlockParamsList[9] = new DataParamDescriptor(0x20F, 0xA, 0, "Код прибора для телекодирования", "Telecoder device code", paramEncoding.String, listType.ConfigList, 1, ""); // OUTPUTDJIT
            markup.DataBlockParamsList[10] = new DataParamDescriptor(0x219, 4, 0, "Дата выпуска", "Production date", paramEncoding.Date, listType.ConfigList, 2, ""); // 08 / 05 / 2014 
            markup.DataBlockParamsList[11] = new DataParamDescriptor(0, 0, 0, "Интеллектуальный коммутационный блок", "Smart commutation block", paramEncoding.maskBits, listType.ConfigList, 3, ""); // Имеется
            markup.DataBlockParamsList[12] = new DataParamDescriptor(0, 0, 0, "Интеллектуальный коммутационный блок телекодирован на заводе", "Smart commutation block is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 4, ""); // Да
            markup.DataBlockParamsList[13] = new DataParamDescriptor(0, 0, 0, "Компьютер двигателя", "Engine computer unit", paramEncoding.maskBits, listType.ConfigList, 5, ""); // Имеется
            markup.DataBlockParamsList[14] = new DataParamDescriptor(0, 0, 0, "Панель приборов", "Dashboard", paramEncoding.maskBits, listType.ConfigList, 6, ""); // Имеется
            markup.DataBlockParamsList[15] = new DataParamDescriptor(0, 0, 0, "Панель приборов телекодирована на заводе", "Dashboard is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 7, ""); // Да
            markup.DataBlockParamsList[16] = new DataParamDescriptor(0, 0, 0, "Радиоприемник", "Radio", paramEncoding.maskBits, listType.ConfigList, 8, ""); // Отсутствует
            markup.DataBlockParamsList[17] = new DataParamDescriptor(0, 0, 0, "Автомагнитола телекодирована на заводе", "Radio is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 9, ""); // Нет
            markup.DataBlockParamsList[18] = new DataParamDescriptor(0, 0, 0, "Антиблокировка колес(ABS) / Система динамической стабилизации (ESP)", "ABS / ESP", paramEncoding.maskBits, listType.ConfigList, 10, ""); // Имеется
            markup.DataBlockParamsList[19] = new DataParamDescriptor(0, 0, 0, "Кондиционер", "Air conditioning", paramEncoding.maskBits, listType.ConfigList, 11, ""); // Отсутствует
            markup.DataBlockParamsList[20] = new DataParamDescriptor(0, 0, 0, "Кондиционер телекодирован на заводе", "Air conditioning is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 12, ""); // Нет
            markup.DataBlockParamsList[21] = new DataParamDescriptor(0, 0, 0, "Автоматизированная механическая коробка передач", "Automated transmission", paramEncoding.maskBits, listType.ConfigList, 13, ""); // In development
            markup.DataBlockParamsList[22] = new DataParamDescriptor(0, 0, 0, "Датчик угла поворота рулевого колеса", "Steering angle sensor", paramEncoding.maskBits, listType.ConfigList, 14, ""); // Имеется
            markup.DataBlockParamsList[23] = new DataParamDescriptor(0, 0, 0, "Система помощи при парковке", "Parking aid", paramEncoding.maskBits, listType.ConfigList, 15, ""); // Имеется
            markup.DataBlockParamsList[24] = new DataParamDescriptor(0, 0, 0, "Подушка безопасности", "Airbag", paramEncoding.maskBits, listType.ConfigList, 16, ""); // Имеется
            markup.DataBlockParamsList[25] = new DataParamDescriptor(0, 0, 0, "Подушки безопасности телекодированы на заводе", "Airbags is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 17, ""); // Да

            markup.DataBlockParamsList[26] = new DataParamDescriptor(0, 0, 0, "Пневматическая подвеска", "Air suspension", paramEncoding.Unknown, listType.ConfigList, 18, ""); // In development Отсутствует
            markup.DataBlockParamsList[27] = new DataParamDescriptor(0, 0, 0, "Предупреждение непреднамеренном пересечении линии дорожной разметки", "Unintenional crossing road lines warning", paramEncoding.maskBits, listType.ConfigList, 19, ""); // Отсутствует
            markup.DataBlockParamsList[28] = new DataParamDescriptor(0, 0, 0, "Обнаружение падения давления воздуха в шинах", "Tire pressure drop detection", paramEncoding.maskBits, listType.ConfigList, 20, ""); // Отсутствует
            markup.DataBlockParamsList[29] = new DataParamDescriptor(0, 0, 0, "Обнаружение падения давления воздуха в шинах телекодировано на заводе", "Tire pressure drop detection is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 21, ""); // Нет
            markup.DataBlockParamsList[30] = new DataParamDescriptor(0, 0, 0, "Навигационная система", "Navigation system", paramEncoding.maskBits, listType.ConfigList, 22, ""); // Имеется
            markup.DataBlockParamsList[31] = new DataParamDescriptor(0, 0, 0, "Радионавигация телекодирована на заводе", "Radioavigation system is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 23, ""); // Да
            markup.DataBlockParamsList[32] = new DataParamDescriptor(0, 0, 0, "Обнаружение перегрузки", "Overload detection", paramEncoding.maskBitsYesNo, listType.ConfigList, 23, ""); // Да
            markup.DataBlockParamsList[33] = new DataParamDescriptor(0, 0, 0, "Обнаружение чрезмерного заряда, телекодированного на заводе", "Detection of excessive charge, body coded at the factory", paramEncoding.maskBitsYesNo, listType.ConfigList, 23, ""); // Да
            markup.DataBlockParamsList[34] = new DataParamDescriptor(0, 0, 0, "Цифровой хронотахограф (спидометр, тахометр?)", "Digital chronotachograph", paramEncoding.Unknown, listType.ConfigList, 24, ""); // In development Отсутствует
            markup.DataBlockParamsList[35] = new DataParamDescriptor(0, 0, 0, "Коммутационный блок прицепа", "Trailer switch block", paramEncoding.Unknown, listType.ConfigList, 25, ""); // In development Отсутствует
            markup.DataBlockParamsList[36] = new DataParamDescriptor(0, 0, 0, "Общее число технических обслуживаний", "Maintenance counter", paramEncoding.Unknown, listType.ConfigList, 26, ""); // In development 5

            // Оборудование

            // Остальные 2 значения будут меняться при проверке первого.
            markup.DataBlockParamsList[37] = new DataParamDescriptor(0x266, 1, 0x04, "Наличие хронотахографа", "Digital chronotachograph present", paramEncoding.maskBitsNoYes, listType.EquipmentList, 0, "",
                new DataParamDescriptor[] { new DataParamDescriptor(0x227, 1, 0x40, "Наличие хронотахографа", "Digital chronotachograph present", paramEncoding.maskBitsYesNo, listType.EquipmentList, 0, ""),
               new DataParamDescriptor(0x271, 1, 0x04, "Наличие хронотахографа", "Digital chronotachograph present", paramEncoding.maskBitsYesNo, listType.EquipmentList, 0, "")
            });

            // Для дебага, чтобы убедиться что меняются все дочерние
            // markup.DataBlockParamsList[38] = new DataParamDescriptor(0x227, 1, 0x40, "Наличие хронотахографа", "Digital chronotachograph present", paramEncoding.maskBitsYesNo, listType.EquipmentList, 1, ""); // Да
            // markup.DataBlockParamsList[39] = new DataParamDescriptor(0x271, 1, 0x04, "Наличие хронотахографа", "Digital chronotachograph present", paramEncoding.maskBitsYesNo, listType.EquipmentList, 2, ""); // Да
            return markup;
        }

        private static PropertiesMarkup CreateBC250I_PropertiesMarkup()
        {
            PropertiesMarkup markup = new PropertiesMarkup("BC250I");
            // Идентификация
            markup.DataBlockParamsList[0] = new DataParamDescriptor(0x219, 0, 0, "Дата загрузки данных", "Data loading date", paramEncoding.Date, listType.IDList, 7, "");
            markup.DataBlockParamsList[1] = new DataParamDescriptor(0, 0, 0, "Версия оборудования", "Equipment version", paramEncoding.Unknown, listType.IDList, 2, "");
            markup.DataBlockParamsList[2] = new DataParamDescriptor(0x36, 0xB, 0, "Референс ПО", "Soft reference", paramEncoding.String, listType.IDList, 3, "");
            markup.DataBlockParamsList[3] = new DataParamDescriptor(0x4C, 0x4, 0, "Редакция ПО", "Soft edition", paramEncoding.String, listType.IDList, 4, "");
            markup.DataBlockParamsList[4] = new DataParamDescriptor(0x1E3C, 0xA, 0, "Сквозной референс", "Flat reference", paramEncoding.String, listType.IDList, 5, "");
            markup.DataBlockParamsList[5] = new DataParamDescriptor(0x1E47, 0xB, 0, "Референс оборудования", "Equipment reference", paramEncoding.String, listType.IDList, 1, "");
            markup.DataBlockParamsList[6] = new DataParamDescriptor(0X1E59, 0x5, 0, "ISO", "ISO", paramEncoding.Bits, listType.IDList, 0, "");
            markup.DataBlockParamsList[7] = new DataParamDescriptor(0x1E66, 0x11, 0, "VIN", "VIN", paramEncoding.String, listType.IDList, 6, "");

            // Конфигурация
            markup.DataBlockParamsList[8] = new DataParamDescriptor(0x204, 0xB, 0, "Код конфигурации", "Configuration Code", paramEncoding.String, listType.ConfigList, 0, ""); // 08250260106
            markup.DataBlockParamsList[9] = new DataParamDescriptor(0x20F, 0xA, 0, "Код прибора для телекодирования", "Telecoder device code", paramEncoding.String, listType.ConfigList, 1, ""); // OUTPUTDJIT
            markup.DataBlockParamsList[10] = new DataParamDescriptor(0x219, 4, 0, "Дата выпуска", "Production date", paramEncoding.Date, listType.ConfigList, 2, ""); // 08 / 05 / 2014 
            markup.DataBlockParamsList[11] = new DataParamDescriptor(0x220, 1, 0x01, "Интеллектуальный коммутационный блок", "Smart commutation block", paramEncoding.maskBits, listType.ConfigList, 3, ""); // Имеется
            markup.DataBlockParamsList[12] = new DataParamDescriptor(0x228, 1, 0x01, "Интеллектуальный коммутационный блок телекодирован на заводе", "Smart commutation block is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 4, ""); // Да
            markup.DataBlockParamsList[13] = new DataParamDescriptor(0x220, 1, 0x02, "Компьютер двигателя", "Engine computer unit", paramEncoding.maskBits, listType.ConfigList, 5, ""); // Имеется
            markup.DataBlockParamsList[14] = new DataParamDescriptor(0x220, 1, 0x08, "Панель приборов", "Dashboard", paramEncoding.maskBits, listType.ConfigList, 6, ""); // Имеется
            markup.DataBlockParamsList[15] = new DataParamDescriptor(0x228, 1, 0x08, "Панель приборов телекодирована на заводе", "Dashboard is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 7, ""); // Да
            markup.DataBlockParamsList[16] = new DataParamDescriptor(0x220, 1, 0x20, "Радиоприемник", "Radio", paramEncoding.maskBits, listType.ConfigList, 8, ""); // Отсутствует
            markup.DataBlockParamsList[17] = new DataParamDescriptor(0x228, 1, 0x20, "Автомагнитола телекодирована на заводе", "Radio is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 9, ""); // Нет
            markup.DataBlockParamsList[18] = new DataParamDescriptor(0x220, 1, 0x40, "Антиблокировка колес(ABS) / Система динамической стабилизации (ESP)", "ABS / ESP", paramEncoding.maskBits, listType.ConfigList, 10, ""); // Имеется
            markup.DataBlockParamsList[19] = new DataParamDescriptor(0x221, 1, 0x04, "Кондиционер", "Air conditioning", paramEncoding.maskBits, listType.ConfigList, 11, ""); // Отсутствует
            markup.DataBlockParamsList[20] = new DataParamDescriptor(0x229, 1, 0x04, "Кондиционер телекодирован на заводе", "Air conditioning is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 12, ""); // Нет
            markup.DataBlockParamsList[21] = new DataParamDescriptor(0x221, 1, 0x08, "Автоматизированная механическая коробка передач", "Automated transmission", paramEncoding.maskBits, listType.ConfigList, 13, ""); // In development
            markup.DataBlockParamsList[22] = new DataParamDescriptor(0x221, 1, 0x10, "Датчик угла поворота рулевого колеса", "Steering angle sensor", paramEncoding.maskBits, listType.ConfigList, 14, ""); // Имеется
            markup.DataBlockParamsList[23] = new DataParamDescriptor(0x223, 1, 0x01, "Система помощи при парковке", "Parking aid", paramEncoding.maskBits, listType.ConfigList, 15, ""); // Имеется
            markup.DataBlockParamsList[24] = new DataParamDescriptor(0x223, 1, 0x04, "Подушка безопасности", "Airbag", paramEncoding.maskBits, listType.ConfigList, 16, ""); // Имеется
            markup.DataBlockParamsList[25] = new DataParamDescriptor(0x22B, 1, 0x04, "Подушки безопасности телекодированы на заводе", "Airbags is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 17, ""); // Да

            //-----------------------------------------------------------??
            markup.DataBlockParamsList[26] = new DataParamDescriptor(0, 0, 0, "Пневматическая подвеска", "Air suspension", paramEncoding.Unknown, listType.ConfigList, 18, ""); // In development Отсутствует
            markup.DataBlockParamsList[27] = new DataParamDescriptor(0x223, 1, 0x40, "Предупреждение непреднамеренном пересечении линии дорожной разметки", "Unintenional crossing road lines warning", paramEncoding.maskBits, listType.ConfigList, 19, ""); // Отсутствует
            markup.DataBlockParamsList[28] = new DataParamDescriptor(0x224, 1, 0x1, "Обнаружение падения давления воздуха в шинах", "Tire pressure drop detection", paramEncoding.maskBits, listType.ConfigList, 20, ""); // Отсутствует
            markup.DataBlockParamsList[29] = new DataParamDescriptor(0x22C, 1, 0x1, "Обнаружение падения давления воздуха в шинах телекодировано на заводе", "Tire pressure drop detection is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 21, ""); // Нет
            markup.DataBlockParamsList[30] = new DataParamDescriptor(0x224, 1, 0x2, "Навигационная система", "Navigation system", paramEncoding.maskBits, listType.ConfigList, 22, ""); // Имеется
            markup.DataBlockParamsList[31] = new DataParamDescriptor(0x22C, 1, 0x2, "Навигационная система телекодирована на заводе", "Navigation system is factory coded", paramEncoding.maskBitsYesNo, listType.ConfigList, 23, ""); // Да
            markup.DataBlockParamsList[32] = new DataParamDescriptor(0, 0, 0, "Цифровой хронотахограф (спидометр, тахометр?)", "Digital chronotachograph", paramEncoding.Unknown, listType.ConfigList, 24, ""); // In development Отсутствует
            markup.DataBlockParamsList[33] = new DataParamDescriptor(0, 0, 0, "Коммутационный блок прицепа", "Trailer switch block", paramEncoding.Unknown, listType.ConfigList, 25, ""); // In development Отсутствует
            markup.DataBlockParamsList[34] = new DataParamDescriptor(0, 0, 0, "Общее число технических обслуживаний", "Maintenance counter", paramEncoding.Unknown, listType.ConfigList, 26, ""); // In development 5

            // Оборудование
            markup.DataBlockParamsList[35] = new DataParamDescriptor(0x230, 1, 0x01, "Индикатор переключения передачи коробки передач", "Transmission switch indicator", paramEncoding.maskBits, listType.EquipmentList, 0, ""); // Имеется
            markup.DataBlockParamsList[36] = new DataParamDescriptor(0x230, 1, 0x02, "Старт-Стоп", "Stop and Start", paramEncoding.maskBits, listType.EquipmentList, 1, ""); // 
            markup.DataBlockParamsList[37] = new DataParamDescriptor(0x230, 1, 0x10, "Автомобиль с правосторонним рулевым управлением", "Right side wheel", paramEncoding.maskBitsYesNo, listType.EquipmentList, 2, ""); // Да
            markup.DataBlockParamsList[38] = new DataParamDescriptor(0x230, 1, 0x80, "Наличие передних противотуманных фар", "Front fog lights", paramEncoding.maskBitsYesNo, listType.EquipmentList, 3, ""); // Нет
            markup.DataBlockParamsList[39] = new DataParamDescriptor(0x234, 1, 0x01, "Наличие датчика дождя", "Rain sensor", paramEncoding.maskBitsYesNo, listType.EquipmentList, 4, ""); // Нет
            markup.DataBlockParamsList[40] = new DataParamDescriptor(0x234, 1, 0x04, "Наличие датчика освещенности", "Light sensor", paramEncoding.maskBitsYesNo, listType.EquipmentList, 5, ""); // Нет
            markup.DataBlockParamsList[41] = new DataParamDescriptor(0x234, 1, 0x08, "Наличие омывателя фар", "Headlight washer", paramEncoding.maskBitsYesNo, listType.EquipmentList, 6, ""); // Нет
            markup.DataBlockParamsList[42] = new DataParamDescriptor(0x230, 1, 0x40, "Зеркало заднего вида с антиобледенением", "Anti-icing rearview mirror", paramEncoding.maskBits, listType.EquipmentList, 7, ""); // Имеется
            markup.DataBlockParamsList[43] = new DataParamDescriptor(0x238, 1, 0x01, "Разрешение использования режима Парковка", "Parking mode using permission", paramEncoding.maskBitsYesNo, listType.EquipmentList, 8, ""); // Да
            markup.DataBlockParamsList[44] = new DataParamDescriptor(0x238, 1, 0x02, "Наличие кондиционера с механическими регулировками", "Mechanical adjustment air conditioner", paramEncoding.maskBitsYesNo, listType.EquipmentList, 9, ""); // Да
            markup.DataBlockParamsList[45] = new DataParamDescriptor(0x238, 1, 0x40, "Количество и расположение задних противотуманных фонарей", "Rear fog lights number and position", paramEncoding.Byte, listType.EquipmentList, 10, ""); // 2 противотуманных фонаря (слева и справа)
            markup.DataBlockParamsList[46] = new DataParamDescriptor(0x238, 1, 0x10, "Наличие дневного освещения", "Daytime running lights", paramEncoding.maskBitsYesNo, listType.EquipmentList, 11, ""); // Да
            markup.DataBlockParamsList[47] = new DataParamDescriptor(0x23C, 2, 0x00, "Типоразмер шин", "Tire size", paramEncoding.Word, listType.EquipmentList, 12, ""); // 2080

            //-----------------------------------------------------------??
            markup.DataBlockParamsList[48] = new DataParamDescriptor(0, 0, 0, "Конфигурация автомобиля", "Vehicle configuration", paramEncoding.Unknown, listType.EquipmentList, 13, "todo: исправить адрес и как показывать"); // Модель 1
            markup.DataBlockParamsList[49] = new DataParamDescriptor(0x240, 1, 0x10, "Концевой выключатель состояния капота", "Hood limit switch", paramEncoding.maskBits, listType.EquipmentList, 14, ""); // Имеется
            markup.DataBlockParamsList[50] = new DataParamDescriptor(0, 0, 0, "Сигнал экстренной остановки", "Emergency stop signal", paramEncoding.Unknown, listType.EquipmentList, 15, "todo: исправить маску и смещение"); // Отсутствует
            markup.DataBlockParamsList[51] = new DataParamDescriptor(0, 0, 0, "Тип элементов управления на рулевом колесе", "Steering wheel controls type", paramEncoding.Unknown, listType.EquipmentList, 16, "todo: исправить маску и смещение"); // Отсутствует
            markup.DataBlockParamsList[52] = new DataParamDescriptor(0, 0, 0, "Наличие функции камеры заднего вида", "Rearview camera", paramEncoding.Unknown, listType.EquipmentList, 17, "todo: исправить адрес и как показывать"); // Выключатель с режимом прерывистой работы
            markup.DataBlockParamsList[53] = new DataParamDescriptor(0, 0, 0, "Наличие охранной сигнализации", "Security alarm", paramEncoding.Unknown, listType.EquipmentList, 18, "todo: исправить маску и смещение"); // Да
            markup.DataBlockParamsList[54] = new DataParamDescriptor(0, 0, 0, "Наличие пульта дистанционного управления", "Remote control", paramEncoding.Unknown, listType.EquipmentList, 19, "todo: исправить маску и смещение"); // Да

            //-----------------------------------------------------------??
            markup.DataBlockParamsList[55] = new DataParamDescriptor(0, 0, 0, "Мигание сигнализатора состояния защиты автомобиля", "Security alarm blinking lights", paramEncoding.Unknown, listType.EquipmentList, 20, "todo: исправить маску и смещение"); // Активировано
            markup.DataBlockParamsList[56] = new DataParamDescriptor(0, 0, 0, "Суперблокировка замков", "Advanced doorlock block", paramEncoding.Unknown, listType.EquipmentList, 21, "todo: исправить маску и смещение"); // 
            markup.DataBlockParamsList[57] = new DataParamDescriptor(0, 0, 0, "Тип охранной сигнализации (страна)", "Security alarm type (origin country)", paramEncoding.Unknown, listType.EquipmentList, 22, "todo: исправить адрес и как показывать"); // 
            markup.DataBlockParamsList[58] = new DataParamDescriptor(0, 0, 0, "Наличие сирены охранной сигнализации", "Security alarm buzzer", paramEncoding.Unknown, listType.EquipmentList, 23, "todo: исправить маску и смещение"); // Да
            markup.DataBlockParamsList[59] = new DataParamDescriptor(0, 0, 0, "Включение аварийной сигнализации при блокировке автомобиля кнопкой на центральной консоли", "Security alarm activation with a dashboard button", paramEncoding.Unknown, listType.EquipmentList, 24, "todo: исправить маску и смещение"); // Деактивировано
            markup.DataBlockParamsList[60] = new DataParamDescriptor(0, 0, 0, "Наличие дополнительного отопителя (автономного отопителя)", "S/c heater", paramEncoding.Unknown, listType.EquipmentList, 25, "todo: исправить маску и смещение"); // Нет
            markup.DataBlockParamsList[61] = new DataParamDescriptor(0, 0, 0, "Наличие системы предупреждения о превышении скорости", "Overspeed warning", paramEncoding.Unknown, listType.EquipmentList, 26, "todo: исправить маску и смещение"); // Нет

            markup.DataBlockParamsList[62] = new DataParamDescriptor(0x265, 1, 0x04, "Изменение единицы измерения указателя пробега", "Mileage units change ability", paramEncoding.maskBits, listType.EquipmentList, 27, ""); // Имеется
            markup.DataBlockParamsList[63] = new DataParamDescriptor(0x265, 1, 0x08, "Наличие сажевого фильтра", "Particulate filter", paramEncoding.maskBitsYesNo, listType.EquipmentList, 28, ""); // Да
            markup.DataBlockParamsList[64] = new DataParamDescriptor(0x265, 1, 0x10, "Меню активации дневных ходовых огней", "Daytime running light activation menu", paramEncoding.maskBits, listType.EquipmentList, 29, ""); // Отсутствует
            markup.DataBlockParamsList[65] = new DataParamDescriptor(0x265, 1, 0x20, "Меню включения запирания дверей в движении", "Doorblock on the go activation menu", paramEncoding.maskBits, listType.EquipmentList, 30, ""); // Отсутствует
            markup.DataBlockParamsList[66] = new DataParamDescriptor(0x265, 1, 0x80, "Наличие датчика температуры наружного воздуха", "Outdoor temperature sensor", paramEncoding.maskBitsYesNo, listType.EquipmentList, 31, ""); // Да
            markup.DataBlockParamsList[67] = new DataParamDescriptor(0x266, 1, 0x00, "Коррекция, применяемая к величине скорости", "Speed correction", paramEncoding.Byte, listType.EquipmentList, 32, ""); // 06 %

            //-----------------------------------------------------------??
            markup.DataBlockParamsList[68] = new DataParamDescriptor(0x234, 1, 0x80, "Наличие меню отключения подушки безопаности переднего пассажира", "Front passenger airbag disabling menu", paramEncoding.maskBitsYesNo, listType.EquipmentList, 33, "todo: исправить маску и смещение"); // Нет
            markup.DataBlockParamsList[69] = new DataParamDescriptor(0x26F, 1, 0x00, "Тип топливного бака", "Fuel tank type", paramEncoding.Byte, listType.EquipmentList, 34, ""); // тип 2 90л
            markup.DataBlockParamsList[70] = new DataParamDescriptor(0x270, 2, 0x00, "Пробег в километрах до ближайшего ТО", "Mileage to nearest maintenance time", paramEncoding.WordInverted, listType.EquipmentList, 35, "todo: уточнить"); // 48000?
            markup.DataBlockParamsList[71] = new DataParamDescriptor(0x271, 1, 0x04, "Наличие хронотахографа", "Digital chronotachograph present", paramEncoding.maskBitsYesNo, listType.EquipmentList, 36, ""); // Нет
            markup.DataBlockParamsList[72] = new DataParamDescriptor(0, 0, 0, "Вывод дополнительного языка", "Additional language", paramEncoding.Unknown, listType.EquipmentList, 37, "todo: исправить адрес и как показывать"); // Весь остальной мир
            markup.DataBlockParamsList[73] = new DataParamDescriptor(0, 0, 0, "Уровень регулировки", "Adjustment level", paramEncoding.Unknown, listType.EquipmentList, 38, "todo: исправить"); // Имеется
            markup.DataBlockParamsList[74] = new DataParamDescriptor(0, 0, 0, "Коррекция направления фар в вертикальной плоскости", "Headlight vertical correction", paramEncoding.Unknown, listType.EquipmentList, 39, "todo: исправить"); // Имеется

            //-----------------------------------------------------------??
            markup.DataBlockParamsList[75] = new DataParamDescriptor(0, 0, 0, "Наличие дополнительных задних фонарей", "Additional rearlights", paramEncoding.Unknown, listType.EquipmentList, 40, "todo: исправить"); // Нет
            markup.DataBlockParamsList[76] = new DataParamDescriptor(0, 0, 0, "Наличие аварийной кнопки (для автомобиля, перевозящего пассажиров)", "Emergency button (for passenger vehicle)", paramEncoding.maskBitsYesNo, listType.EquipmentList, 41, "todo: исправить"); // Нет
            markup.DataBlockParamsList[77] = new DataParamDescriptor(0, 0, 0, "Тип высокочастотного пульта дистанционного управления", "Remote type", paramEncoding.Unknown, listType.EquipmentList, 42, "todo: исправить"); // 3 кнопок
            markup.DataBlockParamsList[78] = new DataParamDescriptor(0, 0, 0, "Заднее стекло с обогревом", "Rear window heater", paramEncoding.Unknown, listType.EquipmentList, 43, "todo: исправить"); // Отсутствует
            markup.DataBlockParamsList[79] = new DataParamDescriptor(0, 0, 0, "Тип сервисной аккумуляторной батареи", "Service accumulator", paramEncoding.Unknown, listType.EquipmentList, 44, "todo: исправить"); // Стандартная аккумуляторная батареяОтсутствует
            markup.DataBlockParamsList[80] = new DataParamDescriptor(0, 0, 0, "Тип системы обнаружения падения давления в шинах", "Tire pressure drop detection type", paramEncoding.Unknown, listType.EquipmentList, 45, "todo: исправить"); // Премиум
            markup.DataBlockParamsList[81] = new DataParamDescriptor(0, 0, 0, "Конфигурация системы обнаружения падения давления воздуха в шинах", "Tire pressure drop configuration", paramEncoding.Unknown, listType.EquipmentList, 46, "todo: исправить"); // Communauté Economique Européenne
            markup.DataBlockParamsList[82] = new DataParamDescriptor(0, 0, 0, "Плафон с постепенным уменьшением света", "Gradual dimming lamp", paramEncoding.Unknown, listType.EquipmentList, 47, "todo: исправить"); // Отсутствует
            markup.DataBlockParamsList[83] = new DataParamDescriptor(0, 0, 0, "Тип телематики", "Telematics type", paramEncoding.Unknown, listType.EquipmentList, 48, "todo: исправить"); // Телематика Jumper / Boxer

            markup.DataBlockParamsList[84] = new DataParamDescriptor(0x29C, 1, 0x06, "Тип коробки передач", "Transmission type", paramEncoding.TwoBit, listType.EquipmentList, 49, ""); // Механическая коробка передач PSA MLGU 6M
            markup.DataBlockParamsList[85] = new DataParamDescriptor(0x29C, 1, 0x60, "Тип антиблокировочной системы/системы динамической стабилизации", "ABS/ESP type", paramEncoding.Byte, listType.EquipmentList, 50, ""); // 
            markup.DataBlockParamsList[86] = new DataParamDescriptor(0x29D, 1, 0x10, "Сторона водителя", "Drivers side", paramEncoding.Byte, listType.EquipmentList, 51, ""); // 
            markup.DataBlockParamsList[87] = new DataParamDescriptor(0x29E, 1, 0x70, "Тип двигателя", "Engine type", paramEncoding.Byte, listType.EquipmentList, 52, ""); // Двигатель PUMA
            markup.DataBlockParamsList[88] = new DataParamDescriptor(0x29F, 1, 0x01, "Наличие круиз контроля", "Cruise control", paramEncoding.maskBitsYesNo, listType.EquipmentList, 53, ""); // Да
            markup.DataBlockParamsList[89] = new DataParamDescriptor(0x29F, 1, 0x02, "Наличие ограничителя скорости", "Speed limit function", paramEncoding.maskBitsYesNo, listType.EquipmentList, 54, ""); // Да

            //-----------------------------------------------------------??
            markup.DataBlockParamsList[90] = new DataParamDescriptor(0, 0, 0, "Датчик наличия воды в дизельном топливе", "Water detection in diesel fuel sensor", paramEncoding.Unknown, listType.EquipmentList, 55, "todo: исправить"); // Имеется
            markup.DataBlockParamsList[91] = new DataParamDescriptor(0x2A0, 1, 0x30, "Марка автомобиля", "Vehicle model", paramEncoding.Byte, listType.EquipmentList, 56, ""); // 
            markup.DataBlockParamsList[92] = new DataParamDescriptor(0x2A1, 1, 0x70, "Максимальная нагрузка автомобиля", "Maximum vehicle load", paramEncoding.Byte, listType.EquipmentList, 57, ""); //  3, 3 тонн
            markup.DataBlockParamsList[93] = new DataParamDescriptor(0x2A1, 1, 0x06, "Колесная база автомобиля", "Wheel base", paramEncoding.TwoBit, listType.EquipmentList, 58, ""); //  Длинная колесная база: от 3, 8 м до 4, 05 м
            markup.DataBlockParamsList[94] = new DataParamDescriptor(0, 0, 0, "Автомобиль для тропических стран", "Vehicle for tropical countries", paramEncoding.Unknown, listType.EquipmentList, 59, "todo: исправить"); // Нет
            markup.DataBlockParamsList[95] = new DataParamDescriptor(0x2A4, 1, 0xFF, "Предел ограничения скорости", "Speed limit", paramEncoding.Byte, listType.EquipmentList, 60, "todo: уточнить"); // Скорость не Ограничена, км / ч
            markup.DataBlockParamsList[96] = new DataParamDescriptor(0x2A6, 1, 0x04, "Подогреватель паров масла", "Oil vapor heater", paramEncoding.maskBits, listType.EquipmentList, 61, ""); // Отсутствует
            markup.DataBlockParamsList[97] = new DataParamDescriptor(0x2A6, 1, 0x0f, "Передаточное отношение выхода из коробки передач", "Transmission gear ratio", paramEncoding.Byte, listType.EquipmentList, 62, ""); // 4, 933(15x74)

            //-----------------------------------------------------------??
            markup.DataBlockParamsList[98] = new DataParamDescriptor(0, 0, 0, "Система подогрева топлива", "Fuel heating system", paramEncoding.maskBitsYesNo, listType.EquipmentList, 63, "todo: исправить"); // Нет
            markup.DataBlockParamsList[99] = new DataParamDescriptor(0, 0, 0, "Выборочная блокировка замков автомобиля", "Selective door blocking system", paramEncoding.maskBitsYesNo, listType.EquipmentList, 64, "todo: исправить"); // Нет
            markup.DataBlockParamsList[100] = new DataParamDescriptor(0x2A6, 1, 0x80, "Система снижения выбросов NOX", "NOX emission control system", paramEncoding.maskBitsYesNo, listType.EquipmentList, 65, ""); // Нет
            markup.DataBlockParamsList[101] = new DataParamDescriptor(0, 0, 0, "Старт-стоп система", "Stop and Start system", paramEncoding.maskBitsYesNo, listType.EquipmentList, 66, "todo: исправить"); // Нет
            markup.DataBlockParamsList[102] = new DataParamDescriptor(0x265, 1, 0x80, "Наличие датчика температуры наружного воздуха", "Outdoor temperature sensor", paramEncoding.maskBitsYesNo, listType.EquipmentList, 67, ""); // Да
            markup.DataBlockParamsList[103] = new DataParamDescriptor(0x223, 1, 0x40, "Предупреждение о непреднамеренном пересечении линии дорожной разметки", "Unintenional crossing road lines warning", paramEncoding.maskBits, listType.EquipmentList, 68, ""); // Отсутствует
            markup.DataBlockParamsList[104] = new DataParamDescriptor(0x26F, 1, 0x00, "Тип топливного бака", "Fuel tank type", paramEncoding.Byte, listType.EquipmentList, 69, ""); // тип 2 90л
            markup.DataBlockParamsList[105] = new DataParamDescriptor(0, 0, 0, "Система контроля на спуске", "Descent control system", paramEncoding.Unknown, listType.EquipmentList, 70, "todo: исправить"); // Нет
            markup.DataBlockParamsList[106] = new DataParamDescriptor(0, 0, 0, "Конфигурация автомагнитолы", "Radio configuration", paramEncoding.Unknown, listType.EquipmentList, 71, "todo: исправить"); // Недействительное значение

            //-----------------------------------------------------------??
            markup.DataBlockParamsList[107] = new DataParamDescriptor(0, 0, 0, "Тип аудиосистемы", "Audio system type", paramEncoding.Unknown, listType.EquipmentList, 72, "todo: исправить"); // Основа
            markup.DataBlockParamsList[108] = new DataParamDescriptor(0, 0, 0, "Конфигурация аудиосистемы", "Audio system configuration", paramEncoding.Unknown, listType.EquipmentList, 73, "todo: исправить"); // Недействительное значение
            markup.DataBlockParamsList[109] = new DataParamDescriptor(0, 0, 0, "Тип мультимедийного разъема", "Multimedia socket type", paramEncoding.Unknown, listType.EquipmentList, 74, "todo: исправить"); // AUX и USB
            markup.DataBlockParamsList[110] = new DataParamDescriptor(0, 0, 0, "Регулировка параметров микрофона", "Microphone parameters", paramEncoding.Unknown, listType.EquipmentList, 75, "todo: исправить"); // Конфигурация микрофона №1
            markup.DataBlockParamsList[111] = new DataParamDescriptor(0, 0, 0, "Время до автоматического выключения автомагнитолы или радионавигации", "Time to automatical turning off audio or navigation system", paramEncoding.Unknown, listType.EquipmentList, 76, "todo: исправить"); // 20 min
            markup.DataBlockParamsList[112] = new DataParamDescriptor(0, 0, 0, "Тип подрулевого управления автомагнитолой", "Audio on steer control type", paramEncoding.Unknown, listType.EquipmentList, 77, "todo: исправить"); // Тип 1
            markup.DataBlockParamsList[113] = new DataParamDescriptor(0, 0, 0, "Конфигурация микрофона", "Microphone configuration", paramEncoding.Unknown, listType.EquipmentList, 78, "todo: исправить"); // Микрофон
            markup.DataBlockParamsList[114] = new DataParamDescriptor(0, 0, 0, "Тип микрофона", "Microphone type", paramEncoding.Unknown, listType.EquipmentList, 79, "todo: исправить"); // Тип 1
            markup.DataBlockParamsList[115] = new DataParamDescriptor(0, 0, 0, "Тип включения звукового сигнала", "Sound signal type", paramEncoding.Unknown, listType.EquipmentList, 80, "todo: исправить"); // Неактивно
            markup.DataBlockParamsList[116] = new DataParamDescriptor(0, 0, 0, "Язык", "Language", paramEncoding.Unknown, listType.EquipmentList, 81, "todo: исправить"); // Язык по умолчанию

            //-----------------------------------------------------------??
            markup.DataBlockParamsList[117] = new DataParamDescriptor(0, 0, 0, "Тип антенны FM1/AM", "FM/AM Antenna type", paramEncoding.Unknown, listType.EquipmentList, 82, "todo: исправить"); // Активно
            markup.DataBlockParamsList[118] = new DataParamDescriptor(0, 0, 0, "Наличие антенны FM2", "FM2 Antenna presence", paramEncoding.Unknown, listType.EquipmentList, 83, "todo: исправить"); // Нет
            markup.DataBlockParamsList[119] = new DataParamDescriptor(0, 0, 0, "Наличие антенны цифрового радиоприемника", "Digital reciever antenna presence", paramEncoding.Unknown, listType.EquipmentList, 84, "todo: исправить"); // Нет
            markup.DataBlockParamsList[120] = new DataParamDescriptor(0, 0, 0, "Наличие антенны GPS", "GPS Antenna presence", paramEncoding.Unknown, listType.EquipmentList, 85, "todo: исправить"); // Да
            markup.DataBlockParamsList[121] = new DataParamDescriptor(0, 0, 0, "Управление фарами LED", "Lead headlights control", paramEncoding.Unknown, listType.EquipmentList, 86, "todo: исправить"); // Отсутствует
            markup.DataBlockParamsList[122] = new DataParamDescriptor(0, 0, 0, "Отсоединение аккумуляторной батареи", "Accumulator disconnection", paramEncoding.Unknown, listType.EquipmentList, 87, "todo: исправить"); // Другое
            markup.DataBlockParamsList[123] = new DataParamDescriptor(0x33E, 1, 0x03, "Тип ветрового стекла", "Windshield type", paramEncoding.Byte, listType.EquipmentList, 88, ""); // 
            markup.DataBlockParamsList[124] = new DataParamDescriptor(0x341, 1, 0x01, "Дверь со стороны водителя", "Drivers side door", paramEncoding.maskBits, listType.EquipmentList, 89, ""); // 
            markup.DataBlockParamsList[125] = new DataParamDescriptor(0x341, 1, 0x02, "Дверь пассажира", "Passengers side door", paramEncoding.maskBits, listType.EquipmentList, 90, ""); // 
            markup.DataBlockParamsList[126] = new DataParamDescriptor(0x341, 1, 0x04, "Левая боковая сдвижная дверь", "Left side sliding door", paramEncoding.maskBits, listType.EquipmentList, 91, ""); // 
            markup.DataBlockParamsList[127] = new DataParamDescriptor(0x341, 1, 0x08, "Правая боковая сдвижная дверь", "Right side sliding door", paramEncoding.maskBits, listType.EquipmentList, 92, ""); // 
            markup.DataBlockParamsList[128] = new DataParamDescriptor(0x341, 1, 0x10, "Задние створчатые двери", "Rear side flap doors", paramEncoding.maskBits, listType.EquipmentList, 93, ""); // 

            markup.DataBlockParamsList[129] = new DataParamDescriptor(0x344, 1, 0x01, "Функция диагностики задних противотуманных фонарей", "Rearside fog lights diagnostic function", paramEncoding.maskBits, listType.EquipmentList, 94, ""); // Имеется
            markup.DataBlockParamsList[130] = new DataParamDescriptor(0x344, 1, 0x02, "Функция диагностики габаритных фонарей", "Side lights diagnostic function", paramEncoding.maskBits, listType.EquipmentList, 95, ""); // Имеется
            markup.DataBlockParamsList[131] = new DataParamDescriptor(0x344, 1, 0x08, "Функция диагностики фонарей заднего хода", "Reversing lights diagnostic function", paramEncoding.maskBits, listType.EquipmentList, 96, ""); // Имеется
            markup.DataBlockParamsList[132] = new DataParamDescriptor(0x344, 1, 0x10, "Функция диагностики фонарей стоп-сигнала", "Stop signal lights diagnostic function", paramEncoding.maskBits, listType.EquipmentList, 97, ""); // Имеется
            markup.DataBlockParamsList[133] = new DataParamDescriptor(0x344, 1, 0x80, "Функция диагностики центрального стоп-сигнала", "Middlemost stop signal lights diagnostic function", paramEncoding.maskBits, listType.EquipmentList, 98, ""); // Имеется
            markup.DataBlockParamsList[134] = new DataParamDescriptor(0x344, 1, 0x20, "Функция диагностики дневных ходовых огней", "Daylights running diagnostic function", paramEncoding.maskBits, listType.EquipmentList, 99, ""); // Имеется
            markup.DataBlockParamsList[135] = new DataParamDescriptor(0x348, 1, 0xf0, "Указатели поворота", "Turn signal lights", paramEncoding.Byte, listType.EquipmentList, 100, ""); // 16wt
            markup.DataBlockParamsList[136] = new DataParamDescriptor(0, 0, 0, "Повторная автоматическая блокировка дверей", "Repeated automatical door blocking", paramEncoding.Unknown, listType.EquipmentList, 101, "todo: исправить"); // 16wt

            // Пассивная безопасность
            markup.DataBlockParamsList[137] = new DataParamDescriptor(0, 0, 0, "Тип подушек безопасности водителя", "Drivers airbag type", paramEncoding.Unknown, listType.PassiveSafetyList, 0, "todo: исправить"); // Подушка безопасности 1 уровня
            markup.DataBlockParamsList[138] = new DataParamDescriptor(0, 0, 0, "Тип подушек безопасности пассажира", "Passengers airbag type", paramEncoding.Unknown, listType.PassiveSafetyList, 1, "todo: исправить"); // Отсутствие подушки безопасности
            markup.DataBlockParamsList[139] = new DataParamDescriptor(0, 0, 0, "Оконная подушка безопасности водителя", "Drivers window airbag", paramEncoding.Unknown, listType.PassiveSafetyList, 2, "todo: исправить"); // Отсутствует
            markup.DataBlockParamsList[140] = new DataParamDescriptor(0, 0, 0, "Оконная подушка безопасности пассажира", "Passengers window airbag", paramEncoding.Unknown, listType.PassiveSafetyList, 3, "todo: исправить"); // Нет
            markup.DataBlockParamsList[141] = new DataParamDescriptor(0, 0, 0, "Передние боковые подушки безопасности", "Front side airbags", paramEncoding.Unknown, listType.PassiveSafetyList, 4, "todo: исправить"); // Отсутствуют
            markup.DataBlockParamsList[142] = new DataParamDescriptor(0, 0, 0, "Передний левый и передний правый датчик удара", "Front left and right shock sensor", paramEncoding.Unknown, listType.PassiveSafetyList, 5, "todo: исправить"); // Отсутствует (состояние невозможно)
            markup.DataBlockParamsList[143] = new DataParamDescriptor(0, 0, 0, "Преднатяжитель ремня безопасности водителя", "Drivers seat belt pretensioner", paramEncoding.Unknown, listType.PassiveSafetyList, 6, "todo: исправить"); // Преднатяжитель 1 уровня
            markup.DataBlockParamsList[144] = new DataParamDescriptor(0, 0, 0, "Преднатяжитель ремня безопасности пассажира", "Passengers seat belt pretensioner", paramEncoding.Unknown, listType.PassiveSafetyList, 7, "todo: исправить"); // Отсутствие преднатяжителя
            markup.DataBlockParamsList[145] = new DataParamDescriptor(0, 0, 0, "Преднатяжитель ремня безопасности центрального переднего пассажира", "Middle front passengers seat belt pretensioner", paramEncoding.Unknown, listType.PassiveSafetyList, 8, "todo: исправить"); // Отсутствие преднатяжителя
            markup.DataBlockParamsList[146] = new DataParamDescriptor(0, 0, 0, "Контактор ремня безопасности водителя", "Drivers seat belt contactor", paramEncoding.Unknown, listType.PassiveSafetyList, 9, "todo: исправить"); // Имеется
            return markup;
        }


        public const int MAX_PARAMS_IN_DATA_BLOCK = 160;
        public const int MAX_DATA_SIZE_IN_BYTES = 18;
        private string epromName;
    }
}
