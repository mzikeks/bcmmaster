﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BCMmaster.MarkupParsing;

namespace BCMmaster
{
    public partial class MainForm : Form
    {
        public const int MAX_ROWS_AT_EQUIPMENT_TAB = 104;

        stringsMultyColumnsData[] strListEquipmet = new stringsMultyColumnsData[MAX_ROWS_AT_EQUIPMENT_TAB];

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        void FillEquipmentListViewFromParsedList()
        {
            ListViewItem lvi;

            listViewEquipment.Items.Clear(); 
            strListEquipmet = new stringsMultyColumnsData[MAX_ROWS_AT_EQUIPMENT_TAB];

            int i = 0, j = 0, index = 0;

            for (i = 0; i < PropertiesMarkup.MAX_PARAMS_IN_DATA_BLOCK; i++)
            //
            // в цикле по всем параметрам
            //
            {
                if (null == currentMarkup.propertiesMarkup.DataBlockParamsList[i].additionalInfo)
                    //
                    // пустой элемент - конец массива параметров, выходим 
                    //
                    break;

                if (currentMarkup.propertiesMarkup.DataBlockParamsList[i].listTab == listType.EquipmentList)
                //
                // если параметр для этого (EquipmentList) списка 
                //
                {
                    index = currentMarkup.propertiesMarkup.DataBlockParamsList[i].positionInList;
                    if (index >= MAX_ROWS_AT_EQUIPMENT_TAB)
                        continue;

                    strListEquipmet[index].FirstCol = SetStringLanguageAccording(currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramDescriptionRu,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramDescriptionEn);

                    strListEquipmet[index].SecondCol = SetStringEncodingTypeAccording(
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].encodingType,
                        epromFile,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramOffset,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramLength,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].bitMask,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].currDataBytes,
                        ref currentMarkup.propertiesMarkup.DataBlockParamsList[i].currCaseSelector);

                    strListEquipmet[index].ThirdCol = currentMarkup.propertiesMarkup.DataBlockParamsList[i].additionalInfo;

                    strListEquipmet[index].FourthCol = SetValueTypeAccording(
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramOffset,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].paramLength,
                        epromFile,
                        currentMarkup.propertiesMarkup.DataBlockParamsList[i].currDataBytes);

                    strListEquipmet[index].index = i;

                    j++;
                }
            }

            for (i = 0; i < j; i++)
            {
                lvi = new ListViewItem(strListEquipmet[i].FirstCol);
                lvi.SubItems.Add(strListEquipmet[i].SecondCol);
                lvi.SubItems.Add(strListEquipmet[i].ThirdCol);

                listViewEquipment.Items.Add(lvi);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //
        // 
        //
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        private void listViewEquipment_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            EditSelectedParameter();
        }


    }
}